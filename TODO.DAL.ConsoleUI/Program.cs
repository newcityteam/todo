﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Config;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Repositories;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace TODO.DAL.ConsoleUI
{
	/// <summary>
	/// Стартовый класс с методом Main
	/// </summary>
	class Program
	{
		/// <summary>
		/// Строка подключения к БД
		/// </summary>
		static string CurrentConnectionString = ConnectionString.LocalMain;

		/// <summary>
		/// Unity контейнер
		/// </summary>
		static IUnityContainer container;

		/// <summary>
		/// Точка старта приложения
		/// </summary>
		/// <param name="args">аргументы командной строки</param>
		static void Main(string[] args)
		{
			Console.WriteLine("begin");

			ContainerInitialize();

			CreateTask();

			ReadAllTasks();

			Console.WriteLine("\nend");
			Console.ReadKey();
		}

		/// <summary>
		/// Инициализация контейнера Unity
		/// </summary>
		static void ContainerInitialize()
		{
			try
			{
				// Создание контейнера

				container = new UnityContainer();

				// Передача строки подключения в TODOContext минуя DALUnity

				container.RegisterInstance("ConnectionString", CurrentConnectionString, new ContainerControlledLifetimeManager());
				var cs = new InjectionConstructor(new ResolvedParameter<string>("ConnectionString"));
				container.RegisterType<TODOContext>(cs);

				// Регистрация DALUnit - класс доступа к слою DAL

				container.RegisterType<DALUnit>(new ContainerControlledLifetimeManager());

				// Регистрация репозиториев

				container.RegisterType<IRepository<TaskDTO>, TaskRepository>();
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		/// <summary>
		/// Отображение исключения и завершение работы приложения
		/// </summary>
		/// <param name="ex"></param>
		static void ShowException(Exception ex)
		{
			Console.WriteLine(ex.Message);
			if (ex.InnerException != null)
			{
				Console.WriteLine("\nInner exception:");
				Console.WriteLine(ex.InnerException.Message);
				Console.ReadKey();
				Environment.Exit(0);
			}
		}

		/// <summary>
		/// Проверка: создание объекта задача в БД
		/// </summary>
		static void CreateTask()
		{
			var rnd = new Random().Next(100, 500);

			var dto = new TaskDTO();
			dto.Title = "Task N " + rnd;
			dto.Description = $"Description for task {rnd}";
			dto.DateCreated = DateTime.Now;
			dto.DateDue = DateTime.Now.AddDays(5);

			try
			{
				var dal = container.Resolve<DALUnit>();
				dal.Tasks.Create(dto);
				Console.WriteLine($"Create OK. ID: {dto.ID}");
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		/// <summary>
		/// Проверка: чтение списка задач из БД
		/// </summary>
		static void ReadAllTasks()
		{
			try
			{
				var dal = container.Resolve<DALUnit>();
				var result = dal.Tasks.ReadAll();
				foreach (var item in result)
				{
					Console.WriteLine($"{item.ID} {item.Title} \t {item.Description}");
					Console.WriteLine($"{item.DateCreated}\n{item.DateDue}\n{item.State}\n{item.Direction}\n");
				}
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}
	}
}
