﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TODO.Common.DTO;
using TODO.Common.Interfaces.Services;
using TODO.UI.Views2;

namespace TODO.UI.Infrastructure
{
	// TODO interface + unity

	public static class SessionManage
	{
		public static MainView mainView;

		public static int CurrentProjectID;
		public static int CurrentTaskID;
		public static string CurrentViewName;

		private static ITaskService _taskService;
		private static IProjectService _projectService;
		private static ICounteragentService _counteragentService;
		private static IFinanceOperationService _financeOperation;
		private static ILogger _logger;

		//
		// Инициализация интерфейсов
		//

		public static void Initialize(ICounteragentService counteragents, ITaskService tasks, ILogger logger, IProjectService projects, IFinanceOperationService financeOperation)
		{
			_taskService = tasks;
			_projectService = projects;
			_counteragentService = counteragents;
			_logger = logger;
			_financeOperation = financeOperation;
		}

		//
		// Загрузка параметров из конфига приложения и восстановление view
		//

		public static void RestoreView()
		{
			CurrentProjectID = Properties.Settings.Default.CurrentProjectID;
			CurrentTaskID = Properties.Settings.Default.CurrentTaskID;
			CurrentViewName = Properties.Settings.Default.CurrentView;

			if (CurrentViewName.Contains(".TasksView"))
			{
				SwitchToTasks();
			}
			else if (CurrentViewName.Contains("ProjectsView"))
			{
				SwitchToProjects();
			}
			else if (CurrentViewName.Contains("CounteragentsView"))
			{
				SwitchToCounteragents();
			}
			else if (CurrentViewName.Contains("ProjectTasksView"))
			{
				var project = _projectService.ReadProject(CurrentProjectID);
				if (project == null)
				{
					SwitchToTasks();
				}
				else
				{
					SwitchToProjectTasks(project);
				}
			}
			else
			{
				// default view
				SwitchToTasks();
			}
		}

		//
		// Сохранение параметров в конфиг приложения
		//

		private static void Save()
		{
			Properties.Settings.Default.Save();
		}

		public static void SaveCurrentView(string viewName)
		{
			Properties.Settings.Default.CurrentView = viewName;
			Save();
		}

		public static void SaveCurrentTaskID(int taskID)
		{
			Properties.Settings.Default.CurrentTaskID = taskID;
			Save();
		}

		public static void SaveCurrentProjectID(int projectID)
		{
			Properties.Settings.Default.CurrentProjectID = projectID;
			Save();
		}

		/// 
		/// Переключение view
		/// 

		private static void Switch(UserControl newView)
		{
			mainView.Navigate(newView);
			SaveCurrentView(newView.ToString());
		}

		public static void SwitchToTasks()
		{
			var view = new TasksView(_taskService, _projectService, _counteragentService, _logger, _financeOperation);
			Switch(view);
		}

		public static void SwitchToTaskProfile()
		{
			var view = new TaskProfileView();
			Switch(view);
		}

		public static void SwitchToProjects()
		{
			var view = new ProjectsView(_taskService, _counteragentService, _projectService, _logger, _financeOperation);
			Switch(view);
		}

		public static void SwitchToProjectProfile()
		{
			var view = new ProjectProfileView();
			Switch(view);
		}

		public static void SwitchToProjectTasks(ProjectDTO project)
		{
			var view = new ProjectTasksView(_projectService, _counteragentService, _taskService, project);
			SaveCurrentProjectID(project.ID);
			Switch(view);
		}

		public static void SwitchToCounteragents()
		{
			var view = new CounteragentsView(_counteragentService);
			Switch(view);
		}

		public static void SwitchToCounteragentProfile()
		{
			var view = new CounteragentProfileView();
			Switch(view);
		}
	}
}
