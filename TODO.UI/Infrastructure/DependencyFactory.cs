﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.BAL;
using TODO.BAL.Services;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL;
using TODO.DAL.Context;
using TODO.DAL.Repositories;
using TODO.Logging;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace TODO.UI.Infrastructure
{
	/// <summary>
	/// Класс фабрика контейнера Unity
	/// </summary>
	public static class DependencyFactory
	{
		/// <summary>
		/// Объект контейнера Unity
		/// </summary>
		public static IUnityContainer Container;

		/// <summary>
		/// Инициализация контейнера Unity
		/// </summary>
		public static void Initialize()
		{
			Container = new UnityContainer();

			// Передача строки подключения в TODOContext минуя DALUnit

			// Строка для MSSQL: DefaultConnection. Для встроенного в студию клиента (localdb): LocalDbConnection.
			// Строки для интеграционных тестов: TestConnection и TestLocalDbConnection. Устанавливаются в классах с тестами и работают только оттуда. Прописаны в DAL.Tests/App.Config.
			Container.RegisterInstance("ConnectionString", "LocalDbConnection", new ContainerControlledLifetimeManager());
			var cs = new InjectionConstructor(new ResolvedParameter<string>("ConnectionString"));
			Container.RegisterType<TODOContext>(new ContainerControlledLifetimeManager(), cs);

			// Регистрация UnitOfWork - класс доступа к слою DAL

			Container.RegisterType<IUnitOfWork, UnitOfWork>(new ContainerControlledLifetimeManager());

			// Регистрация репозиториев

			Container.RegisterType<IRepository<TaskDTO>, TaskRepository>();
			Container.RegisterType<IRepository<CounteragentDTO>, CounteragentRepository>();
			Container.RegisterType<IRepository<TagDTO>, TagRepository>();
			Container.RegisterType<IRepository<ProjectDTO>, ProjectRepository>();
			Container.RegisterType<IRepository<FinanceOperationDTO>, FinanceOperationRepository>();

			// Регистрация сервисов

			Container.RegisterType<ITaskService, TaskService>();
			Container.RegisterType<ICounteragentService, CounteragentService>();
			Container.RegisterType<IProjectService, ProjectService>();
			Container.RegisterType<IFinanceOperationService, FinanceOperationService>();

			// Регистрация логера
			Container.RegisterSingleton<ILogger, NLogLogger>();
		}
	}
}
