﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.ViewModels;

namespace TODO.UI.Views
{
	/// <summary>
	/// Interaction logic for ProjectBookView.xaml
	/// </summary>
	public partial class ProjectBookView : Window
	{
		public ProjectBookView(IProjectService projects, ICounteragentService counteragents, ITaskService tasks, ProjectDTO project)
		{
			InitializeComponent();
			
			var vm = new ProjectBookVM(project,tasks, counteragents, projects);
			vm.ErrorEvent += OnError;
			DataContext = vm;
		}
		private void OnError(object sender, StringMessageEventArgs e)
		{
			MessageBox.Show(e.Message, "", MessageBoxButton.OK, MessageBoxImage.Warning);
		}
	}
}
