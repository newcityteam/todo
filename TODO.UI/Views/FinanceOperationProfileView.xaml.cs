﻿using System;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Interfaces.Services;
using TODO.UI.ViewModels;

namespace TODO.UI.Views
{
	public partial class FinanceOperationProfileView : Window
	{
		public FinanceOperationProfileView(IFinanceOperationService financeOperationService,ILogger logger , ProjectDTO project)
		{
			InitializeComponent();
			var vm = new FinanceOperationProfileViewModel(financeOperationService, logger, project);
			vm.OKEvent += OnOK;
			vm.CancelEvent += OnCancel;

			DataContext = vm;
		}

		/// <summary>
		/// Обработка события OKEvent из ViewModel
		/// </summary>
		/// <param name="sender">Вызывающий объект</param>
		/// <param name="e">Агрументы вызова</param>
		private void OnOK(object sender, EventArgs e)
		{
			// Закрытие окна с положительным результатом

			DialogResult = true;
		}

		/// <summary>
		/// Обработка события CancelEvent из ViewModel
		/// </summary>
		/// <param name="sender">Вызывающий объект</param>
		/// <param name="e">Агрументы вызова</param>
		private void OnCancel(object sender, EventArgs e)
		{
			// Закрытие окна с отрицательным результатом

			DialogResult = false;
		}
	}
}
