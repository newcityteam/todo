﻿using System;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.ViewModels;

namespace TODO.UI.Views
{
	/// <summary>
	/// Окно профиля проекта.
	/// </summary>
	public partial class ProjectProfileView : Window
	{
		public ProjectProfileView(ICounteragentService counteragents, IProjectService projects, IFinanceOperationService financeOperation ,ProjectDTO project = null)
		{
			InitializeComponent();

			var viewModel = new ProjectProfileVM(counteragents, projects, financeOperation, project);

			viewModel.OkEvent += OnOk;
			viewModel.CancelEvent += OnCancel;
			viewModel.ErrorEvent += OnError;

			DataContext = viewModel;
		}

		/// <summary>
		/// Обработка события OkEvent из ViewModel
		/// </summary>
		/// <param name="sender">Вызывающий объект</param>
		/// <param name="e">Агрументы вызова</param>
		private void OnOk(object sender, EventArgs e)
		{
			// Закрытие окна с положительным результатом

			DialogResult = true;
		}

		/// <summary>
		/// Обработка события CancelEvent из ViewModel
		/// </summary>
		/// <param name="sender">Вызывающий объект</param>
		/// <param name="e">Агрументы вызова</param>
		private void OnCancel(object sender, EventArgs e)
		{
			// Закрытие окна с отрицательным результатом

			DialogResult = false;
		}

		/// <summary>
		/// Обработка ошибок во ViewModel
		/// </summary>
		/// <param name="sender">класс ViewModel</param>
		/// <param name="e">Аргументы ErrorViewModelArgs</param>
		private void OnError(object sender, StringMessageEventArgs e)
		{
			MessageBox.Show(e.Message, "", MessageBoxButton.OK, MessageBoxImage.Warning);
		}

	}
}
