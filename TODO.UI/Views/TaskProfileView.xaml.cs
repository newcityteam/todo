﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.ViewModels;

namespace TODO.UI.Views
{
	/// <summary>
	/// Окно профиля задачи
	/// </summary>
	public partial class TaskProfileView : Window
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="task">Задача для отображения. В случае null - будет создана новая задача</param>
		public TaskProfileView(ICounteragentService counteragents, ITaskService tasks, TaskDTO task = null, ProjectDTO project = null)
		{
			InitializeComponent();

			var vm = new TaskProfileVM(task, project, counteragents, tasks);
			vm.OKEvent += OnOK;
			vm.CancelEvent += OnCancel;
			vm.ErrorEvent += OnError;

			DataContext = vm;
		}

		/// <summary>
		/// Обработка события OKEvent из ViewModel
		/// </summary>
		/// <param name="sender">Вызывающий объект</param>
		/// <param name="e">Агрументы вызова</param>
		private void OnOK(object sender, EventArgs e)
		{
			// Закрытие окна с положительным результатом

			DialogResult = true;
		}

		/// <summary>
		/// Обработка события CancelEvent из ViewModel
		/// </summary>
		/// <param name="sender">Вызывающий объект</param>
		/// <param name="e">Агрументы вызова</param>
		private void OnCancel(object sender, EventArgs e)
		{
			// Закрытие окна с отрицательным результатом

			DialogResult = false;
		}

		/// <summary>
		/// Обработка ошибок во ViewModel
		/// </summary>
		/// <param name="sender">класс ViewModel</param>
		/// <param name="e">Аргументы ErrorViewModelArgs</param>
		private void OnError(object sender, StringMessageEventArgs e)
		{
			MessageBox.Show(e.Message, "", MessageBoxButton.OK, MessageBoxImage.Warning);
		}
	}
}
