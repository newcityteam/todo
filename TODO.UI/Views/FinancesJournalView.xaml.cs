﻿using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Interfaces.Services;
using TODO.UI.ViewModels;

namespace TODO.UI.Views
{
	/// <summary>
	/// Interaction logic for FinancesJournalView.xaml
	/// </summary>
	public partial class FinancesJournalView : Window
	{
		public FinancesJournalView(ICounteragentService counteragent, IFinanceOperationService financeOperation, ProjectDTO project)
		{
			InitializeComponent();

			var financeJournalViewModel = new FinancesJournalViewModel(counteragent, financeOperation, project);
			DataContext = financeJournalViewModel;

		}
	}
}
