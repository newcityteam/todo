﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace TODO.UI.Helpers
{
	/// <summary>
	/// Конвертер даты и времени в формат сообщения "Осталось n дней"
	/// </summary>
	/// <seealso cref="System.Windows.Data.IValueConverter" />
	public class DateTimeToDateConverter : IValueConverter
	{
		/// <summary>
		/// Возвращает дату в формате сообщения "Осталось n дней". Если дата не установлена, то будет возвращен null.
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return null;
			}

			var d1 = (DateTime)value;
			string result = string.Empty;
			var days = (d1 - DateTime.Now.Date).Days;

			if (days == 1)
			{
				result = "Остался 1 день ";
			}
			else if (days > 1 && days < 4)
			{
				result = $"Осталось {days} дня ";
			}

			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}
}