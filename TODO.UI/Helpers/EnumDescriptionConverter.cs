﻿using System;
using System.Globalization;
using System.Windows.Data;
using TODO.Common.Helpers;

namespace TODO.UI.Helpers
{
	/// <summary>
	/// Конвертер для показа значения атрибута Description у Enum
	/// </summary>
	public class EnumDescriptionConverter : IValueConverter
	{
		object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var myEnum = (Enum)value;
			var description = myEnum.GetDescription();
			return description;
		}

		object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return string.Empty;
		}
	}
}
