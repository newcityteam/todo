﻿using System;
using System.Globalization;
using System.Windows.Data;
using TODO.Common.Helpers;
using TODO.Common.Types;
using System.Windows.Media;

namespace TODO.UI.Helpers
{
	/// <summary>
	/// Конвертер для определения цвета фона для задач
	/// </summary>
	/// <seealso cref="System.Windows.Data.IMultiValueConverter" />
	public class ProjectStateToBackgroundColorConverter : IMultiValueConverter
	{
		/// <summary>
		/// Возвращает красный цвет фона для просроченных задач, серый для отмененных, и синий для всех остальных
		/// </summary>
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values.Length != 2 || !(values[0] is ProjectStateType) || (values[1] != null && !(values[1] is DateTime)))
			{
				throw new ArgumentException("Неправильные параметры конвертера для цвета фона проекта. Должно быть два параметра: статус проекта и срок его выполнения.");
			}
			var type = (ProjectStateType)values[0];
			var dateDue = (DateTime?)values[1];
			if (type == ProjectStateType.InProgress && dateDue.HasValue && dateDue < DateTime.Now)
			{
				return Brushes.LightCoral;
			}
			if (type == ProjectStateType.Cancelled)
			{
				return Brushes.Silver;
			}
			return Brushes.AliceBlue;
		}
		/// <summary>
		/// Не используется.
		/// </summary>
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}