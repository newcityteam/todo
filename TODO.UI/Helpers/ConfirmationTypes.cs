﻿using System.ComponentModel;

namespace TODO.UI.Helpers
{
	public enum TaskDialogConfirmationType
	{
		/// <summary>
		/// Описание подтверждения при завершении (переносе в архив) задачи.
		/// </summary>
		[Description("завершить задачу")]
		Complate,
		/// <summary>
		/// Описание подтверждения при удалении задачи.
		/// </summary>
		[Description("удалить задачу")]
		Delete,
		/// <summary>
		/// Описание подтверждения при отмене задачи.
		/// </summary>
		[Description("отменить задачу")]
		Cancel,

		/// <summary>
		/// Описание пересоздания задачи
		/// </summary>
		[Description("пересоздать задачу")]
		Recreate
	}

	public enum ProjectDialogConfirmationType
	{
		[Description("завершить проект")]
		Complate,

		[Description("удалить проект")]
		Delete,

		[Description("отменить проект")]
		Cancel,
		
		[Description("пересоздать проект")]
		Recreate
	}
}