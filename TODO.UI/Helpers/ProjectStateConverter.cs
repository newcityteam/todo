﻿using System;
using System.Globalization;
using System.Windows.Data;
using TODO.Common.Helpers;
using TODO.Common.Types;

namespace TODO.UI.Helpers
{
	/// <summary>
	/// Конвертер для вывода статуса задачи в UI (всех статусов, кроме Deleted и InProgress).
	/// </summary>
	/// <seealso cref="System.Windows.Data.IValueConverter" />
	public class ProjectStateConverter : IValueConverter
	{
		/// <summary>
		/// Возвращает описание Enum при определенных значениях Enum.
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var type = (ProjectStateType)value;
			if (type != ProjectStateType.Deleted && type != ProjectStateType.InProgress)
			{
				return type.GetDescription();
			}
			return null;
		}
		/// <summary>
		/// Не используется.
		/// </summary>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}