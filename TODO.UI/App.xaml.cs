﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TODO.Common.Interfaces.Services;
using TODO.UI.Infrastructure;
using Unity;

namespace TODO.UI
{
	/// <summary>
	/// Класс старта приложения
	/// </summary>
	public partial class App : Application
	{
		/// <summary>
		/// Метод старта приложения
		/// </summary>
		/// <param name="e">аргументы страта приложения</param>
		protected override void OnStartup(StartupEventArgs e)
		{
			DependencyFactory.Initialize();
			var tasks = DependencyFactory.Container.Resolve<ITaskService>();
			var counteragents = DependencyFactory.Container.Resolve<ICounteragentService>();
			var logger = DependencyFactory.Container.Resolve<ILogger>();
			var projects = DependencyFactory.Container.Resolve<IProjectService>();
			var financeOperation = DependencyFactory.Container.Resolve<IFinanceOperationService>();

			//var mainView = new Views.MainView(counteragents, tasks, logger, projects);
			//mainView.Show();

			SessionManage.Initialize(counteragents, tasks, logger, projects, financeOperation);

			var mainView = new Views2.MainView();
			mainView.Show();

			base.OnStartup(e);
		}
	}
}
