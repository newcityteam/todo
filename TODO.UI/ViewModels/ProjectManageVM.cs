﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.Helpers;
using TODO.UI.Views;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для главного окна MainView
	/// </summary>
	public class ProjectManageVM : PropertyChangedBase
	{
		/// <summary>
		/// Список задач. Испльзуется для отображения в ListView главного окна
		/// </summary>
		public ICollection<ProjectDTO> ProjectList { get; set; }

		/// <summary>
		/// Выбранная задача
		/// </summary>
		public ProjectDTO SelectedProject { get; set; }

		/// <summary>
		/// Пемеренная, хранящая заголовок текущего списка задач (активные / архивные).
		/// </summary>
		private string _projectListCaption;
		/// <summary>
		/// Заголовок текущего списка задач (активные / архивные).
		/// </summary>
		public string ProjectListCaption
		{
			get => _projectListCaption;
			set
			{
				_projectListCaption = value;
				NotifyPropertyChanged("ProjectListCaption");
			}
		}

		/// <summary>
		/// Перменная, хранящая сообщение об ошибке.
		/// </summary>
		private string _errorMessage;
		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(this, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		/// <summary>
		/// Событие возникновения ошибки.
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		/// <summary>
		/// Команда создания задачи.
		/// </summary>
		public RelayCommand CreateProjectCommand { get; private set; }

		/// <summary>
		/// Команда вывода профиля задачи.
		/// </summary>
		public RelayCommand ShowProjectDetailsCommand { get; private set; }

		/// <summary>
		/// Команда завершения задачи.
		/// </summary>
		public RelayCommand CompleteProjectCommand { get; private set; }

		/// <summary>
		/// Команда завершения задачи.
		/// </summary>
		public RelayCommand CancelProjectCommand { get; private set; }

		/// <summary>
		/// Команда удаления задачи.
		/// </summary>
		public RelayCommand DeleteProjectCommand { get; private set; }

		/// <summary>
		/// Команда вывода активных задач.
		/// </summary>
		public RelayCommand ShowActiveProjectsCommand { get; private set; }

		/// <summary>
		/// Команда вывода архивных задач.
		/// </summary>
		public RelayCommand ShowArchiveProjectsCommand { get; private set; }

		/// <summary>
		/// Команда вызова окна управления списком контрагентов.
		/// </summary>
		public RelayCommand ShowCounteragentsManageCommand { get; private set; }

		/// <summary>
		/// Команда вызова окна журнала задач проекта.
		/// </summary>
		public RelayCommand ShowProjectBookCommand { get; private set; }

		/// <summary>
		/// Закрыть окно
		/// </summary>
		public RelayCommand WindowCloseCommand { get; private set; }

		//
		// Состояние интерфейса
		//

		/// <summary>
		/// Переменная, хранящая признак отображения активного списка задач.
		/// </summary>
		private bool _isActiveProjectList;
		/// <summary>
		/// Признак отображения активного списка задач.
		/// Используется для задания состояния элементов View.
		/// </summary>
		public bool IsActiveProjectList
		{
			get => _isActiveProjectList;
			set
			{
				_isActiveProjectList = value;
				NotifyPropertyChanged("IsActiveProjectList");
				UpdateViewControls();
			}
		}

		/// <summary>
		/// Вспомогательное свойство, для биндинга
		/// </summary>
		public bool IsArchiveProjectList => !IsActiveProjectList;

		/// <summary>
		/// Переменная, хранящая признак доступности кнопки "Завершить задачу".
		/// </summary>
		private Visibility _visibilityButtonCompleteTask;
		/// <summary>
		/// Признак доступности кнопки "Завершить задачу".
		/// </summary>
		public Visibility VisibilityButtonCompleteTask
		{
			get => _visibilityButtonCompleteTask;
			set { _visibilityButtonCompleteTask = value; NotifyPropertyChanged("VisibilityButtonCompleteTask"); }
		}
		/// <summary>
		/// Поле доступа к TaskService
		/// </summary>
		private ITaskService _taskService;

		/// <summary>
		/// Поле доступа к ProjectService
		/// </summary>
		private IProjectService _projectService;

		/// <summary>
		/// Поле доступа к CounteragentService
		/// </summary>
		private ICounteragentService _counteragentService;

		private IFinanceOperationService _financeOperation;

		/// <summary>
		/// Конструктор.
		/// </summary>
		public ProjectManageVM(IProjectService projects, ICounteragentService counteragents, ITaskService tasks, IFinanceOperationService financeOperation)
		{
			_projectService = projects;
			_counteragentService = counteragents;
			_taskService = tasks;
			_financeOperation = financeOperation;

			// Инициализация команд.
			CreateProjectCommand = new RelayCommand(CreateProject);
			DeleteProjectCommand = new RelayCommand(DeleteProject);
			ShowProjectDetailsCommand = new RelayCommand(ShowProjectDetails);
			CompleteProjectCommand = new RelayCommand(CompleteProject, (x) => IsActiveProjectList);
			CancelProjectCommand = new RelayCommand(CancelProject, (x) => IsActiveProjectList);
			ShowActiveProjectsCommand = new RelayCommand(ShowActiveProjects, (x) => IsArchiveProjectList);
			ShowArchiveProjectsCommand = new RelayCommand(ShowArchiveProjects, (x) => IsActiveProjectList);
			ShowCounteragentsManageCommand = new RelayCommand(ShowCounteragentsManage);
			ShowProjectBookCommand = new RelayCommand(ShowProjectBook);
			WindowCloseCommand = new RelayCommand(x => ((Window)x).Close());
			ShowActiveProjects(null);
		}

		/// <summary>
		/// Метод создания проекта.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CreateProject(object parameter)
		{
			var projectProfile = new ProjectProfileView(_counteragentService,_projectService, _financeOperation);
			if (projectProfile.ShowDialog() == false)
			{
				return;
			}

			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowActiveProjects(null);
		}

		/// <summary>
		/// Метод создания задачи. Аналогичен методу CreateProject,
		/// только в качестве параметра для TaskProfileView передается SelectedProject.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowProjectDetails(object parameter)
		{
			if (SelectedProject == null)
			{
				MessageBox.Show("Не выбран проект","");
				return;
			}

			var taskProfile = new ProjectProfileView(_counteragentService, _projectService, _financeOperation, SelectedProject);
			if (taskProfile.ShowDialog() == false)
			{
				return;
			}

			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowActiveProjects(null);
		}

		/// <summary>
		/// Метод завершения.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CompleteProject(object parameter)
		{
			if (SelectedProject == null)
			{
				MessageBox.Show("Не выбран проект","");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(ProjectDialogConfirmationType.Complate))
			{
				var result = _projectService.SetCompleted(SelectedProject.ID);
				if (!result)
				{
					ErrorMessage = _projectService.ErrorMessage;
					return;
				}

				// Удаляем из локального списка, чтобы не перечитывать список из БД.

				ProjectList.Remove(SelectedProject);
				NotifyPropertyChanged("ProjectList");
			}
		}

		/// <summary>
		/// Метод удаления.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void DeleteProject(object parameter)
		{
			if (SelectedProject == null)
			{
				MessageBox.Show("Не выбран проект","");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(ProjectDialogConfirmationType.Delete))
			{
				var result = _projectService.SetDeleted(SelectedProject.ID);
				if (!result)
				{
					ErrorMessage = _projectService.ErrorMessage;
					return;
				}

				// Удаляем из локального списка, чтобы не перечитывать список из БД.

				ProjectList.Remove(SelectedProject);
				NotifyPropertyChanged("ProjectList");
			}
		}


		/// <summary>
		/// Метод отмены.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CancelProject(object parameter)
		{
			if (SelectedProject == null)
			{
				MessageBox.Show("Не выбран проект","");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(ProjectDialogConfirmationType.Cancel))
			{
				var result = _projectService.SetCancelled(SelectedProject.ID);
				if (!result)
				{
					ErrorMessage = _projectService.ErrorMessage;
					return;
				}
				
				// Удаляем из локального списка, чтобы не перечитывать список из БД.

				ProjectList.Remove(SelectedProject);
				NotifyPropertyChanged("ProjectList");
			} 
		}

		/// <summary>
		/// Метод вывода активных задач.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowActiveProjects(object parameter)
		{
			var result = _projectService.ReadActive();
			if (result == null)
			{
				ErrorMessage = _projectService.ErrorMessage;
				return;
			}

			ProjectList = new ObservableCollection<ProjectDTO>(result);
			NotifyPropertyChanged("ProjectList");

			IsActiveProjectList = true;
		}

		/// <summary>
		/// Метод вывода архивных задач.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowArchiveProjects(object parameter)
		{
			var result = _projectService.ReadArchive();
			if (result == null)
			{
				ErrorMessage = _projectService.ErrorMessage;
				return;
			}

			ProjectList = new ObservableCollection<ProjectDTO>(result);
			NotifyPropertyChanged("ProjectList");

			IsActiveProjectList = false;
		}

		/// <summary>
		/// Метод показа окна управления контрагентами.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowCounteragentsManage(object parameter)
		{
			var counteragentManageView = new CounteragentManageView(_counteragentService);
			counteragentManageView.ShowDialog();
		}

		/// <summary>
		/// Метод показа журнала задач проекта.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowProjectBook(object parameter)
		{
			if (SelectedProject != null)
			{
				var projectBookView = new ProjectBookView(_projectService, _counteragentService, _taskService, SelectedProject);
				projectBookView.ShowDialog();
			}
			else
			{
				MessageBox.Show("Проект не выбран");
			}
		}

		/// <summary>
		/// Вызов диалога подтверждения действия.
		/// </summary>
		/// <param name="type">Тип операции для подтверждения.</param>
		/// <returns>Результат подтверждения</returns>
		private bool ConfirmAction(ProjectDialogConfirmationType type)
		{
			var mbResult = MessageBox.Show($"Вы уверены, что хотите {type.GetDescription()} «{SelectedProject.Title}»?", "Подтвердите действие",
				MessageBoxButton.YesNo, MessageBoxImage.Question);
			return mbResult == MessageBoxResult.Yes;
		}

		/// <summary>
		/// Обновление состояния элементов View.
		/// </summary>
		private void UpdateViewControls()
		{
			if (IsActiveProjectList)
			{
				ProjectListCaption = "Активные проекты";
			}
			else
			{
				ProjectListCaption = "Архив проектов";
			}
		}

	}
}
