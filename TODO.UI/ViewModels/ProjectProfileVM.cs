﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using TODO.BAL.Services;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.UI.Helpers;
using TODO.UI.Views;

namespace TODO.UI.ViewModels
{
	public class ProjectProfileVM : PropertyChangedBase, INotifyDataErrorInfo
	{

		/// <summary>
		/// Признак создания нового проекта.
		/// </summary>
		private readonly bool _newProjectRequested;
		/// <summary>
		/// Переменная, хранящая ссылку на сервис проекта.
		/// </summary>
		private readonly IProjectService _projectService;
		/// <summary>
		/// Переменная, хранящая ссылку на сервис контрагента.
		/// </summary>
		private readonly ICounteragentService _counteragentService;

		private readonly ProjectDTO _project;

		private readonly IFinanceOperationService _financeOperationService;

		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(null, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		//
		// Приватные поля 
		//

		/// <summary>
		/// Перменная, хранящая идентификатор проекта.
		/// </summary>
		private int _id;
		/// <summary>
		/// Перменная, хранящая название проекта.
		/// </summary>
		private string _title;
		/// <summary>
		/// Перменная, хранящая описание проекта.
		/// </summary>
		private string _description;
		/// <summary>
		/// Перменная, хранящая да	ту создания проекта.
		/// </summary>
		private DateTime _dateCreated;
		/// <summary>
		/// Переменная, хранящая планируемую дату выполнения проекта.
		/// </summary>
		private DateTime? _dateDuePlanned;
		/// <summary>
		/// Переменная, хранящая фактическую дату выполнения проекта.
		/// </summary>
		private DateTime? _dateDueFact;
		/// <summary>
		/// Переменная, хранящая статус проекта.
		/// </summary>
		private ProjectStateType _state;
		/// <summary>
		/// Переменная, хранящая планируемый бюджет проекта.
		/// </summary>
		private decimal _plannedBudget;
		/// <summary>
		/// Переменная, хранящая планируемые расходы проекта.
		/// </summary>
		private decimal _plannedCost;
		/// <summary>
		/// Переменная, хранящая планируемую прибыль проекта.
		/// </summary>
		private decimal _plannedIncome;
		/// <summary>
		/// Переменная, хранящая фактическую прибыль проекта.
		/// </summary>
		private decimal _actualIncome;
		/// <summary>
		/// Переменная, хранящая ожидаемые поступления по проекту.
		/// </summary>
		private decimal _expectedIncome;
		/// <summary>
		/// Переменная, хранящая количество потраченных на проекте денег.
		/// </summary>
		private decimal _spentMoney;
		/// <summary>
		/// Переменная, остаток средств проекта.
		/// </summary>
		private decimal _cashBalance;
		/// <summary>
		/// Переменная, хранящая количество завершенных задач в проекте.
		/// </summary>
		private int _tasksDone;
		/// <summary>
		/// Переменная, хранящая количество активных задач в проекте.
		/// </summary>
		private int _tasksActive;
		/// <summary>
		/// Переменная, хранящая сумму затрат по активным задачам проекта.
		/// </summary>
		private decimal _expectedTasksCosts;
		/// <summary>
		/// Переменная, хранящая коллекцию операций прихода денег проекта.
		/// </summary>
		private ICollection<FinanceOperationDTO> _incomeFinance;
		/// <summary>
		/// Переменная, хранящая коллекцию задач проекта.
		/// </summary>
		private ICollection<TaskDTO> _tasks;
		/// <summary>
		/// Переменная, хранящая коллекцию тэгов проекта.
		/// </summary>
		private string _tags;
		/// <summary>
		/// Переменная, хранящая текущего контрагента проекта.
		/// </summary>
		private CounteragentDTO _counteragent;
		/// <summary>
		/// Переменная, хранящая ID текущего контрагента проекта.
		/// </summary>
		private int _currentCounteragentId;
		/// <summary>
		/// Перменная, хранящая сообщение об ошибке.
		/// </summary>
		private string _errorMessage;

		//
		// Свойства объекта Проект.
		//

		/// <summary>
		/// Идентификатор проекта.
		/// </summary>
		public int ID
		{
			get => _id;
			set
			{
				_id = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Название проекта.
		/// </summary>
		public string Title
		{
			get => _title;
			set
			{
				_title = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Контрагент проекта.
		/// </summary>
		public CounteragentDTO Counteragent
		{
			get => _counteragent;
			set
			{
				_counteragent = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// ID текущего контрагента (для UI).
		/// </summary>
		public int CurrentCounteragentId
		{
			get => _currentCounteragentId;
			set
			{
				_currentCounteragentId = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Коллекция всех контрагентов программы.
		/// </summary>
		public ICollection<CounteragentDTO> CounteragentList { get; set; }
		/// <summary>
		/// Описание проекта.
		/// </summary>
		public string Description
		{
			get => _description;
			set
			{
				_description = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Дата создания проекта.
		/// </summary>
		public DateTime DateCreated
		{
			get => _dateCreated;
			set
			{
				_dateCreated = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Планируемая дата выполнения проекта.
		/// </summary>
		public DateTime? DateDuePlanned
		{
			get => _dateDuePlanned;
			set
			{
				if (_dateDuePlanned != value)
				{
					_dateDuePlanned = value;
					NotifyPropertyChanged();
				}
			}
		}	
		/// <summary>
		/// Фактическая дата выполнения проекта.
		/// </summary>
		public DateTime? DateDueFact
		{
			get => _dateDueFact;
			set
			{
				if (_dateDueFact != value)
				{
					_dateDueFact = value;
					NotifyPropertyChanged();
				}
			}
		}
		/// <summary>
		/// Статус проекта.
		/// </summary>
		public ProjectStateType State
		{
			get => _state;
			set
			{
				_state = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Коллекция финансовых операций проекта.
		/// </summary>
		public ICollection<FinanceOperationDTO> IncomeFinance
		{
			get => _incomeFinance;
			set
			{
				_incomeFinance = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Коллекция задач в проекте.
		/// </summary>
		public ICollection<TaskDTO> Tasks
		{
			get => _tasks;
			set
			{
				_tasks = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Коллекция тэгов проекта.
		/// </summary>
		public string Tags
		{
			get => _tags;
			set
			{
				_tags = value;
				NotifyPropertyChanged();
			}
		}
	
		//
		// Финансовые свойства.
		//

		/// <summary>
		/// Планируемый бюджет проекта.
		/// </summary>
		public decimal PlannedBudget
		{
			get => _plannedBudget;
			set
			{
				_plannedBudget = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Планируемый расход по проекту.
		/// </summary>
		public decimal PlannedCost
		{
			get => _plannedCost;
			set
			{
				_plannedCost = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Планируемая прибыль по проекту.
		/// </summary>
		public decimal PlannedIncome
		{
			get => _plannedIncome;
			set
			{
				_plannedIncome = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Фактические поступления по проекту.
		/// </summary>
		public decimal ActualIncome
		{
			get => _actualIncome;
			set
			{
				_actualIncome = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Ожидаемые поступления по проекту.
		/// </summary>
		public decimal ExpectedIncome
		{
			get => _expectedIncome;
			set
			{
				_expectedIncome = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Потрачено денег на проекте.
		/// </summary>
		public decimal SpentMoney
		{
			get => _spentMoney;
			set
			{
				_spentMoney = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Остаток денежных средств на проекте.
		/// </summary>
		public decimal CashBalance
		{
			get => _cashBalance;
			set
			{
				_cashBalance = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Количество задач в проекте.
		/// </summary>
		public int TasksCount
		{
			get => (TasksDone + TasksActive);

		}
		/// <summary>
		/// Завершенных задач в проекте.
		/// </summary>
		public int TasksDone
		{
			get => _tasksDone;
			set
			{
				_tasksDone = value;
				NotifyPropertyChanged();
				NotifyPropertyChanged("TasksCount");
			}
		}
		/// <summary>
		/// Активных задач в проекте.
		/// </summary>
		public int TasksActive
		{
			get => _tasksActive;
			set
			{
				_tasksActive = value;
				NotifyPropertyChanged();
				NotifyPropertyChanged("TasksCount");
			}
		}
		/// <summary>
		/// Ожидаемая сумма затрат по активным задачам.
		/// </summary>
		public decimal ExpectedTasksCosts
		{
			get => _expectedTasksCosts;
			set
			{
				_expectedTasksCosts = value;
				NotifyPropertyChanged();
			}
		}

		//
		// События.
		//

		/// <summary>
		/// Событие возникновения ошибки.
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		/// <summary>
		/// Событие подтверждения изменений, для команды OKCommand.
		/// </summary>
		public event EventHandler OkEvent;

		/// <summary>
		/// Событие отмены изменений, для команды CancelCommand.
		/// </summary>
		public event EventHandler CancelEvent;

		//
		// Команды.
		//

		/// <summary>
		/// Команда подтверждения и внесения изменений в БД.
		/// </summary>
		public RelayCommand OKCommand { get; private set; }
		/// <summary>
		/// Команда отмены изменений.
		/// </summary>
		public RelayCommand CancelCommand { get; private set; }
		/// <summary>
		/// Команда открытия журнала задач.
		/// </summary>
		public RelayCommand FinanceJournalCommand { get; private set; }

		//
		// Конструктор.
		//

		public ProjectProfileVM(ICounteragentService counteragentService, IProjectService projectService, IFinanceOperationService financeOperationService, ProjectDTO project)
		{

			// Устанавливаем сервисы.

			_counteragentService = counteragentService;
			_projectService = projectService;
			_financeOperationService = financeOperationService;
			_project = project;

			// Устанавливаем свойство для создания нового объекта «Проект».

			_newProjectRequested = project == null || project.ID == 0;

			// Подгружаем контрагентов.

			var allCounteragents = counteragentService.ReadAll();
			CounteragentList = new ObservableCollection<CounteragentDTO>(allCounteragents);
			var counteragentsIdList = CounteragentList.Select(c => c.ID).ToList();

			// Устанавливаем команды.

			OKCommand = new RelayCommand(OK);
			CancelCommand = new RelayCommand(Cancel);
			FinanceJournalCommand = new RelayCommand(FinanceJournal);

			// Заполнение, если проект создается.

			if (_newProjectRequested)
			{
				Title = string.Empty;
				Description = string.Empty;
				DateCreated = DateTime.Now;
				DateDuePlanned = DateTime.Today.AddDays(1);
				DateDueFact = null;
				State = ProjectStateType.InProgress;
				return;
			}

			// Заполнение, если проект редактируется.

			ID = project.ID;
			Title = project.Title;
			Counteragent = project.Counteragent;
			CurrentCounteragentId = (Counteragent != null) ? counteragentsIdList.IndexOf(Counteragent.ID) : -1;
			Description = project.Description;
			DateCreated = project.DateCreated;
			DateDuePlanned = project.DateDuePlanned;
			DateDueFact = project.DateDueFact;
			State = project.State;
			IncomeFinance = project.IncomeFinance;
			Tasks = project.Tasks;
			if (project.Tags != null && project.Tags.Count != 0) _tags = GetTagString(project.Tags);
			else _tags = "";
			PlannedBudget = project.PlannedAmount;

			RefreshAll();
	
		}

		//
		// Методы.
		//

		private void RefreshAll()
		{
			PlannedCost = _financeOperationService.GetCurrentPlannedOutcomeAmountforProject(ID);
			PlannedCost = _financeOperationService.GetCurrentOutcomeforProject(ID);
			ActualIncome = _financeOperationService.GetCurrentIncomeforProject(ID);
			ExpectedIncome = PlannedBudget - ActualIncome;
			CashBalance = _financeOperationService.GetAvailableMoneyforProject(ID);
			TasksActive = _project.Tasks.Count(x => x.State == TaskStateType.InProgress);
			TasksDone = _project.Tasks.Count(x => x.State == TaskStateType.Completed);
		}

		private string GetTagString(ICollection<TagDTO> tags)
		{
			if (tags.Count <= 0) return "";

			var tagString = new StringBuilder();
			const string separator = ",";

			foreach (var tag in tags)
			{
				tagString.Append(tag.Name).Append(separator);
			}

			return tagString.Remove(tagString.Length - 1, 1).ToString();
		}

		/// <summary>
		/// Метод сохранения изменений.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void OK(object parameter)
		{
			
			// Проверка планируемой даты выполнения.

			if (DateDuePlanned < DateTime.Today)
			{
				ErrorMessage = "Планируемая дата выполнения проекта не должна быть в прошлом периоде.";
				return;
			}

			if (Counteragent == null)
			{
				ErrorMessage = "У проекта должен быть закрепленный контрагент.";
				return;
			}

			// Создание нового объекта DTO.

			var project = new ProjectDTO()
			{
				ID = ID,
				Title = Title,
				Description = Description,
				Counteragent = Counteragent,
				DateCreated = DateCreated,
				DateDuePlanned = DateDuePlanned,
				DateDueFact = DateDueFact,
				IncomeFinance = IncomeFinance,
				State = State,
				Tasks = Tasks,
				PlannedAmount = PlannedBudget
			};

			if (!string.IsNullOrEmpty(_tags))
			{
				//удаляем существующие теги у задачи
				project.Tags.Clear();
				//Внесение изменений в таблице тегов.
#warning TODO Реализовать внесение изменений в теги через нажатие кнопки, чтобы не вызывать метод лишний раз.             
				if (!_projectService.ChangeTags(ref project, _tags))
				{
					ErrorMessage = _projectService.ErrorMessage;
					return;
				}
			}

			// Вызов соответствующего метода сервиса (создать проект \ обновить данные проекта).

			var result = _newProjectRequested ? _projectService.Create(project) : _projectService.Update(project);

			// Проверка результата.

			if (!result)
			{
				ErrorMessage = _projectService.ErrorMessage;
				return;
			}

			// Успешное выполнение команды, вызвать OKEvent.

			OkEvent?.Invoke(null, null);

		}
		/// <summary>
		/// Метод отмены изменений.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void Cancel(object parameter)
		{
			CancelEvent?.Invoke(null, null);
		}

		/// <summary>
		/// Метод вызова журнала задач проекта.
		/// </summary>
		/// <param name="parameter"></param>
		private void FinanceJournal(object parameter)
		{
			var financeJournalView = new FinancesJournalView(_counteragentService, _financeOperationService, _project);
			if (financeJournalView.ShowDialog() == false)
			{
				RefreshAll();
			}
		}

		#region DataValidation
#warning TODO Пересмотреть механизм валидации полей. INotifyDataErrorInfo вполне подходит, но для него надо будет подправить архитектуру нашей VM

		private Dictionary<string, List<string>> _validationErrors = new Dictionary<string, List<string>>();
		

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
		private void RaiseErrorsChanged(string propertyName)
		{
			ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
		}

		public bool IsValid
		{
			get { return !HasErrors; }
		}

		public bool HasErrors => _validationErrors.Any();

		public IEnumerable GetErrors(string propertyName)
		{
			_validationErrors.TryGetValue(propertyName, out var errorsForProperty);
			return errorsForProperty;
		}

		public void ValidateProperty(object value, [CallerMemberName] string propertyName = null)
		{
			var validationContext = new ValidationContext(this, null, null) { MemberName = propertyName };
			var validationResults = new List<ValidationResult>();
			if (!Validator.TryValidateProperty(value, validationContext, validationResults))
			{
				_validationErrors[propertyName] = validationResults.Select(x => x.ErrorMessage).ToList();
				_errorMessage = string.Join(";", _validationErrors[propertyName]);
				RaiseErrorsChanged(propertyName);
			}
			else if (_validationErrors.ContainsKey(propertyName))
			{
				_validationErrors.Remove(propertyName);
				RaiseErrorsChanged(propertyName);
			}
		}

		#endregion
	}
}