﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Logging;
using TODO.UI.Helpers;
using TODO.UI.Views;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для главного окна MainView
	/// </summary>
	public class MainVM : PropertyChangedBase
	{
		/// <summary>
		/// Список задач. Испльзуется для отображения в ListView главного окна
		/// </summary>
		public ICollection<TaskDTO> TaskList { get; set; }

		/// <summary>
		/// Выбранная задача
		/// </summary>
		public TaskDTO SelectedTask { get; set; }

		/// <summary>
		/// Пемеренная, хранящая заголовок текущего списка задач (активные / архивные).
		/// </summary>
		private string _taskListCaption;
		/// <summary>
		/// Заголовок текущего списка задач (активные / архивные).
		/// </summary>
		public string TaskListCaption
		{
			get => _taskListCaption;
			set
			{
				_taskListCaption = value;
				NotifyPropertyChanged("TaskListCaption");
			}
		}

		/// <summary>
		/// Перменная, хранящая сообщение об ошибке.
		/// </summary>
		private string _errorMessage;
		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(this, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		/// <summary>
		/// Событие возникновения ошибки.
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		/// <summary>
		/// Команда создания задачи.
		/// </summary>
		public RelayCommand CreateTaskCommand { get; private set; }

		/// <summary>
		/// Команда вывода профиля задачи.
		/// </summary>
		public RelayCommand ShowTaskDetailsCommand { get; private set; }

		/// <summary>
		/// Команда завершения задачи.
		/// </summary>
		public RelayCommand CompleteTaskCommand { get; private set; }

		/// <summary>
		/// Команда отмены задачи.
		/// </summary>
		public RelayCommand CancelTaskCommand { get; private set; }

		/// <summary>
		/// Команда удаления задачи.
		/// </summary>
		public RelayCommand DeleteTaskCommand { get; private set; }

		/// <summary>
		/// Команда пересоздания задачи.
		/// </summary>
		public RelayCommand RecreateTaskCommand { get; private set; }

		/// <summary>
		/// Команда вывода активных задач.
		/// </summary>
		public RelayCommand ShowActiveTasksCommand { get; private set; }

		/// <summary>
		/// Команда вывода архивных задач.
		/// </summary>
		public RelayCommand ShowArchiveTasksCommand { get; private set; }

		/// <summary>
		/// Команда вызова окна управления списком контрагентов.
		/// </summary>
		public RelayCommand ShowCounteragentsManageCommand { get; private set; }

		/// <summary>
		/// Команда вызова окна управления списком проектов.
		/// </summary>
		public RelayCommand ShowProjectManageCommand { get; private set; }

		/// <summary>
		/// Команда выхода из приложения
		/// </summary>
		public RelayCommand ApplicationExitCommand { get; private set; }

		//
		// Состояние интерфейса
		//

		/// <summary>
		/// Переменная, хранящая признак отображения активного списка задач.
		/// </summary>
		private bool _isActiveTaskList;

		/// <summary>
		/// Признак отображения активного списка задач.
		/// Используется для задания состояния элементов View.
		/// </summary>
		public bool IsActiveTaskList
		{
			get => _isActiveTaskList;
			private set
			{
				_isActiveTaskList = value;
				NotifyPropertyChanged("IsActiveTaskList");
				NotifyPropertyChanged("IsNotActiveTaskList");
				UpdateViewControls();
			}
		}

		/// <summary>
		/// Возвращает значение, обратное признаку отображения активного списка задач (!_isActiveTaskList).
		/// </summary>
		public bool IsNotActiveTaskList
		{
			get => !_isActiveTaskList;
		}

		/// <summary>
		/// Поле доступа к TaskService
		/// </summary>
		private ITaskService _taskService;

		/// <summary>
		/// Поле доступа к CounteragentService
		/// </summary>
		private ICounteragentService _counteragentService;

		/// <summary>
		/// Поле доступа к ProjectService
		/// </summary>
		private readonly IProjectService _projectService;
		private readonly ILogger _logger;
		private readonly IFinanceOperationService _financeOperation;

		/// <summary>
		/// Конструктор.
		/// </summary>
		public MainVM(ITaskService tasks, ICounteragentService counteragents, IProjectService projects, IFinanceOperationService financeOperation ,ILogger logger)
		{
			_taskService = tasks;
			_counteragentService = counteragents;
			_projectService = projects;
			_financeOperation = financeOperation;
			_logger = logger;

			// Инициализация команд.
			CreateTaskCommand = new RelayCommand(CreateTask);
			DeleteTaskCommand = new RelayCommand(DeleteTask);
			ShowTaskDetailsCommand = new RelayCommand(ShowTaskDetails);
			CompleteTaskCommand = new RelayCommand(CompleteTask, (x) => IsActiveTaskList);
			CancelTaskCommand = new RelayCommand(CancelTask, (x) => IsActiveTaskList);
			RecreateTaskCommand = new RelayCommand(RecreateTask, (x) => !IsActiveTaskList);
			ShowActiveTasksCommand = new RelayCommand(ShowActiveTasks);
			ShowArchiveTasksCommand = new RelayCommand(ShowArchiveTasks);
			ShowCounteragentsManageCommand = new RelayCommand(ShowCounteragentsManage);
			ShowProjectManageCommand = new RelayCommand(ShowProjectManage);
			ApplicationExitCommand = new RelayCommand(x => ((Window)x).Close());
			ShowActiveTasks(null);
		}

		/// <summary>
		/// Метод создания задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CreateTask(object parameter)
		{
			var taskProfile = new TaskProfileView(_counteragentService, _taskService);
			if (taskProfile.ShowDialog() == false)
			{
				return;
			}

			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowActiveTasks(null);
		}

		/// <summary>
		/// Метод создания задачи. Аналогичен методу CreateTask,
		/// только в качестве параметра для TaskProfileView передается SelectedTask.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowTaskDetails(object parameter)
		{
			if (SelectedTask == null)
			{
				MessageBox.Show("Не выбрана задача");
				return;
			}

			var taskProfile = new TaskProfileView(_counteragentService, _taskService, SelectedTask);
			if (taskProfile.ShowDialog() == false)
			{
				return;
			}

			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowActiveTasks(null);
		}

		/// <summary>
		/// Метод завершения задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CompleteTask(object parameter)
		{
			if (SelectedTask == null)
			{
				MessageBox.Show("Не выбрана задача");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(TaskDialogConfirmationType.Complate))
			{
				var result = _taskService.SetComplete(SelectedTask.ID);
				if (!result)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}

				// Удаляем задачу из локального списка, чтобы не перечитывать список из БД.

				TaskList.Remove(SelectedTask);
				NotifyPropertyChanged("TaskList");
			}
		}

		/// <summary>
		/// Метод удаления задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void DeleteTask(object parameter)
		{
			if (SelectedTask == null)
			{
				MessageBox.Show("Не выбрана задача");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(TaskDialogConfirmationType.Delete))
			{
				var result = _taskService.Delete(SelectedTask.ID);
				if (!result)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}

				// Удаляем задачу из локального списка, чтобы не перечитывать список из БД.

				TaskList.Remove(SelectedTask);
				NotifyPropertyChanged("TaskList");
			}
		}


		/// <summary>
		/// Метод отмены задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CancelTask(object parameter)
		{
			if (SelectedTask == null)
			{
				MessageBox.Show("Не выбрана задача");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(TaskDialogConfirmationType.Cancel))
			{
				var result = _taskService.SetCancel(SelectedTask.ID);
				if (!result)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}

				// Удаляем задачу из локального списка, чтобы не перечитывать список из БД.

				TaskList.Remove(SelectedTask);
				NotifyPropertyChanged("TaskList");
			}
		}

		/// <summary>
		/// Метод пересоздания задачи в архиве.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void RecreateTask(object parameter)
		{
			if (SelectedTask == null)
			{
				MessageBox.Show("Не выбрана задача");
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(TaskDialogConfirmationType.Recreate))
			{
				SelectedTask = _taskService.Recreate(SelectedTask);
				ShowTaskDetails(SelectedTask);
			}
		}

		/// <summary>
		/// Метод вывода активных задач.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowActiveTasks(object parameter)
		{
			var result = _taskService.ReadActive();
			if (result == null)
			{
				ErrorMessage = _taskService.ErrorMessage;
				return;
			}

			TaskList = new ObservableCollection<TaskDTO>(result);
			NotifyPropertyChanged("TaskList");

			IsActiveTaskList = true;
		}

		/// <summary>
		/// Метод вывода архивных задач.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowArchiveTasks(object parameter)
		{
			var result = _taskService.ReadArchive();
			if (result == null)
			{
				ErrorMessage = _taskService.ErrorMessage;
				return;
			}

			TaskList = new ObservableCollection<TaskDTO>(result);
			NotifyPropertyChanged("TaskList");

			IsActiveTaskList = false;
		}

		/// <summary>
		/// Метод показа окна управления контрагентами.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowCounteragentsManage(object parameter)
		{
			var counteragentManageView = new CounteragentManageView(_counteragentService);
			counteragentManageView.ShowDialog();
		}

		/// <summary>
		/// Метод показа окна управления проектами.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowProjectManage(object parameter)
		{
			var projectManageView = new ProjectManageView(_taskService,_counteragentService,_projectService, _financeOperation ,_logger);
			projectManageView.ShowDialog();
		}

		/// <summary>
		/// Обновление состояния элементов View.
		/// </summary>
		private void UpdateViewControls()
		{
			if (IsActiveTaskList)
			{
				TaskListCaption = "Активные задачи";
			}
			else
			{
				TaskListCaption = "Архив задач";
			}
		}

		/// <summary>
		/// Вызов диалога подтверждения действия.
		/// </summary>
		/// <param name="type">Тип операции для подтверждения.</param>
		/// <returns>Результат подтверждения</returns>
		private bool ConfirmAction(TaskDialogConfirmationType type)
		{
			var mbResult = MessageBox.Show($"Вы уверены, что хотите {type.GetDescription()} «{SelectedTask.Title}»?", "Подтвердите действие",
				MessageBoxButton.YesNo, MessageBoxImage.Question);
			return mbResult == MessageBoxResult.Yes;
		}

	}
}
