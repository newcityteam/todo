﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.UI.Helpers;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для окна профиля задачи TaskProfileView.
	/// </summary>
	public class TaskProfileVM : PropertyChangedBase, INotifyDataErrorInfo
	{
		/// <summary>
		/// Признак создания новой задачи.
		/// Если в конструктор передается null - будет создана новая задача.
		/// </summary>
		private readonly bool _newTaskRequested;

		/// <summary>
		/// Поле доступа к CounteragentService
		/// </summary>
		private ICounteragentService _counteragentService;

		/// <summary>
		/// Поле доступа к TaskService
		/// </summary>
		private ITaskService _taskService;

		private string GetTagString(ICollection<TagDTO> tags)
		{
			if (tags.Count>0)
			{
				StringBuilder tagString = new StringBuilder();
				var separator = ",";
				foreach (var tag in tags)
				{
					tagString.Append(tag.Name).Append(separator);
				}

				return tagString.Remove(tagString.Length - 1, 1).ToString();
			}
			else return "";
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="task">Задача для отображения. В случае null - будет создана новая задача.</param>
		public TaskProfileVM(TaskDTO task, ProjectDTO project, ICounteragentService counteragents, ITaskService tasks)
		{
			// Инициализация команд.
			_counteragentService = counteragents;
			_taskService = tasks;
			// Добавляем пустого контрагента в начало списка. Если он выбран, сохраняем задачу без контрагента.
			var result = new List<CounteragentDTO> { new CounteragentDTO { ID = -1 } };
			result.AddRange(_counteragentService.ReadAll());
			CounteragentList = new ObservableCollection<CounteragentDTO>(result);
			var IDList = CounteragentList.Select(counteragent => counteragent.ID).ToList();
			OKCommand = new RelayCommand(OK);
			CancelCommand = new RelayCommand(Cancel);
			SetDirectionTypeCommand = new RelayCommand(SetDirectionType);

			// Установка признака создания новой задачи.
			// task может быть null, если создаётся новая задача.
			// task.ID == 0, если задача копируется.
			_newTaskRequested = (task == null) || task.ID == 0;

			//Установка признака, принадлежит ли задача проекту
			ProjectOfTask = project;
			IsInProject = ProjectOfTask != null;
			WindowTitle = IsInProject ? "Задача проекта " + ProjectOfTask.Title : "Задача";

			if (task == null)
			{
				// для новых задач время выполнения по умолчанию - 1 день.

				Title = string.Empty;
				DateCreated = DateTime.Now;
				DateDue = DateCreated.AddDays(1);
				Direction = TaskDirectionType.Outcome;
				IsOutcome = true;
				CurrentCounteragentID = -1;
				State = TaskStateType.InProgress;
				PlannedAmount = 0;
				return;
			}

			// Заполнение окна данными задачи.

			ID = task.ID;
			Title = task.Title;
			Description = task.Description;
			DateCreated = task.DateCreated;
			DateDue = task.DateDue;
			DateDueUnlimit = DateDue == null;
			Direction = task.Direction;
			State = task.State;
			PlannedAmount = task.PlannedAmount;
			Counteragent = task.Counteragent;
			CurrentCounteragentID = (Counteragent != null) ? IDList.IndexOf(Counteragent.ID) : -1;
			if (task.Tags != null && task.Tags.Count != 0) _tags = GetTagString(task.Tags);
			else _tags = "";

			// Установка типа задачи в окне для RadioButton.
			IsIncome = Direction == TaskDirectionType.Income;
			IsOutcome = Direction == TaskDirectionType.Outcome;
		}

		#region Свойства для отображения модели TaskDTO

		private decimal _plannedAmount;

		public decimal PlannedAmount
		{
			get => _plannedAmount;
			set { _plannedAmount = value; NotifyPropertyChanged("PlannedAmount"); }
		}
		
		private string _tags;

		public string Tags
		{
			get => _tags;
			set { _tags = value; NotifyPropertyChanged("Tags");}
		}

		/// <summary>
		/// Переменная, хранящая сведения о статусе задачи
		/// </summary>
		private TaskStateType _state;

		public TaskStateType State
		{
			get => _state;
			set { _state = value; NotifyPropertyChanged("State"); }
		}
		/// <summary>
		/// Переменная, устанавливающая заголовок окна
		/// </summary>
		private string _windowTitle;

		public string WindowTitle
		{
			get => _windowTitle;
			set { _windowTitle = value; NotifyPropertyChanged("WindowTitle"); }
		}

		/// <summary>
		/// Переменная, хранящая идентификатор задачи.
		/// </summary>
		private int _id;
		/// <summary>
		/// Идентификатор задачи.
		/// </summary>
		public int ID
		{
			get => _id;
			set { _id = value; NotifyPropertyChanged("ID"); }
		}

		/// <summary>
		/// Переменная, хранящая название задачи задачи.
		/// </summary>
		private string _title;

		/// <summary>
		/// Название задачи.
		/// </summary>
		[Required(ErrorMessage = @"Поле ""Название"" обязательно к заполнению")]
		public string Title
		{
			get => _title;
			set
			{
				_title = value;
				NotifyPropertyChanged("Title");
				ValidateProperty(value, "Title");
			}
		}

		/// <summary>
		/// Переменная, хранящая описание задачи.
		/// </summary>
		private string _description;
		/// <summary>
		/// Описание задачи.
		/// </summary>
		public string Description
		{
			get => _description;
			set { _description = value; NotifyPropertyChanged("Description"); }
		}

		/// <summary>
		/// Переменная, хранящая дату создания задачи.
		/// </summary>
		private DateTime _dateCreated;
		/// <summary>
		/// Дата создания задачи.
		/// </summary>
		public DateTime DateCreated
		{
			get => _dateCreated;
			set { _dateCreated = value; NotifyPropertyChanged("DateCreated"); }
		}

		/// <summary>
		/// Переменная, хранящая дату выполнения задачи.
		/// </summary>
		private DateTime? _dateDue;
		/// <summary>
		/// Дата выполнения задачи.
		/// </summary>
		public DateTime? DateDue
		{
			get => _dateDue;
			set
			{
				if (_dateDue != value)
				{
					_dateDue = value;
					NotifyPropertyChanged("DateDue");
				}
			}
		}

		/// <summary>
		/// Переменная, хранящщая ключ бессрочного выполнения задачи.
		/// </summary>
		private bool _dateDueUnlimit;
		/// <summary>
		/// Признак бессрочного выполнения задачи.
		/// </summary>
		public bool DateDueUnlimit
		{
			get => _dateDueUnlimit;
			set
			{
				_dateDueUnlimit = value;
				SetDateDueValue(value);
				NotifyPropertyChanged("DateDueUnlimit");
			}
		}

		/// <summary>
		/// Перменная, хранящая направление задачи (входящая / исходящая).
		/// </summary>
		private TaskDirectionType _direction;
		/// <summary>
		/// Направление задачи (входящая / исходящая).
		/// </summary>
		public TaskDirectionType Direction
		{
			get => _direction;
			set { _direction = value; NotifyPropertyChanged("Direction"); }
		}

		/// <summary>
		/// Изменяет значение DateDue в зависимости от входящего параметра.
		/// Используется в связке с DateDueUnlimit, чтобы ViewModel корректно изменяла данные в View.
		/// </summary>
		/// <param name="unlimitCheckButtonValue">Значение checkButton DateDueUnlimit.</param>
		private void SetDateDueValue(bool unlimitCheckButtonValue)
		{
			// Устанавливаем DateDue null, если наша задача помечена как «без срока».
			if (unlimitCheckButtonValue)
			{
				DateDue = null;
			}
			// Когда мы снимаем галку с «без срока», дата выполнения задачи устанавливается завтрашним числом.
			else
			{
				if (DateDue == null)
				{
					DateDue = DateTime.Today.AddDays(1);
				}
			}
		}
		#endregion

		#region Поля и свойства для отображения данных контрагента
		/// <summary>
		/// Список имеющихся контрагентов.
		/// </summary>
		public ICollection<CounteragentDTO> CounteragentList { get; set; }

		/// <summary>
		/// Перменная, хранящая выбранного контрагента для задачи.
		/// </summary>
		private CounteragentDTO _counteragent;

		/// <summary>
		/// Выбранный контрагент для задачи.
		/// </summary>
		public CounteragentDTO Counteragent
		{
			get => _counteragent;
			set
			{
				// Если выбран пустой контрагент, меняем его на null.
				_counteragent = value?.ID == -1 ? null : value;
				IsSelected = Counteragent != null;
				IsLegal = Counteragent?.Type == CounteragentType.Legal;
				NotifyPropertyChanged("Counteragent");
			}
		}

		/// <summary>
		/// Переменная, хранящая значение типа контрагента (является ли он юр. лицом).
		/// </summary>
		private bool _isLegal;

		/// <summary>
		/// Признак юридического лица контрагента.
		/// </summary>
		public bool IsLegal
		{
			get => _isLegal;
			set { _isLegal = value; NotifyPropertyChanged("IsLegal"); }}

		/// <summary>
		/// Переменная, хранящая индекс контрагента, нужен только для UI.
		/// </summary>
		private int _currentCounteragentID;
		/// <summary>
		/// Индекс контрагента, нужен только для UI.
		/// </summary>
		public int CurrentCounteragentID
		{
			get => _currentCounteragentID;
			set { _currentCounteragentID = value; NotifyPropertyChanged("CounteragentID"); }
		}
		#endregion

		#region Данные проекта
		/// <summary>
		/// Переменная, показывающая, относится ли данная задача к какому-либо проекту.
		/// </summary>
		private bool _isInProject;
		/// <summary>
		/// Признак выбранной задачи.
		/// </summary>
		public bool IsInProject
		{
			get => _isInProject;
			set { _isInProject = value; NotifyPropertyChanged("IsInProject"); }
		}

		/// <summary>
		/// Переменная, хранящая ДТО проекта, к которому относится данная задача
		/// </summary>
		public ProjectDTO ProjectOfTaskOfTask;
		public ProjectDTO ProjectOfTask
		{
			get => ProjectOfTaskOfTask;
			set
			{
				ProjectOfTaskOfTask = value;
				IsInProject = ProjectOfTask != null;
				NotifyPropertyChanged("Counteragent");
			}
		}
		#endregion

		#region События управления состоянием View.

		/// <summary>
		/// Событие подтверждения изменений, для команды OKCommand.
		/// </summary>
		public event EventHandler OKEvent;

		/// <summary>
		/// Событие отмены изменений, для команды CancelCommand.
		/// </summary>
		public event EventHandler CancelEvent;

		/// <summary>
		/// Событие возникновения ошибки.
		/// </summary>
		public event StringMessageHandler ErrorEvent;
		#endregion

		#region Команды
		/// <summary>
		/// Команда подтверждения и внесения изменений в БД.
		/// </summary>
		public RelayCommand OKCommand { get; private set; }

		/// <summary>
		/// Команда отмены изменений.
		/// </summary>
		public RelayCommand CancelCommand { get; private set; }

		/// <summary>
		/// Команда выбора типа задачи.
		/// </summary>
		public RelayCommand SetDirectionTypeCommand { get; private set; }
		#endregion

		#region Признаки задачи
		/// <summary>
		/// Переменная, хранящая значение «выбрана ли задача».
		/// </summary>
		private bool _isSelected;
		/// <summary>
		/// Признак выбранной задачи.
		/// </summary>
		public bool IsSelected
		{
			get => _isSelected;
			set { _isSelected = value; NotifyPropertyChanged("IsSelected"); }
		}

		/// <summary>
		/// Свойство установки типа задачи. Для RadioButton.
		/// </summary>
		private bool _isIncome;
		public bool IsIncome
		{
			get => _isIncome;
			set
			{
				_isIncome = value;
				NotifyPropertyChanged("IsIncome");
			}
		}

		/// <summary>
		/// Переменная, хранящая свойство установки типа задачи. Для RadioButton.
		/// </summary>
		private bool _isOutcome;

		/// <summary>
		/// Свойство установки типа задачи. Для RadioButton.
		/// </summary>
		public bool IsOutcome
		{
			get => _isOutcome;
			set
			{
				_isOutcome = value;
				NotifyPropertyChanged("IsOutcome");
			}
		}

		/// <summary>
		/// Метод установки типа задачи(входящая/исходящая).
		/// </summary>
		private void SetDirectionType(object parameter)
		{
			var type = parameter as string;
			if (type == TaskDirectionType.Income.GetDescription())
			{
				Direction = TaskDirectionType.Income;
			}
			else if (type == TaskDirectionType.Outcome.GetDescription())
			{
				Direction = TaskDirectionType.Outcome;
			}
		}
		#endregion

		#region Сообщение об ошибке
		/// <summary>
		/// Переменная, хранящая сообщение об ошибке.
		/// </summary>
		private string _errorMessage;
		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(null, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}
		#endregion

		#region Обработка изменений
		/// <summary>
		/// Метод подтверждения и внесения изменений в БД.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void OK(object parameter)
		{
			if (!HasErrors)
			{
				if (DateDue < DateTime.Today)
				{
					ErrorMessage = "Дата выполнения задачи не должна быть в прошлом периоде";
					return;
				}

				// установка типа задачи для записи в БД.

				Direction = (IsIncome) ? TaskDirectionType.Income : TaskDirectionType.Outcome;

				// Создание объекта DTO для отправки в бизнес-слой BAL.

				var task = new TaskDTO
				{
					ID = ID,
					Title = Title,
					Description = Description,
					DateCreated = DateCreated,
					DateDue = (DateDueUnlimit) ? null : DateDue,
					Direction = Direction,
					State = State,
					Counteragent = Counteragent,
					Project = ProjectOfTask,
					PlannedAmount = PlannedAmount
				};
				
				if (_tags!=null && _tags!="")
				{ 
				//удаляем существующие теги у задачи
				task.Tags.Clear();
				//Внесение изменений в таблице тегов.
#warning TODO Реализовать внесение изменений в теги через нажатие кнопки, чтобы не вызывать метод лишний раз.             
				if (!_taskService.ChangeTags(ref task, _tags))
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}
				}
				// Создание или обновление объекта в БД.
				var result = (_newTaskRequested) ? _taskService.Create(task) : _taskService.Update(task);
				
				// Неуспешное выполнение команды.

				if (!result)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}

				// Успешное выполнение команды, вызвать OKEvent.
				OKEvent?.Invoke(null, null);

			}
			else
			{
				ErrorEvent?.Invoke(null, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		/// <summary>
		/// Метод отмены изменений.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void Cancel(object parameter)
		{
			CancelEvent?.Invoke(null, null);
		}
		#endregion
		
		#region DataValidation
#warning TODO Пересмотреть механизм валидации полей. INotifyDataErrorInfo вполне подходит, но для него надо будет подправить архитектуру нашей VM

		private Dictionary<string, List<string>> _validationErrors = new Dictionary<string, List<string>>();

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
		private void RaiseErrorsChanged(string propertyName)
		{
			ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
		}

		public bool IsValid
		{
			get { return !HasErrors; }
		}

		public bool HasErrors => _validationErrors.Any();

		public IEnumerable GetErrors(string propertyName)
		{
			_validationErrors.TryGetValue(propertyName, out List<string> errorsForProperty);
			return errorsForProperty;
		}

		public void ValidateProperty(object value, [CallerMemberName] string propertyName = null)
		{
			var validationContext = new ValidationContext(this, null, null) { MemberName = propertyName };
			var validationResults = new List<ValidationResult>();
			if (!Validator.TryValidateProperty(value, validationContext, validationResults))
			{
				_validationErrors[propertyName] = validationResults.Select(x => x.ErrorMessage).ToList();
				_errorMessage = string.Join(";", _validationErrors[propertyName]);
				RaiseErrorsChanged(propertyName);
			}
			else if (_validationErrors.ContainsKey(propertyName))
			{
				_validationErrors.Remove(propertyName);
				RaiseErrorsChanged(propertyName);
			}
		}

		#endregion
	}
}
