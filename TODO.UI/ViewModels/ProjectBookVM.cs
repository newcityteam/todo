﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.UI.Helpers;
using TODO.UI.Views;

namespace TODO.UI.ViewModels
{
	class ProjectBookVM: PropertyChangedBase
	{
		private ITaskService _taskService;
		private ICounteragentService _counteragentService;
		private IProjectService _projectService;

		public ProjectBookVM(ProjectDTO project, ITaskService taskService, ICounteragentService counteragentService, IProjectService projectService)
		{
			Project = project;
			_taskService = taskService;
			_counteragentService = counteragentService;
			_projectService = projectService;

			#region Инициализация команд
			// Инициализация команд.
			CreateTaskCommand = new RelayCommand(CreateTask);
			DeleteTaskCommand = new RelayCommand(DeleteTask);
			ShowTaskDetailsCommand = new RelayCommand(ShowTaskDetails);
			CompleteTaskCommand = new RelayCommand(CompleteTask);
			ShowActiveTasksCommand = new RelayCommand(ShowActiveTasks);
			ShowAllTasksCommand = new RelayCommand(ShowAllTasks);
			ShowActiveTasksCommand = new RelayCommand(ShowActiveTasks);
			#endregion
			
			ShowAllTasks(null);

			ProjectTasksCaption = $"Задачи проекта \"{Project.Title}\"";
		}

		private string _projectTasksCaption;
		public string ProjectTasksCaption
		{
			get
			{
				return _projectTasksCaption;
			}
			set
			{
				_projectTasksCaption = value;
				NotifyPropertyChanged("ProjectTasksCaption");
			}
		}

		#region Поля для привязки Binding
		/// <summary>
		/// Общее кол-во задач выбранного проекта
		/// </summary>
		private int _taskAmount;

		public int TasksAmount
		{
			get => (CompleteTasksAmount+ActiveTasksAmount);
		}

		/// <summary>
		/// Кол-во завершенных задач проекта
		/// </summary>
		private int _completeTaskAmount;

		public int CompleteTasksAmount
		{
			get => _completeTaskAmount;
			set
			{
				_completeTaskAmount = value;
				NotifyPropertyChanged("CompleteTasksAmount");
				NotifyPropertyChanged("TasksAmount");
			}
		}

		/// <summary>
		/// Кол-во активных задач проекта
		/// </summary>
		private int _activeTaskAmount;

		public int ActiveTasksAmount
		{
			get => _activeTaskAmount;
			set
			{
				_activeTaskAmount = value;
				NotifyPropertyChanged("ActiveTasksAmount");
				NotifyPropertyChanged("TasksAmount");
			}
		}

		private void CountTaskAmount()
		{
			ActiveTasksAmount = Project.Tasks
				.Count(x => x.State == TaskStateType.InProgress);
			CompleteTasksAmount = Project.Tasks
				.Count(x => x.State == TaskStateType.Completed);
		}

		/// <summary>
		/// Выбранный проект
		/// </summary>
		public ProjectDTO Project { get; private set; }

		/// <summary>
		/// Список задач. Испльзуется для отображения в ListView главного окна
		/// </summary>
		public ICollection<TaskDTO> TaskList { get; set; }

		/// <summary>
		/// Выбранная задача
		/// </summary>
		public TaskDTO SelectedTask { get; set; }
#endregion

		#region	Поля, содержащие команды для привязки Binding
		/// <summary>
		/// Команда создания задачи.
		/// </summary>
		public RelayCommand CreateTaskCommand { get; private set; }

		/// <summary>
		/// Команда вывода профиля задачи.
		/// </summary>
		public RelayCommand ShowTaskDetailsCommand { get; private set; }

		/// <summary>
		/// Команда завершения задачи.
		/// </summary>
		public RelayCommand CompleteTaskCommand { get; private set; }

		/// <summary>
		/// Команда удаления задачи.
		/// </summary>
		public RelayCommand DeleteTaskCommand { get; private set; }

		/// <summary>
		/// Команда вывода активных и завершенных задач.
		/// </summary>
		public RelayCommand ShowAllTasksCommand { get; private set; }

		/// <summary>
		/// Команда вывода только активных задач.
		/// </summary>
		public RelayCommand ShowActiveTasksCommand { get; private set; }
		#endregion

		#region Признак отображения активных и завершенных задач 
		/// <summary>
		/// Переменная, хранящая признак отображения активных и завершенных задач.
		/// </summary>
		private bool _isAllTaskList;

		/// <summary>
		/// Признак отображения активного списка задач.
		/// Используется для задания состояния элементов View.
		/// </summary>
		public bool IsAllTaskList
		{
			get => _isAllTaskList;
			set
			{
				_isAllTaskList = value;
				NotifyPropertyChanged("IsAllTaskList");
			}
		}
		#endregion

		#region Методы создания и редактирования задачи
		/// <summary>
		/// Метод создания задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CreateTask(object parameter)
		{
			var taskProfile = new TaskProfileView(_counteragentService, _taskService, null,Project);
			if (taskProfile.ShowDialog() == false)
			{
				return;
			}
			//Обновление данных проекта
			Project=_projectService.ReadProject(Project.ID);
			CountTaskAmount();
			ShowAllTasks(null);
		}

		/// <summary>
		/// Метод создания задачи. Аналогичен методу CreateTask,
		/// только в качестве параметра для TaskProfileView передается SelectedTask.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowTaskDetails(object parameter)
		{
			if (SelectedTask == null)
			{
				return;
			}

			var taskProfile = new TaskProfileView(_counteragentService, _taskService, SelectedTask,Project);
			if (taskProfile.ShowDialog() == false)
			{
				return;
			}
			//Обновление данных проекта
			Project = _projectService.ReadProject(Project.ID);
			ShowAllTasks(null);
		}
		#endregion

		#region Методы изменения статуса задачи
		/// <summary>
		/// Метод завершения задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void CompleteTask(object parameter)
		{
			if (SelectedTask == null)
			{
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(TaskDialogConfirmationType.Complate))
			{
				var result = _taskService.SetComplete(SelectedTask.ID);
				if (!result)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}

				Project = _projectService.ReadProject(Project.ID);
				ShowAllTasks(null);
			}
		}

		/// <summary>
		/// Метод удаления задачи.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void DeleteTask(object parameter)
		{
			if (SelectedTask == null)
			{
				return;
			}

			// Просим пользователя подтвердить действие. В случае отмены выделение задания не сбрасывается.

			if (ConfirmAction(TaskDialogConfirmationType.Delete))
			{
				var result = _taskService.Delete(SelectedTask.ID);
				if (!result)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}

				Project = _projectService.ReadProject(Project.ID);
				ShowAllTasks(null);
			}
		}
		#endregion

		#region Методы вывода задач
		/// <summary>
		/// Метод вывода активных и завершенных задач.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowAllTasks(object parameter)
		{
			if (Project != null)
			{
				var result = Project.Tasks
					.Where(x => x.State == TaskStateType.InProgress || 
					            x.State==TaskStateType.Completed)
					.OrderByDescending(x => x.State)
				 	.ThenByDescending(x => x.DateCreated);
				if (result == null)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}
				TaskList = new ObservableCollection<TaskDTO>(result);
				NotifyPropertyChanged("TaskList");
				CountTaskAmount();
				IsAllTaskList = true;
			}
			else
			{
				ErrorMessage = "Проект не выбран";
			}
		}

		/// <summary>
		/// Метод вывода только активных задач.
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand.</param>
		private void ShowActiveTasks(object parameter)
		{

			if (Project != null)
			{
				var result = Project.Tasks
					.Where(x => x.State == TaskStateType.InProgress);
				if (result == null)
				{
					ErrorMessage = _taskService.ErrorMessage;
					return;
				}
				TaskList = new ObservableCollection<TaskDTO>(result);
				NotifyPropertyChanged("TaskList");
				CountTaskAmount();
				IsAllTaskList = false;
			}
			else
			{
				ErrorMessage = "Проект не выбран";
			}
			IsAllTaskList = false;
		}
		#endregion

		#region Вывод сообщений об ошибке 
		/// <summary>
		/// Переменная, хранящая сообщение об ошибке.
		/// </summary>
		private string _errorMessage;

		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		public string ErrorMessage
		{
			get => _errorMessage;
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(this, new StringMessageEventArgs {Message = ErrorMessage});
			}
		}

		/// <summary>
		/// Событие возникновения ошибки.
		/// </summary>
		public event StringMessageHandler ErrorEvent;
		#endregion

		#region Вызов диалога подтверждения действия
		/// <summary>
		/// Вызов диалога подтверждения действия.
		/// </summary>
		/// <param name="type">Тип операции для подтверждения.</param>
		/// <returns>Результат подтверждения</returns>
		private bool ConfirmAction(TaskDialogConfirmationType type)
		{
			var mbResult = MessageBox.Show($"Вы уверены, что хотите {type.GetDescription()} «{SelectedTask.Title}»?", "Подтвердите действие",
				MessageBoxButton.YesNo, MessageBoxImage.Question);
			return mbResult == MessageBoxResult.Yes;
		}
#endregion
	}
}


