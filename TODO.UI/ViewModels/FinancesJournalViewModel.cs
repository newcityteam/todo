﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.Helpers;
using TODO.UI.Views;

namespace TODO.UI.ViewModels
{
	public class FinancesJournalViewModel : PropertyChangedBase
	{
		private readonly ICounteragentService _counteragentService;
		private readonly IFinanceOperationService _financeOperationService;
		private readonly ILogger _logger;
		private readonly ProjectDTO _project;

		#region Свойства

		private ICollection<FinanceOperationDTO> _financeOperationList;
		private FinanceOperationDTO _selectedFinanceOperation;
		/// <summary>
		/// Полный список всех приходов по проекту.
		/// </summary>
		public ICollection<FinanceOperationDTO> FinanceOperationList
		{
			get => _financeOperationList;
			set
			{
				_financeOperationList = value;
				NotifyPropertyChanged();
			}
		}
		/// <summary>
		/// Выбранная операция прихода.
		/// </summary>
		public FinanceOperationDTO SelectedFinanceOperation
		{
			get => _selectedFinanceOperation;
			set
			{
				_selectedFinanceOperation = value;
				NotifyPropertyChanged();
			}
		}


		#endregion

		#region Команды

		public RelayCommand CreateFinanceOperationCommand { get; private set; }

		#endregion

		#region Конструктор

		public FinancesJournalViewModel(ICounteragentService counteragent, IFinanceOperationService financeOperation, ProjectDTO project)
		{
			_counteragentService = counteragent;
			_financeOperationService = financeOperation;
			_project = project;

			
			CreateFinanceOperationCommand = new RelayCommand(CreateFinanceOperation);

			ShowFinanceOpereationList(null);
		}

		#endregion

		#region Методы
		/// <summary>
		///	Метод загрузки всех операций прихода денег по проекту.
		/// </summary>
		private void ShowFinanceOpereationList(object parameter)
		{
			var result = _financeOperationService.GetEnumerableIncomeFinanceOperationforProject(_project.ID);
			FinanceOperationList = new ObservableCollection<FinanceOperationDTO>(result);
		}

		private void CreateFinanceOperation(object parameter)
		{
			var financeOperationProfileView = new FinanceOperationProfileView(_financeOperationService, _logger, _project);
			if (financeOperationProfileView.ShowDialog() != true)
			{
				//return;
			}
			ShowFinanceOpereationList(null);
		}

		#endregion

	}
}