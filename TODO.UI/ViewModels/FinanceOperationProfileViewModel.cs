﻿using System;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.Helpers;

namespace TODO.UI.ViewModels
{
	public class FinanceOperationProfileViewModel : PropertyChangedBase
	{
		private readonly IFinanceOperationService _financeOperation;
		private readonly ILogger _logger;
		private readonly ProjectDTO _project;
		private decimal _amount;
		private string _description;

		public decimal Amount
		{
			get => _amount;
			set
			{
				_amount = value;
				NotifyPropertyChanged();
			}
		}

		public string Description
		{
			get => _description;
			set
			{
				_description = value;
				NotifyPropertyChanged();
			}
		}

		public RelayCommand OKCommand { get; private set; }
		public RelayCommand CancelCommand { get; private set; }

		/// <summary>
		/// Событие подтверждения изменений, для команды OKCommand.
		/// </summary>
		public event EventHandler OKEvent;

		/// <summary>
		/// Событие отмены изменений, для команды CancelCommand.
		/// </summary>
		public event EventHandler CancelEvent;



		public FinanceOperationProfileViewModel(IFinanceOperationService financeOperationService, ILogger logger,
			ProjectDTO project)
		{
			_financeOperation = financeOperationService;
			_logger = logger;
			_project = project;

			OKCommand = new RelayCommand(OK);
			CancelCommand = new RelayCommand(Cancel);
		}

		private void OK(object parameter)
		{

			var newFinanceOperation = new FinanceOperationDTO
			{
				AmountIncome = Amount,
				Date = DateTime.Now,
				Description = Description,
				Project = _project,
				ProjectID = _project.ID
			};
			_financeOperation.Create(newFinanceOperation);

			OKEvent?.Invoke(null, null);
		}

		private void Cancel(object parameter)
		{
			CancelEvent?.Invoke(null, null);
		}

	}
}