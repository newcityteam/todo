﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.UI.Helpers;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для окна профиля контрагента CounteragentProfileView.
	/// </summary>
	public class CounteragentProfileVM : PropertyChangedBase
	{
		/// <summary>
		/// Признак создания нового контрагента.
		/// Если в конструктор передается null - будет создан новый контрагент.
		/// </summary>
		private readonly bool _newCounteragentRequested;

		/// <summary>
		/// Перменная, хранящая сообщение об ошибке.
		/// </summary>
		private string _errorMessage;
		/// <summary>
		/// Сообщение об ошибке.
		/// </summary>
		public string ErrorMessage
		{
			get { return _errorMessage; }
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(null, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		//
		// Свойства для отображения модели CounteragentDTO
		//

		/// <summary>
		/// Идентификатор задачи
		/// </summary>
		private int _id;
		public int ID
		{
			get { return _id; }
			set { _id = value; NotifyPropertyChanged("ID"); }
		}

		/// <summary>
		/// Название контрагента
		/// </summary>
		private string _name;
		public string Name
		{
			get { return _name; }
			set { _name = value; NotifyPropertyChanged("Name"); }
		}

		/// <summary>
		/// Тип контрагента
		/// </summary>
		private CounteragentType _type;
		public CounteragentType Type
		{
			get { return _type; }
			set { _type = value; NotifyPropertyChanged("Type"); }
		}

		/// <summary>
		/// Описание контрагента
		/// </summary>
		private string _description;
		public string Description
		{
			get { return _description; }
			set { _description = value; NotifyPropertyChanged("Description"); }
		}

		/// <summary>
		/// Телефон контрагента
		/// </summary>
		private string _phoneNumber;
		public string PhoneNumber
		{
			get { return _phoneNumber; }
			set { _phoneNumber = value; NotifyPropertyChanged("PhoneNumber"); }
		}

		/// <summary>
		/// Адрес контрагента
		/// </summary>
		private string _address;
		public string Address
		{
			get { return _address; }
			set { _address = value; NotifyPropertyChanged("Address"); }
		}

		/// <summary>
		/// ИНН контрагента
		/// </summary>
		private string _inn;
		public string INN
		{
			get { return _inn; }
			set { _inn = value; NotifyPropertyChanged("INN"); }
		}

		/// <summary>
		/// КПП контрагента
		/// </summary>
		private string _kpp;
		public string KPP
		{
			get { return _kpp; }
			set { _kpp = value; NotifyPropertyChanged("KPP"); }
		}

		/// <summary>
		/// Расчётный счёт контрагента
		/// </summary>
		private string _paymentAccount;
		public string PaymentAccount
		{
			get { return _paymentAccount; }
			set { _paymentAccount = value; NotifyPropertyChanged("PaymentAccount"); }
		}

		//
		// События управления состоянием View
		//

		/// <summary>
		/// Событие подтверждения изменений, для команды OKCommand
		/// </summary>
		public event EventHandler OKEvent;

		/// <summary>
		/// Событие отмены изменений, для команды CancelCommand
		/// </summary>
		public event EventHandler CancelEvent;

		/// <summary>
		/// Событие возникновения ошибки
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		//
		// Команды
		//

		/// <summary>
		/// Команда подтверждения и внесения изменений в БД
		/// </summary>
		public RelayCommand OKCommand { get; private set; }

		/// <summary>
		/// Команда отмены изменений
		/// </summary>
		public RelayCommand CancelCommand { get; private set; }

		/// <summary>
		/// Команда установки типа контрагента физ./юр. лицо
		/// </summary>
		public RelayCommand SetCounteragentTypeCommand { get; private set; }


		/// <summary>
		/// Свойство установки типа контрагента. Для RadioButton и полей ввода
		/// </summary>
		private bool _isIndividualPerson;
		public bool IsIndividualPerson
		{
			get { return _isIndividualPerson; }
			set
			{
				_isIndividualPerson = value;
				NotifyPropertyChanged("IsIndividualPerson");
			}
		}

		/// <summary>
		/// Свойство установки типа контрагента. Для RadioButton и полей ввода
		/// </summary>
		private bool _isLegal;
		public bool IsLegal
		{
			get { return _isLegal; }
			set
			{
				_isLegal = value;
				NotifyPropertyChanged("IsLegal");
			}
		}

		/// <summary>
		/// Поле доступа к CounteragentsService
		/// </summary>
		private ICounteragentService _counteragentService;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="task">Контрагент для отображения. В случае null - будет создан новый контрагент</param>
		public CounteragentProfileVM(CounteragentDTO counteragent, ICounteragentService counteragents)
		{
			_counteragentService = counteragents;

			// Инициализация команд

			OKCommand = new RelayCommand(OK);
			CancelCommand = new RelayCommand(Cancel);
			SetCounteragentTypeCommand = new RelayCommand(SetCounteragentType);

			// Установка признака создания новой задачи

			if (counteragent == null)
			{
				IsIndividualPerson = true;
				_newCounteragentRequested = true;
				return;
			}

			// Заполнение окна данными задачи

			ID = counteragent.ID;
			Name = counteragent.Name;
			Description = counteragent.Description;
			PhoneNumber = counteragent.PhoneNumber;
			Address = counteragent.Address;
			INN = counteragent.INN;
			KPP = counteragent.KPP;
			PaymentAccount = counteragent.PaymentAccount;
			Type = counteragent.Type;

			// Установка типа контрагента в окне для RadioButton. Возможно, есть решение лучше

			if (Type == CounteragentType.IndividualPerson)
			{
				IsIndividualPerson = true;
				IsLegal = false;
			}
			else if (Type == CounteragentType.Legal)
			{
				IsIndividualPerson = false;
				IsLegal = true;
			}

		}

		/// <summary>
		/// Дополнение к методу - "Метод подтверждения и внесения изменений в БД"
		/// </summary>
		/// <returns></returns>
		private bool ValidateParams()
		{
			var iNN = new Regex(@"^(([0-9]{4}\s[0-9]{4}\s[0-9]{4})|([0-9]{2}\s[0-9]{2}\s[0-9]{3}\s[0-9]{3}))?$");
			var kPP = new Regex(@"^[0-9]{3}\s[0-9]{3}\s[0-9]{3}$");

			//Если Юр. лицо
			if (IsLegal == true)
			{
				//Проверка имени
				if (string.IsNullOrWhiteSpace(Name) == true)
				{
					ErrorMessage = "Заполните наименование контрагента";
					return false;
				}

				//Проверка ИНН
				if (string.IsNullOrWhiteSpace(INN) == true)
				{
				}
				else if (!iNN.IsMatch(INN))
				{
					ErrorMessage = "Введите существующий ИНН контрагента. Пример: 1111 2222 3333 или 11 22 333 444";
					return false;
				}

				//Проверка KPP
				if (string.IsNullOrWhiteSpace(KPP) == true)
				{
				}
				else if (!kPP.IsMatch(KPP))
				{
					ErrorMessage = "Введите существующий КПП контрагента. Пример: 111 222 333";
					return false;
				}

			}

			//Если Физ. лицо
			else if (IsLegal == false)
			{
				//Проверка имени
				if (string.IsNullOrWhiteSpace(Name) == true)
				{
					ErrorMessage = "Заполните наименование контрагента";
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Метод подтверждения и внесения изменений в БД
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void OK(object parameter)
		{
			// Валидация введённых данных

			if (ValidateParams() == false)
			{
				return;
			}

			// установка типа контрагента для записи в бд. Возможно, есть лучшее решение
			if (IsIndividualPerson == true)
			{
				Type = CounteragentType.IndividualPerson;
			}
			else if (IsLegal == true)
			{
				Type = CounteragentType.Legal;
			}

			// Создание объекта DTO для отправки в бизнес-слой BAL

			var сounteragent = new CounteragentDTO();
			сounteragent.ID = ID;
			сounteragent.Name = Name;
			сounteragent.Description = Description;
			сounteragent.PhoneNumber = PhoneNumber;
			сounteragent.Address = Address;
			сounteragent.Type = Type;

			if (сounteragent.Type == CounteragentType.Legal)
			{
				сounteragent.INN = INN;
				сounteragent.KPP = KPP;
				сounteragent.PaymentAccount = PaymentAccount;
			}

			// Создание или обновление объекта в БД

			bool result;

			if (_newCounteragentRequested == true)
			{
				result = _counteragentService.Create(сounteragent);
			}
			else
			{
				result = _counteragentService.Update(сounteragent);
			}

			// Неуспешное выполнение команды

			if (result == false)
			{
				ErrorMessage = _counteragentService.ErrorMessage;
				return;
			}

			// Успешное выполнение команды, вызвать OKEvent

			OKEvent?.Invoke(null, null);
		}

		/// <summary>
		/// Метод отмены изменений
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void Cancel(object parameter)
		{
			CancelEvent?.Invoke(null, null);
		}

		/// <summary>
		/// Метод установки типа контрагента физ./юр. лицо
		/// </summary>
		/// <param name="parameter"></param>
		private void SetCounteragentType(object parameter)
		{
			// TODO Убрать магические строки (брать из enum CounteragentType)
			var type = parameter as string;
			if (type == "IndividualPerson")
			{
				var configrm = MessageBox.Show("Изменить тип контрагента?", "", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (configrm == MessageBoxResult.Cancel)
				{
					IsLegal = true;
					return;
				}
				Type = CounteragentType.IndividualPerson;
			}
			else
			{
				var configrm = MessageBox.Show("Изменить тип контрагента?", "", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (configrm == MessageBoxResult.Cancel)
				{
					IsIndividualPerson = true;
					return;
				}
				Type = CounteragentType.Legal;
			}
		}
	}
}
