﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.Helpers;
using TODO.UI.Views;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для окна управления списком контрагентов CounteragentManageVM
	/// </summary>
	public class CounteragentManageVM : PropertyChangedBase
	{
		/// <summary>
		/// Список контрагентов. Испльзуется для отображения в ListView окна
		/// </summary>
		public ICollection<CounteragentDTO> CounteragentList { get; set; }

		/// <summary>
		/// Текущий выбранный контрагент в ListView
		/// </summary>
		public CounteragentDTO SelectedCounteragent { get; set; }

		/// <summary>
		/// Сообщение об ошибке
		/// </summary>
		private string _errorMessage;
		public string ErrorMessage
		{
			get { return _errorMessage; }
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(this, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		/// <summary>
		/// Событие возникновения ошибки
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		/// <summary>
		/// Команда создания контрагента
		/// </summary>
		public RelayCommand CreateCounteragentCommand { get; private set; }

		/// <summary>
		/// Команда удаления контрагента
		/// </summary>
		public RelayCommand DeleteCounteragentCommand { get; private set; }

		/// <summary>
		/// Команда вывода информации о выбранном контрагенте
		/// </summary>
		public RelayCommand ShowCounteragentDetailsCommand { get; private set; }

		/// <summary>
		/// Поле доступа к CounteragentsService
		/// </summary>
		private ICounteragentService _counteragentService;

		/// <summary>
		/// Конструктор
		/// </summary>
		public CounteragentManageVM(ICounteragentService counteragents)
		{
			_counteragentService = counteragents;

			// Инициализация команд
			CreateCounteragentCommand = new RelayCommand(CreateCounteragent);
			DeleteCounteragentCommand = new RelayCommand(DeleteCounteragent);
			ShowCounteragentDetailsCommand = new RelayCommand(ShowCounteragentDetails);

			ShowCounteragentList(null);
		}

		/// <summary>
		/// Метод создания контрагента
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void CreateCounteragent(object parameter)
		{
			var counteragentProfile = new CounteragentProfileView(_counteragentService);
			if (counteragentProfile.ShowDialog() == false)
			{
				return;
			}

			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowCounteragentList(null);
		}

		/// <summary>
		/// Метод удаления контрагента
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void DeleteCounteragent(object parameter)
		{
			if (SelectedCounteragent == null)
			{
				// TODO Во ViewModel не должно быть MessageBox, для того чтобы она была универсальной
				// TODO и потом её можно было подключить к web view, например. Сделать другой способ уведомления
				MessageBox.Show("Не выбран контрагент");
				return;
			}

			var configrm = MessageBox.Show("Удалить контрагента?", "", MessageBoxButton.OKCancel, MessageBoxImage.Information);
			if (configrm == MessageBoxResult.Cancel)
			{
				return;
			}

			var result = _counteragentService.Delete(SelectedCounteragent.ID);
			if (result == false)
			{
				MessageBox.Show("Удаление невозможно. Контрагент используется.", "", MessageBoxButton.OK, MessageBoxImage.Information);
				return;
			}

			// Удаление контрагента из локального списка, чтобы не перечитывать список из БД
			CounteragentList.Remove(SelectedCounteragent);
			NotifyPropertyChanged("CounteragentList");
		}

		/// <summary>
		/// Метод вывода информации о выбранном контрагенте
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void ShowCounteragentDetails(object parameter)
		{
			if (SelectedCounteragent == null)
			{
				MessageBox.Show("Не выбран контрагент");
				return;
			}

			var counteragentProfile = new CounteragentProfileView(_counteragentService, SelectedCounteragent);
			if (counteragentProfile.ShowDialog() == false)
			{
				return;
			}

			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowCounteragentList(null);
		}

		/// <summary>
		/// Метод вывода списка контрагентов
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void ShowCounteragentList(object parameter)
		{
			var result = _counteragentService.ReadAll();
			if (result == null)
			{
				ErrorMessage = _counteragentService.ErrorMessage;
				return;
			}

			CounteragentList = new ObservableCollection<CounteragentDTO>(result);
			NotifyPropertyChanged("CounteragentList");
		}
	}
}
