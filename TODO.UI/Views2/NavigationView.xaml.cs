﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TODO.UI.Infrastructure;

namespace TODO.UI.Views2
{
	/// <summary>
	/// Interaction logic for NavigationView.xaml
	/// </summary>
	public partial class NavigationView : UserControl
	{
		public NavigationView()
		{
			InitializeComponent();
		}

		private void btnAllTasks_Click(object sender, RoutedEventArgs e)
		{
			SessionManage.SwitchToTasks();
		}

		private void btnProjects_Click(object sender, RoutedEventArgs e)
		{
			SessionManage.SwitchToProjects();
		}

		private void btnCounteragents_Click(object sender, RoutedEventArgs e)
		{
			SessionManage.SwitchToCounteragents();
		}
	}
}
