﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.Infrastructure;
using TODO.UI.ViewModels;

namespace TODO.UI.Views2
{
	/// <summary>
	/// Interaction logic for ProjectTasksView.xaml
	/// </summary>
	public partial class ProjectTasksView : UserControl
	{
		public ProjectTasksView(IProjectService projects, ICounteragentService counteragents, ITaskService tasks, ProjectDTO project)
		{
			InitializeComponent();
			var vm = new ProjectBookVM(project,tasks, counteragents, projects);
			vm.ErrorEvent += OnError;
			DataContext = vm;
		}

		private void OnError(object sender, StringMessageEventArgs e)
		{
			MessageBox.Show(e.Message, "", MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		private void btnBack_Click(object sender, RoutedEventArgs e)
		{
			SessionManage.SwitchToProjects();
		}
	}
}
