﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.ViewModels;

namespace TODO.UI.Views2
{
	/// <summary>
	/// Interaction logic for CounteragentsView.xaml
	/// </summary>
	public partial class CounteragentsView : UserControl
	{
		public CounteragentsView(ICounteragentService counteragents)
		{
			InitializeComponent();

			var vm = new CounteragentManageVM(counteragents);
			vm.ErrorEvent += OnError;
			DataContext = vm;
		}

		/// <summary>
		/// Обработка ошибок во ViewModel
		/// </summary>
		/// <param name="sender">класс ViewModel</param>
		/// <param name="e">Аргументы ErrorViewModelArgs</param>
		private void OnError(object sender, StringMessageEventArgs e)
		{
			MessageBox.Show(e.Message, "", MessageBoxButton.OK, MessageBoxImage.Warning);
		}
	}
}
