﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.UI.Infrastructure;
using TODO.UI.ViewModels;

namespace TODO.UI.Views2
{
	public partial class ProjectsView : UserControl
	{
		ProjectManageVM vm;

		public ProjectsView(ITaskService tasks, ICounteragentService counteragents, IProjectService projects, ILogger logger, IFinanceOperationService financeOperation)
		{
			InitializeComponent();
			vm = new ProjectManageVM(projects, counteragents, tasks, financeOperation);
			vm.ErrorEvent += OnError;
			DataContext = vm;
		}

		private void OnError(object sender, StringMessageEventArgs e)
		{
			MessageBox.Show(e.Message, "", MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		private void btnProjectProfile_Click(object sender, RoutedEventArgs e)
		{
			SessionManage.SwitchToProjectProfile();
		}

		private void btnProjectTasks_Click(object sender, RoutedEventArgs e)
		{
			if(vm.SelectedProject == null)
			{
				MessageBox.Show("Не выбран проект", "");
				return;
			}
			SessionManage.SwitchToProjectTasks(vm.SelectedProject);
		}
	}
}
