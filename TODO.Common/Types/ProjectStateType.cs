﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Types
{
	/// <summary>
	/// Статус проекта
	/// </summary>
	public enum ProjectStateType
	{
		/// <summary>
		/// В процессе выполнения
		/// </summary>
		[Description("В процессе")]
		InProgress,

		/// <summary>
		/// Завершён
		/// </summary>
		[Description("Завершён")]
		Completed,

		/// <summary>
		/// Отменён
		/// </summary>
		[Description("Отменён")]
		Cancelled,

		/// <summary>
		/// Удалён
		/// </summary>
		[Description("Удалён")]
		Deleted,
	}
}
