﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common
{
	public static class INNChecker
	{
		/// <summary>
		/// Метод проверки валидности Индивидуального номера налогоплательщика
		/// </summary>
		/// <param name="INN"></param>
		/// <returns></returns>
		public static bool IsINN(this string INN)
		{
			// null значение не допускается
			if (INN == null)
			{
				return false;
			}
			INN = INN.Trim();
			if (!INN.All(char.IsDigit))
			{
			// символы не-цифры не допускаются
			return false;
			}
			if (INN.Length == 10)
			{
				// Проверка ИНН юридического лица
				int checksum = (int)Char.GetNumericValue(INN[0]) * 2 +
							(int)Char.GetNumericValue(INN[1]) * 4 +
							(int)Char.GetNumericValue(INN[2]) * 10 +
							(int)Char.GetNumericValue(INN[3]) * 3 +
							(int)Char.GetNumericValue(INN[4]) * 5 +
							(int)Char.GetNumericValue(INN[5]) * 9 +
							(int)Char.GetNumericValue(INN[6]) * 4 +
							(int)Char.GetNumericValue(INN[7]) * 6 +
							(int)Char.GetNumericValue(INN[8]) * 8;
				checksum = checksum % 11 % 10;
				//checksum = checksum % 10;
				return (checksum == (int)Char.GetNumericValue(INN[9]));
			}
			if (INN.Length == 12)
			{
				// проверка ИНН физического лица
				int checksum_1 = (int)Char.GetNumericValue(INN[0]) * 7 +
								(int)Char.GetNumericValue(INN[1]) * 2 +
								(int)Char.GetNumericValue(INN[2]) * 4 +
								(int)Char.GetNumericValue(INN[3]) * 10 +
								(int)Char.GetNumericValue(INN[4]) * 3 +
								(int)Char.GetNumericValue(INN[5]) * 5 +
								(int)Char.GetNumericValue(INN[6]) * 9 +
								(int)Char.GetNumericValue(INN[7]) * 4 +
								(int)Char.GetNumericValue(INN[8]) * 6 +
								(int)Char.GetNumericValue(INN[9]) * 8;
				int checksum_2 = (int)Char.GetNumericValue(INN[0]) * 3 +
								(int)Char.GetNumericValue(INN[1]) * 7 +
								(int)Char.GetNumericValue(INN[2]) * 2 +
								(int)Char.GetNumericValue(INN[3]) * 4 +
								(int)Char.GetNumericValue(INN[4]) * 10 +
								(int)Char.GetNumericValue(INN[5]) * 3 +
								(int)Char.GetNumericValue(INN[6]) * 5 +
								(int)Char.GetNumericValue(INN[7]) * 9 +
								(int)Char.GetNumericValue(INN[8]) * 4 +
								(int)Char.GetNumericValue(INN[9]) * 6 +
								(int)Char.GetNumericValue(INN[10]) * 8;
				checksum_1 = checksum_1 % 11 % 10; 
				//checksum_1 = checksum_1	% 10;
				checksum_2 = checksum_2 % 11 % 10;
				//checksum_2 = checksum_2 % 10 ;
				return ((checksum_1 == (int)Char.GetNumericValue(INN[10])) &&
						(checksum_2 == (int)Char.GetNumericValue(INN[11])));
			}
			// Неверное количество символов
			return false;
		}
	}
}

