﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.DTO
{
	/// <summary>
	/// DTO Финансовая операция. 
	/// Используется для сохранения информации о фактически произведенных/полученных платежах
	/// </summary>
	public class FinanceOperationDTO
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Идентификатор проекта, в рамках которого произошел платеж
		/// </summary>
		public int? ProjectID { get; set; }

		/// <summary>
		/// Идентификатор задачи, в рамках которой произошел платеж (только для операций по кредиту)
		/// </summary>
		public int? TaskID { get; set; }

		/// <summary>
		/// Дата поступления платежа
		/// </summary>
		public DateTime Date { get; set; }

		/// <summary>
		/// Сумма операции по дебету (нам заплатили)
		/// </summary>
		public decimal AmountIncome { get; set; }

		/// <summary>
		/// Сумма операции по кредиту (мы заплатили)
		/// </summary>
		public decimal AmountOutcome { get; set; }

		/// <summary>
		/// Описание и примечания
		/// </summary>
		public string Description { get; set; }	

		/// <summary>
		/// Навигационное свойство Задача
		/// </summary>
		public TaskDTO Task { get; set; }

		/// <summary>
		/// Навигационное свойство Проект
		/// </summary>
		public ProjectDTO Project { get; set; }
	}
}
