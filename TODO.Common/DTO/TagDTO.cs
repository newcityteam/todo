﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.DTO
{
	/// <summary>
	/// DTO тег
	/// </summary>
	public class TagDTO
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Название тега
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Задачи с тегом
		/// </summary>
		public ICollection<TaskDTO> Tasks { get; set; }

		/// <summary>
		/// Проекты с тегом
		/// </summary>
		public ICollection<ProjectDTO> Projects { get; set; }
	}
}
