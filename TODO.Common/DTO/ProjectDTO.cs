﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Types;

namespace TODO.Common.DTO
{
	/// <summary>
	/// DTO Проект
	/// </summary>
	public class ProjectDTO
	{
		public ProjectDTO()
		{
			Tags = new List<TagDTO>();
		}
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Название проекта
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Описание проекта
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Дата создания проекта
		/// </summary>
		public DateTime DateCreated { get; set; }

		/// <summary>
		/// Планируемая дата выполнения проекта
		/// </summary>
		public DateTime? DateDuePlanned { get; set; }

		/// <summary>
		/// Фактическая дата выполнения проекта
		/// </summary>
		public DateTime? DateDueFact { get; set; }

		/// <summary>
		/// Планируемый бюджет (объем финансирования)
		/// </summary>
		public decimal PlannedAmount { get; set; }

		/// <summary>
		/// Статус проекта
		/// </summary>
		public ProjectStateType State { get; set; }

		/// <summary>
		/// Финансовые поступления по проекту
		/// </summary>
		public ICollection<FinanceOperationDTO> IncomeFinance { get; set; }

		/// <summary>
		/// Задачи проекта
		/// </summary>
		public ICollection<TaskDTO> Tasks { get; set; }

		/// <summary>
		/// Теги для проекта
		/// </summary>
		public ICollection<TagDTO> Tags { get; set; }

		/// <summary>
		/// Навигационное свойство Контрагент
		/// </summary>
		public CounteragentDTO Counteragent { get; set; }
	}
}
