﻿using System;
using System.ComponentModel;

namespace TODO.Common.Helpers
{
	/// <summary>
	/// Расширение для получения описания у Enum.
	/// </summary>
	public static class EnumDescription
	{
		/// <summary>
		/// Возвращает описание у Enum.
		/// </summary>
		public static string GetDescription(this Enum en)
		{
			var type = en.GetType();
			var memInfo = type.GetMember(en.ToString());
			if (memInfo != null && memInfo.Length > 0)
			{
				var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
				if (attrs != null && attrs.Length > 0)
				{
					return ((DescriptionAttribute)attrs[0]).Description;
				}
			}
			return en.ToString();
		}
	}
}