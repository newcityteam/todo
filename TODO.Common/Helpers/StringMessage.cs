﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Helpers
{
	/// <summary>
	/// Аргумент события для передачи сообщений типа string
	/// </summary>
	public class StringMessageEventArgs : EventArgs
	{
		public string Message { get; set; }
	}

	/// <summary>
	/// Обработчик события StringMessageHandler
	/// </summary>
	/// <param name="sender">Вызывающий объект</param>
	/// <param name="e">аргумент StringMessageEventArgs</param>
	public delegate void StringMessageHandler(object sender, StringMessageEventArgs e);
}
