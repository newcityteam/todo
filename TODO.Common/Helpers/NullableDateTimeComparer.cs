﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Helpers
{
	/// <summary>
	/// Предоставляет альтернативный компаратор для типа Nullable<DateTime> (DateTime?).
	/// Считает, что null больше любой валидной даты.
	/// Можно использовать, например, для сортировки задач или проектов по DateDue.
	/// </summary>
	public class NullableDateTimeComparer : Comparer<DateTime?>
	{
		/// <summary>
		/// Сравнивает Nullable<DateTime>, считая, что null больше любой валидной даты.
		/// </summary>
		/// <param name="x">Левое значение.</param>
		/// <param name="y">Правое значение.</param>
		/// <returns>Результат сравнения.</returns>
		public override int Compare(DateTime? x, DateTime? y)
		{
			if (!x.HasValue && y.HasValue)
			{
				return 1;
			}
			if (x.HasValue && !y.HasValue)
			{
				return -1;
			}
			if (!x.HasValue && !y.HasValue)
			{
				return 0;
			}
			return x.Value.CompareTo(y.Value);
		}
	}
}
