﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Helpers
{
	/// <summary>
	/// Класс для форматирования сообщений исключения
	/// </summary>
	public static class ExceptionHelper
	{
		/// <summary>
		/// Показать сообщение исключения и внутреннего исключения
		/// </summary>
		/// <param name="ex">Исключение Exception</param>
		/// <returns>Строка сообщения исключений</returns>
		public static string AddInner(Exception ex)
		{
			var result = ex.Message;

			if (ex.InnerException != null)
			{
				result += "\n" + ex.InnerException.Message;
			}

			return result;
		}
	}
}
