﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Interfaces
{
	/// <summary>
	/// Интерфейс для работы с объектами, паттерн CRUD
	/// </summary>
	public interface IRepository<T>
	{
		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект типа Т</param>
		void Create(T item);

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект типа Т</returns>
		T Read(int id);

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов типа Т</returns>
		IEnumerable<T> Read(Expression<Func<T, bool>> query);

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов типа Т</returns>
		IEnumerable<T> ReadAll();

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект типа Т</param>
		void Update(T item);

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		void Delete(int id);
	}
}
