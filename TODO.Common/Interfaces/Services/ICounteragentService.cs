﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;

namespace TODO.Common.Interfaces.Services
{
	/// <summary>
	/// Интерфейс сервиса Контрагенты
	/// </summary>
	public interface ICounteragentService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		string ErrorMessage { get; }

		/// <summary>
		/// Создание контрагента
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		bool Create(CounteragentDTO item);

		/// <summary>
		/// Прочтитать список всех контрагентов.
		/// </summary>
		/// <returns>Список контрагентов</returns>
		IEnumerable<CounteragentDTO> ReadAll();

		/// <summary>
		/// Прочтитать список контрагентов по произвольному условию.
		/// Используется для фильтрации данных
		/// </summary>
		/// <param name="query">linq-вырвжение</param>
		/// <returns>Список контрагентов</returns>
		IEnumerable<CounteragentDTO> ReadCustom(Expression<Func<CounteragentDTO, bool>> query);

		/// <summary>
		/// Обновление информации о контрагенте
		/// </summary>
		/// <param name="item">Объект контрагента CounteragentDTO</param>
		/// <returns>Обновление прошло успешно - true, иначе false</returns>
		bool Update(CounteragentDTO item);

		/// <summary>
		/// Полное удаление контрагента из базы данных
		/// </summary>
		/// <param name="id">ID контрагента</param>
		/// <returns>Удаление прошло успешно - true, иначе false</returns>
		bool Delete(int id);
	}
}
