﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;

namespace TODO.Common.Interfaces.Services
{
	/// <summary>
	/// Интерфейс сервиса Задачи
	/// </summary>
	public interface ITaskService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		string ErrorMessage { get; }

		/// <summary>
		/// Создание задачи
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		bool Create(TaskDTO item);

		/// <summary>
		/// Прочитать все активные задачи.
		/// Активной считается задача со статусом "В процессе" или "Просрочена"
		/// </summary>
		/// <returns>Список активных задач</returns>
		IEnumerable<TaskDTO> ReadActive();

		/// <summary>
		/// Прочитать все архивные задачи.
		/// Архивной считается задача со статусом "Завершена" или "Отменена"
		/// </summary>
		/// <returns>Список архивных задач</returns>
		IEnumerable<TaskDTO> ReadArchive();

		/// <summary>
		/// Прочтитать список задач по произвольному условию.
		/// Используется для фильтрации данных
		/// </summary>
		/// <param name="query">linq-вырвжение</param>
		/// <returns>Список задач</returns>
		IEnumerable<TaskDTO> ReadCustom(Expression<Func<TaskDTO, bool>> query);

		/// <summary>
		/// Обновление информации о задаче.
		/// Обновляемые поля:
		/// Title (название),
		/// Description (описание),
		/// DateDue (дата завершения),
		/// Direction (направление).
		///
		/// Игнорируемые поля:
		/// DateCreated (дата создания),
		/// State (состояние)
		/// </summary>
		/// <param name="item">Объект задачи TaskDTO</param>
		/// <returns>Обновление прошло успешно - true, иначе false</returns>
		bool Update(TaskDTO item);

		/// <summary>
		/// Установить состояние задачи "Завершена"
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		bool SetComplete(int id);

		/// <summary>
		/// Установить состояние задачи "Отменена"
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		bool SetCancel(int id);

		/// <summary>
		/// Повторение ранее завершенной или отменённой задачи.
		/// После выполнения создаётся новая задача, старая остаётся неизменной.
		/// </summary>
		/// <param name="item">Объект задачи TaskDTO</param>
		/// <returns></returns>
		bool Repeat(TaskDTO item);

		/// <summary>
		/// Пересоздание задачи
		/// После выполения создаётся новая задача, но ещё не записывается в базу
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		TaskDTO Recreate(TaskDTO item);

		/// <summary>
		/// Полное удаление задачи из базы данных
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Удаление прошло успешно - true, иначе false</returns>
		bool Delete(int id);

		/// <summary>
		/// Добавление тега к задаче
		/// </summary>
		/// <param name="item"></param>
		/// <param name="tagString"></param>
		/// <returns></returns>
		bool ChangeTags(ref TaskDTO item, string tagString);
	}
}
