﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Interfaces.Services
{
	public interface ILogger
	{
		void Info(string message);
		void Info(Exception exception, string message);
		void Debug(string message);
		void Debug(Exception exception, string message);
		void Error(string message);
		void Error(Exception exception, string message);
	}
}
