﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;

namespace TODO.Common.Interfaces.Services
{
	public interface IUnitOfWork
	{
		/// <summary>
		/// Сохранение изменений в контексте
		/// </summary>
		void Complete();
	}
}
