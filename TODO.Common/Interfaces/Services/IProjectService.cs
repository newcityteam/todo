﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;

namespace TODO.Common.Interfaces.Services
{
	public interface IProjectService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		string ErrorMessage { get; }

		/// <summary>
		/// Создание проекта
		/// </summary>
		/// <param name="project"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		bool Create(ProjectDTO project);

		/// <summary>
		/// Прочитать задачу по номеру id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		ProjectDTO Read(int id);

		/// <summary>
		/// Получить все активные проекты
		/// </summary>
		/// <remarks>Активным считается проект со статусом В процессе</remarks>
		/// <returns></returns>
		IEnumerable<ProjectDTO> ReadActive();

		/// <summary>
		/// Получить все архивные проекты
		/// </summary>
		/// <remarks>Активным считается проект со статусами Завершён или Отменён</remarks>
		/// <returns></returns>
		IEnumerable<ProjectDTO> ReadArchive();

		/// <summary>
		/// Получить проект по ID
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		ProjectDTO ReadProject(int id);

		/// <summary>
		/// Обновление информации о задаче
		/// </summary>
		/// <param name="project"></param>
		/// <returns></returns>
		bool Update(ProjectDTO project);

		/// <summary>
		/// Установить статус проекта Завершён
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		bool SetCompleted(int id);

		/// <summary>
		/// Установить статус проекта Отменён
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		bool SetCancelled(int id);

		/// <summary>
		/// Установить статус проекта Удалён
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		bool SetDeleted(int id);

		/// <summary>
		/// Добавляет в проект финансовое поступление
		/// </summary>
		/// <param name="project"></param>
		/// <param name="financeOperation"></param>
		void AddIncomeFinance(ProjectDTO project, FinanceOperationDTO financeOperation);

		/// <summary>
		/// Добавляет в проект задачу
		/// </summary>
		/// <param name="task"></param>
		void AddTask(ProjectDTO project, TaskDTO task);

		/// <summary>
		/// Изменение тегов проекта
		/// </summary>
		/// <param name="item"></param>
		/// <param name="tagString"></param>
		/// <returns></returns>
		bool ChangeTags(ref ProjectDTO item, string tagString);
	}
}
