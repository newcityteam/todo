﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;

namespace TODO.Common.Interfaces.Services
{
	public interface IFinanceOperationService
	{
		string ErrorMessage { get; }

		bool Create(FinanceOperationDTO item);

		bool IsFinanceOperationCorrect(FinanceOperationDTO item);

		decimal GetCurrentIncomeforProject(int ProjectID);

		decimal GetCurrentOutcomeforProject(int ProjectID);

		decimal GetAvailableMoneyforProject(int ProjectID);

		decimal GetCurrentCostforTask(int TaskID);

		decimal GetCurrentPlannedAmountforTask(int TaskID);

		decimal GetCurrentPlannedOutcomeAmountforProject(int ProjectID);

		decimal GetCurrentFundingGap(int ProjectID);

		IEnumerable<FinanceOperationDTO> GetEnumerableIncomeFinanceOperationforProject(int ProjectID);
	}
}
