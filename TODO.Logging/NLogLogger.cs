﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Logging
{
	public class NLogLogger : TODO.Common.Interfaces.Services.ILogger
	{
		private readonly Logger _logger = LogManager.GetLogger("NLog");

		public void Debug(string message)
		{
			_logger.Debug(message);
		}

		public void Debug(Exception exception, string message)
		{
			_logger.Debug(exception, message);
		}

		public void Error(string message)
		{
			_logger.Error(message);
		}

		public void Error(Exception exception, string message)
		{
			_logger.Error(exception, message);
		}

		public void Info(string message)
		{
			_logger.Info(message);
		}

		public void Info(Exception exception, string message)
		{
			_logger.Info(exception, message);
		}
	}
}
