﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL;
using TODO.DAL.Context;

namespace TODO.BAL.Services
{
	/// <summary>
	/// Класс сервиса для Контрагентов.
	/// Обработка исключений из слоя DAL происходит здесь
	/// </summary>
	public class CounteragentService : ICounteragentService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		public string ErrorMessage { get { return _errorMessage; } }
		string _errorMessage;

		/// <summary>
		/// Поле доступа к репозиторию
		/// </summary>
		IRepository<CounteragentDTO> _counteragentRepository;

		/// <summary>
		/// Пле доступа к UnitOfWork
		/// </summary>
		private IUnitOfWork _unitOfWork;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="counteragentRepository">Объект доступа к слою DAL</param>
		public CounteragentService(IRepository<CounteragentDTO> counteragentRepository, IUnitOfWork unit)
		{
			_counteragentRepository = counteragentRepository;
			_unitOfWork = unit;
		}

		/// <summary>
		/// Создание контрагента
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		public bool Create(CounteragentDTO item)
		{
			try
			{
				_counteragentRepository.Create(item);
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}

		/// <summary>
		/// Прочтитать список всех контрагентов.
		/// </summary>
		/// <returns>Список контрагентов</returns>
		public IEnumerable<CounteragentDTO> ReadAll()
		{
			try
			{
				return _counteragentRepository.ReadAll();
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Прочтитать список контрагентов по произвольному условию.
		/// Используется для фильтрации данных
		/// </summary>
		/// <param name="query">linq-вырвжение</param>
		/// <returns>Список контрагентов</returns>
		public IEnumerable<CounteragentDTO> ReadCustom(Expression<Func<CounteragentDTO, bool>> query)
		{
			try
			{
				return _counteragentRepository.Read(query);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Обновление информации о контрагенте
		/// </summary>
		/// <param name="item">Объект контрагента CounteragentDTO</param>
		/// <returns>Обновление прошло успешно - true, иначе false</returns>
		public bool Update(CounteragentDTO item)
		{
			try
			{
				_counteragentRepository.Update(item);
				_unitOfWork.Complete();
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}

		/// <summary>
		/// Полное удаление контрагента из базы данных
		/// </summary>
		/// <param name="id">ID контрагента</param>
		/// <returns>Удаление прошло успешно - true, иначе false</returns>
		public bool Delete(int id)
		{
			try
			{
				_counteragentRepository.Delete(id);
				_unitOfWork.Complete();
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
	}
}
