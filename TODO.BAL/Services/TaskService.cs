﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.DAL;

namespace TODO.BAL.Services
{
	/// <summary>
	/// Класс сервиса для Задач.
	/// Обработка исключений из слоя DAL происходит здесь
	/// </summary>
	public class TaskService : ITaskService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		public string ErrorMessage { get { return _errorMessage; } }
		string _errorMessage;

		/// <summary>
		/// Объект доступа к репозиторию
		/// </summary>
		IRepository<TaskDTO> _taskRepository;

		/// <summary>
		/// Объект доступа к репозиторию тегов
		/// </summary>
		IRepository<TagDTO> _tagRepository;

		/// <summary>
		/// Поле для доступа к UnitOfWork
		/// </summary>
		private IUnitOfWork _unitOfWork;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="taskRepository">Объект доступа к слою DAL</param>
		public TaskService(IRepository<TagDTO> tags, IRepository<TaskDTO> taskRepository, IUnitOfWork unit)
		{
			_taskRepository = taskRepository;
			_tagRepository = tags;
			_unitOfWork = unit;
		}

		/// <summary>
		/// Создание задачи
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		public bool Create(TaskDTO item)
		{
			try
			{
				item.DateCreated = DateTime.Now;
				_taskRepository.Create(item);
				_unitOfWork.Complete();
				return true;
		}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
}

		/// <summary>
		/// Прочитить все активные задачи.
		/// Активной считается задача со статусом "В процессе" или "Просрочена"
		/// </summary>
		/// <returns>Список активных задач</returns>
		public IEnumerable<TaskDTO> ReadActive()
		{
			try
			{
				return _taskRepository.Read(x => x.State == TaskStateType.InProgress && x.Project==null)
					.OrderBy(x => x.DateDue, new NullableDateTimeComparer());
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Прочитать все архивные задачи.
		/// Архивной считается задача со статусом "Завершена" или "Отменена"
		/// </summary>
		/// <returns>Список архивных задач</returns>
		public IEnumerable<TaskDTO> ReadArchive()
		{
			try
			{
				return _taskRepository.Read(x => (x.State == TaskStateType.Completed || x.State == TaskStateType.Cancelled) && x.Project == null);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Прочтитать список задач по произвольному условию.
		/// Используется для фильтрации данных
		/// </summary>
		/// <param name="query">linq-вырвжение</param>
		/// <returns>Список задач</returns>
		public IEnumerable<TaskDTO> ReadCustom(Expression<Func<TaskDTO, bool>> query)
		{
			try
			{
				return _taskRepository.Read(query);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Обновление информации о задаче.
		/// Обновляемые поля:
		/// Title (название),
		/// Description (описание),
		/// DateDue (дата завершения),
		/// Direction (направление).
		///
		/// Игнорируемые поля:
		/// DateCreated (дата создания),
		/// State (состояние)
		/// </summary>
		/// <param name="item">Объект задачи TaskDTO</param>
		/// <returns>Обновление прошло успешно - true, иначе false</returns>
		public bool Update(TaskDTO item)
		{
			try
			{
				var oldTask = _taskRepository.Read(item.ID);
				if (oldTask.State == TaskStateType.Cancelled ||
					oldTask.State == TaskStateType.Deleted ||
					oldTask.State == TaskStateType.Completed)
				{
					_errorMessage = "Невозможно обновить архивную задачу";
					return false;
				}
				else
				{
					_taskRepository.Update(item);
					_unitOfWork.Complete();
					return true;
				}
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}

		/// <summary>
		/// Установка состояния. Используется в методах реализации
		/// этого интерфейса (ITaskService)
		/// </summary>
		/// <param name="taskID">ID Задачи</param>
		/// <param name="newState">Новое сосотояние</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		protected bool SetStateHelper(int taskID, TaskStateType newState)
		{
			try
			{
				var item = _taskRepository.Read(taskID);
				item.State = newState;
				_taskRepository.Update(item);
				_unitOfWork.Complete();
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Установить состояние задачи "Завершена"
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		public bool SetComplete(int id)
		{
			return SetStateHelper(id, TaskStateType.Completed);
		}

		/// <summary>
		/// Установить состояние задачи "Отменена"
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		public bool SetCancel(int id)
		{
			return SetStateHelper(id, TaskStateType.Cancelled);
		}

		/// <summary>
		/// Повторение ранее завершенной или отменённой задачи.
		/// После выполнения создаётся новая задача, старая остаётся неизменной.
		/// </summary>
		/// <param name="item">Объект задачи TaskDTO</param>
		/// <returns></returns>
		public bool Repeat(TaskDTO item)
		{
			return Create(Copy(item));
		}

		private TaskDTO Copy(TaskDTO item)
		{
			var copy = new TaskDTO();
			{
				copy.DateCreated = DateTime.Now;
				copy.Title = item.Title;
				copy.Description = item.Description;
				copy.DateDue = item.DateDue;
				copy.Counteragent = item.Counteragent;
				copy.Tags = item.Tags;
			}

			return copy;
		}

		public TaskDTO Recreate(TaskDTO item)
		{
			var copy = Copy(item);
			copy.DateDue = copy.DateDue >= DateTime.Today ? copy.DateDue : DateTime.Today;
			return copy;
		}

		/// <summary>
		/// Полное удаление задачи из базы данных
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Удаление прошло успешно - true, иначе false</returns>
		public bool Delete(int id)
		{
			return SetStateHelper(id, TaskStateType.Deleted);
			//try
			//{
			//	_taskRepository.Tasks.Delete(id);
			//	_taskRepository.SaveChanges();
			//	return true;
			//}
			//catch (Exception ex)
			//{
			//	_errorMessage = ExceptionHelper.AddInner(ex);
			//	return false;
			//}
		}

		/// <summary>
		/// Метод изменения тегов у задачи
		/// </summary>
		/// <param name="item"></param>
		/// <param name="tagString"></param>
		/// <returns></returns>
		public bool ChangeTags(ref TaskDTO item, string tagString)
		{
			try
			{
				//получаем список существующих тегов
				var allTags = _tagRepository.ReadAll();
				//обозначаем разделители
				char[] separators = new char[] { ',', ';' };
				//парсим строку. Получаем массив тегов
				var taskTags = tagString.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				foreach (var tag in taskTags)
				{
					var trimTag = tag.Trim();
					//если тега с таким именем в базе нет, добавляем
					if (!allTags.Any(x => x.Name == trimTag))
						_tagRepository.Create(new TagDTO {Name = trimTag});
				}
				_unitOfWork.Complete();
				//получаем обновленный список тегов с их id
				allTags = _tagRepository.ReadAll();
				foreach (var tag in taskTags)
				{
					var trimTag = tag.Trim();
					//добавляем тег из списка к выбранной задаче
					item.Tags.Add(allTags.Where(x=>x.Name==trimTag).First());
				}
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
	}
}
