﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;

namespace TODO.BAL.Services
{
	public class FinanceOperationService : IFinanceOperationService
	{
		public string ErrorMessage { get { return _errorMessage; } }
		string _errorMessage;

		/// <summary>
		/// Поле доступа к репозиторию
		/// </summary>
		IRepository<FinanceOperationDTO> _financeOperationRepository;
		IRepository<TaskDTO> _taskRepository;
		IRepository<ProjectDTO> _projectRepository;

		/// <summary>
		/// Поле доступа к UnitOfWork
		/// </summary>
		private IUnitOfWork _unitOfWork;

		/// <summary>
		/// Конструктор
		/// </summary>
		public FinanceOperationService(IRepository<FinanceOperationDTO> financeOperationRepository, IUnitOfWork unit, 
										IRepository<TaskDTO> taskRepository, IRepository<ProjectDTO> projectRepository)
		{
			_financeOperationRepository = financeOperationRepository;
			_taskRepository = taskRepository;
			_projectRepository = projectRepository;
			_unitOfWork = unit;
		}

		/// <summary>
		/// Проверка на корректность финансовой операции
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool IsFinanceOperationCorrect(FinanceOperationDTO item)
		{
			// Запрещены операции с датой в будущем
			if (item.Date > DateTime.Now) return false;
			// Запрещены отрицательные суммы
			if (item.AmountIncome < 0 || item.AmountOutcome < 0) return false;
			// Запрещены "пустые" операции с суммой 0 по дебету и кредиту
			if (item.AmountOutcome == 0 && item.AmountIncome == 0) return false;
			// Запрещены двойные операции, с заполненным и дебетом, и кредитом
			if (item.AmountOutcome != 0 && item.AmountIncome != 0) return false;
			// Запрещены входящие операции без указания проекта
			if (item.AmountIncome != 0 && item.ProjectID == null) return false;
			// Запрещены исходящие операции без указания задачи
			if (item.AmountOutcome != 0 && item.TaskID == null) return false;
			return true;
		}

		/// <summary>
		/// Создание финансовой операции
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Create(FinanceOperationDTO item)
		{
			try
			{
				if (IsFinanceOperationCorrect(item))
				{
					_financeOperationRepository.Create(item);
					return true;
				}
				else
				{
					throw new Exception("Incorrect financial operation");
				}
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}

		/// <summary>
		/// Рассчитать фактически полученные деньги на проект
		/// </summary>
		/// <param name="ProjectID"></param>
		/// <returns></returns>
		public decimal GetCurrentIncomeforProject(int ProjectID)
		{
			try
			{
				return _financeOperationRepository.Read(x => x.ProjectID == ProjectID).Select(x => x.AmountIncome).Sum();
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return 0;
			}
		}

		/// <summary>
		/// Рассчитать фактически потраченные деньги на проект
		/// </summary>
		/// <param name="ProjectID"></param>
		/// <returns></returns>
		public decimal GetCurrentOutcomeforProject(int ProjectID)
		{
			try
			{
				return _financeOperationRepository.Read(x => x.ProjectID == ProjectID).Select(x => x.AmountOutcome).Sum();
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return 0;
			}
		}

		/// <summary>
		/// Рассчет остатка денег по проекту
		/// Фактически полученные минус фактически потраченные
		/// </summary>
		/// <param name="Project"></param>
		/// <returns></returns>
		public decimal GetAvailableMoneyforProject(int ProjectID)
		{
			return GetCurrentIncomeforProject(ProjectID) - GetCurrentOutcomeforProject(ProjectID);
		}

		/// <summary>
		/// Рассчет реальных затрат на задачу
		/// </summary>
		/// <param name="TaskID"></param>
		/// <returns></returns>
		public decimal GetCurrentCostforTask(int TaskID)
		{
			return _financeOperationRepository.Read(x => x.TaskID == TaskID).Select(x => x.AmountOutcome).Sum();
			
		}

		/// <summary>
		/// Рассчитать планируемые затраты на задачу с учетом фактических затрат
		/// </summary>
		/// <param name="TaskID"></param>
		/// <returns></returns>
		public decimal GetCurrentPlannedAmountforTask(int TaskID)
		{
			var TaskItem = _taskRepository.Read(TaskID);
			// Если операция неактивная, то планируемая затраты равны нулю
			if (TaskItem.State == Common.Types.TaskStateType.Cancelled ||
				TaskItem.State == Common.Types.TaskStateType.Deleted ||
				TaskItem.State == Common.Types.TaskStateType.Completed)
			{
				return 0;
			}
			else
			{
				// Если операция активная, то из предварительного плана задачи надо вычесть фактические расходы
				// Если результат отрицательный, вернуль ноль.
				var _taskCurrentCost = GetCurrentCostforTask(TaskID);
				if (TaskItem.PlannedAmount - _taskCurrentCost < 0)
				{
					return 0;
				}
				else
				{
					return (TaskItem.PlannedAmount - _taskCurrentCost);
				}
			}
		}

		/// <summary>
		/// Рассчитать планируемые затраты по проекту
		/// </summary>
		/// <param name="ProjectID"></param>
		/// <returns></returns>
		public decimal GetCurrentPlannedOutcomeAmountforProject(int ProjectID)
		{
			// Считаем планируемые затраты по каждой задаче, входящей в проект
			var _project = _projectRepository.Read(ProjectID);
			decimal _outcomeAmount = 0;
			if (_project != null)
			{
				foreach (var _task in _project.Tasks)
				{
					_outcomeAmount += GetCurrentCostforTask(_task.ID);
				}
			}
			return _outcomeAmount;
		}

		/// <summary>
		/// Рассчитать объем финансирования проекта по сравнению с планом
		/// </summary>
		/// <param name="ProjectID"></param>
		/// <returns></returns>
		public decimal GetCurrentFundingGap(int ProjectID)
		{
			// Разница между запланированным финансированием и фактически полученным
			var _project = _projectRepository.Read(ProjectID);
			if (_project != null)
			{
				return _project.PlannedAmount - GetCurrentIncomeforProject(ProjectID);
			}
			return 0;
		}

		///// <summary>
		///// Рассчитать финансовый результат по проекту
		///// </summary>
		///// <param name="ProjectID"></param>
		///// <returns></returns>
		//public decimal GetCurrentFinanceResultforProject(int ProjectID)
		//{
		//	// Плановый изначальный расход - Фактические расходы - Текущие плановые расходы
		//	return 0;
		//}
		///// <summary>
		///// Рассчитать финансовый результат по задаче
		///// </summary>
		///// <param name="TaskID"></param>
		///// <returns></returns>
		//public decimal GetCurrentFinanceResultforTask(int TaskID)
		//{
		//	// Бюджет (или объем финансирования?) - Фактические расходы - Плановые расходы
		//	return 0;
		//}

		/// <summary>
		/// Получить список финансовых операций для журнала приходов
		/// </summary>
		/// <param name="ProjectID"></param>
		/// <returns></returns>
		public IEnumerable<FinanceOperationDTO> GetEnumerableIncomeFinanceOperationforProject(int ProjectID)
		{
			return _financeOperationRepository.Read(x => x.ProjectID == ProjectID).Where(x => x.AmountIncome > 0);
		}


	}
}
