﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.DAL;

namespace TODO.BAL.Services
{
	public class ProjectService : IProjectService
	{
		public string ErrorMessage { get; private set; }

		readonly private IUnitOfWork _unit;
		readonly private IRepository<ProjectDTO> _projectRepository;
		readonly private IRepository<TagDTO> _tagRepository;
		readonly private ILogger _logger;

		public ProjectService(IRepository<ProjectDTO> projects, IRepository<TagDTO> tags, IUnitOfWork unit, ILogger logger)
		{
			_unit = unit;
			_projectRepository = projects;
			_logger = logger;
			_tagRepository = tags;
		}

		public void AddIncomeFinance(ProjectDTO project, FinanceOperationDTO financeOperation)
		{
			project.IncomeFinance.Add(financeOperation);
		}

		public void AddTag(ProjectDTO project, TagDTO tag)
		{
			project.Tags.Add(tag);
		}

		public void AddTask(ProjectDTO project, TaskDTO task)
		{
			project.Tasks.Add(task);
		}

		public bool Create(ProjectDTO project)
		{
			try
			{
				project.DateCreated = DateTime.Now;
				_projectRepository.Create(project);
				_logger.Info($"Создан проект {project.ID}");
				return true;
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, "Ошибка создания проекта");
				return false;
			}
		}

		public ProjectDTO Read(int projectID)
		{
			try
			{
				return _projectRepository.Read(projectID);
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, $"Ошибка чтения проекта Id={projectID}");
				return null;
			}
		}

		/// <summary>
		/// Возвращает все записи со статусом В процессе выполнения
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ProjectDTO> ReadActive()
		{
			try
			{
				return _projectRepository.Read(x => x.State == ProjectStateType.InProgress)
					.OrderByDescending(x => x.State)
					.ThenByDescending(x => x.DateCreated);
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, "Ошибка чтения списка активных задач");
				return null;
			}
		}

		/// <summary>
		/// Возвращает все записи со статусами Отменён или Завершён
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ProjectDTO> ReadArchive()
		{
			try
			{
				return _projectRepository.Read(x => x.State == ProjectStateType.Cancelled || x.State == ProjectStateType.Completed)
					.OrderByDescending(x => x.State)
					.ThenByDescending(x => x.DateCreated);
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, "Ошибка чтения списка архивных задач");
				return null;
			}
		}

		public ProjectDTO ReadProject(int id)
		{
			try
			{
				return _projectRepository.Read(id);
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, "Ошибка чтения проекта");
				return null;
			}
		}

		protected bool SetState(int projectID, ProjectStateType state)
		{
			try
			{
				var item = _projectRepository.Read(projectID);
				item.State = state;
				_projectRepository.Update(item);
				_unit.Complete();
				_logger.Info($"Изменение состояния проекта ID={projectID} на {state.GetDescription()} прошло успешно");
				return true;
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, $"Ошибка изменения состояния проекта ID={projectID} на {state.GetDescription()}");
				return false;
			}
		}

		/// <summary>
		/// Установка статуса Отменён
		/// </summary>
		/// <param name="projectID">Id проекта</param>
		/// <returns></returns>
		public bool SetCancelled(int projectID)
		{
			return SetState(projectID, ProjectStateType.Cancelled);
		}

		/// <summary>
		/// Установка статуса Завершён
		/// </summary>
		/// <param name="projectID">Id проекта</param>
		/// <returns></returns>
		public bool SetCompleted(int projectID)
		{
			return SetState(projectID, ProjectStateType.Completed);
		}

		/// <summary>
		/// Установка статуса Удалён
		/// </summary>
		/// <param name="projectID">Id проекта</param>
		/// <returns></returns>
		public bool SetDeleted(int projectID)
		{
			return SetState(projectID, ProjectStateType.Deleted);
		}

		public bool Update(ProjectDTO project)
		{
			try
			{
				_projectRepository.Update(project);
				_unit.Complete();
				_logger.Info($"Обновление проекта ID={project.ID} прошло успешно");
				return true;
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				_logger.Error(ex, $"Ошибка при обновлении проекта ID={project.ID}");
				return false;
			}
		}

		/// <summary>
		/// Метод изменения тегов у проекта
		/// </summary>
		/// <param name="item"></param>
		/// <param name="tagString"></param>
		/// <returns></returns>
		public bool ChangeTags(ref ProjectDTO item, string tagString)
		{
			try
			{
				//получаем список существующих тегов
				var allTags = _tagRepository.ReadAll();
				//обозначаем разделители
				char[] separators = new char[] { ',', ';' };
				//парсим строку. Получаем массив тегов
				var projectTags = tagString.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				foreach (var tag in projectTags)
				{
					var trimTag = tag.Trim();
					//если тега с таким именем в базе нет, добавляем
					if (!allTags.Any(x => x.Name == trimTag))
						_tagRepository.Create(new TagDTO { Name = trimTag });
				}
				_unit.Complete();
				//получаем обновленный список тегов с их id
				allTags = _tagRepository.ReadAll();
				foreach (var tag in projectTags)
				{
					var trimTag = tag.Trim();
					//добавляем тег из списка к выбранному проекту
					item.Tags.Add(allTags.Where(x => x.Name == trimTag).First());
				}
				return true;
			}
			catch (Exception ex)
			{
				ErrorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
	}
}
