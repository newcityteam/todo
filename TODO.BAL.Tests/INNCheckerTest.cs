﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TODO.Common;

namespace TODO.BAL.Tests
{
	/// <summary>
	/// Сводное описание для INNCheckerTest
	/// </summary>
	[TestClass]
	public class INNCheckerTest
	{
		[TestMethod]
		public void TestNull()
		{
			string tstring = null;
			Assert.IsFalse(tstring.IsINN());
		}

		[TestMethod]
		public void TestCorrect10()
		{
			string tstring = "6382050690";
			Assert.IsTrue(tstring.IsINN());
		}

		[TestMethod]
		public void TestIncorrectFormatNotNumeric()
		{
			string tstring = "vasya pupkin";
			Assert.IsFalse(tstring.IsINN());
		}

		[TestMethod]
		public void TestIncorrectFormatToManySymbols()
		{
			string tstring = "123";
			Assert.IsFalse(tstring.IsINN());
		}

		[TestMethod]
		public void TestIncorrect10()
		{
			string tstring = "6382050691";
			Assert.IsFalse(tstring.IsINN());
		}

		[TestMethod]
		public void TestCorrect12()
		{
			string tstring = "638204037489";
			Assert.IsTrue(tstring.IsINN());
		}

		[TestMethod]
		public void TestIncorrect12()
		{
			string tstring = "638204037488";
			Assert.IsFalse(tstring.IsINN());
		}
	}
}
