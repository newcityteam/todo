﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.BAL.Services;
using TODO.DAL;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TODO.BAL.Tests
{
	[TestClass]
	public class CounteragentServiceTests
	{
		static Mock<IRepository<CounteragentDTO>> _caRepo;
		static Mock<ILogger> _logger;
		static Mock<IUnitOfWork> _unit;
		static CounteragentService _service;

		public CounteragentServiceTests()
		{
			_caRepo = _caRepo ?? new Mock<IRepository<CounteragentDTO>>(MockBehavior.Strict);
			_unit = _unit ?? new Mock<IUnitOfWork>(MockBehavior.Strict);
			_unit.Setup(a => a.Complete());
			if (_logger == null)
			{
				_logger = new Mock<ILogger>(MockBehavior.Strict);
				_logger.Setup(logger => logger.Info(It.IsAny<string>()));
			}
			_service = _service ?? new CounteragentService(_caRepo.Object,_unit.Object);
		}

		[TestMethod]
		public void CreateTest()
		{
			// Succesful behavior
			_caRepo.Setup(repo => repo.Create(It.IsAny<CounteragentDTO>()));

			var result = _service.Create(new CounteragentDTO());

			Assert.AreEqual(true, result);

			// Exception behavior
			_caRepo.Setup(repo => repo.Create(It.IsAny<CounteragentDTO>())).Throws(new Exception());

			result = _service.Create(new CounteragentDTO());

			_caRepo.Verify(repo => repo.Create(It.IsAny<CounteragentDTO>()), Times.Exactly(2));
			Assert.AreEqual(false, result);
		}

		[TestMethod]
		public void ReadAllTest()
		{
			var list = new List<CounteragentDTO>
			{
				new CounteragentDTO(),
				new CounteragentDTO(),
				new CounteragentDTO()
			};
			_caRepo.Setup(repo => repo.ReadAll()).Returns(new List<CounteragentDTO>(list));

			// Succesful behavior
			var result = _service.ReadAll();

			Assert.AreEqual(list.Count, Enumerable.Count(result));

			// Failed behavior
			result = _service.ReadAll();

			list.RemoveAt(0);
			Assert.AreNotEqual(list.Count, Enumerable.Count(result));

			// Exception behavior
			_caRepo.Setup(repo => repo.ReadAll()).Throws(new Exception());

			result = _service.ReadAll();

			_caRepo.Verify(repo => repo.ReadAll(), Times.Exactly(3));
			Assert.AreEqual(null, result);
		}

		[TestMethod]
		public void ReadCustomTest()
		{
			var list = new List<CounteragentDTO>
			{
				new CounteragentDTO(),
				new CounteragentDTO(),
				new CounteragentDTO(),
				new CounteragentDTO()
			};
			list[0].Name = "Alice";
			list[1].Name = "Bob";
			list[2].Name = "Brego";
			list[3].Name = "Maria";
			_caRepo.Setup(repo => repo.Read(It.IsAny<Expression<Func<CounteragentDTO, bool>>>()))
				.Returns<Expression<Func<CounteragentDTO, bool>>>(arg => list.AsQueryable().Where(arg).ToList());

			// Succesful behavior
			var result = _service.ReadCustom(ca => ca.Name != "Maria");

			Assert.IsFalse(result.ToList().Exists(ca => ca.Name == "Maria"));

			// Exception behavior
			_caRepo.Setup(repo => repo.Read(It.IsAny<Expression<Func<CounteragentDTO, bool>>>()))
				.Throws(new Exception());

			result = _service.ReadCustom(ca => ca.Name != "Maria");

			_caRepo.Verify(repo => repo.Read(ca => ca.Name != "Maria"), Times.Exactly(2));
			Assert.AreEqual(null, result);
		}

		[TestMethod]
		public void UpdateTest()
		{
			// Succesful behavior
			_caRepo.Setup(repo => repo.Update(It.IsAny<CounteragentDTO>()));

			var result = _service.Update(new CounteragentDTO());

			Assert.AreEqual(true, result);

			// Exception behavior
			_caRepo.Setup(repo => repo.Update(It.IsAny<CounteragentDTO>())).Throws(new Exception());

			result = _service.Update(new CounteragentDTO());

			_caRepo.Verify(repo => repo.Update(It.IsAny<CounteragentDTO>()), Times.Exactly(2));
			Assert.AreEqual(false, result);
		}

		[TestMethod]
		public void DeleteTest()
		{
			// Succesful behavior
			_caRepo.Setup(repo => repo.Delete(It.IsAny<int>()));

			var result = _service.Delete(1);

			Assert.AreEqual(true, result);

			// Exception behavior
			_caRepo.Setup(repo => repo.Delete(It.IsAny<int>())).Throws(new Exception());

			result = _service.Delete(1);

			_caRepo.Verify(repo => repo.Delete(It.IsAny<int>()), Times.Exactly(2));
			Assert.AreEqual(false, result);
		}
	}
}