﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.BAL.Services;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.DAL;
using TODO.DAL.Repositories;

namespace TODO.BAL.Tests
{
	// Класс, наследуемый от TaskService. Создается для проверки приватного метода SetStateHelper
	[TestClass]
	public class DerivedTaskService : TaskService
	{
		public bool setStateHelperResult = true;
		public DerivedTaskService(IUnitOfWork unit, IRepository<TagDTO> tagRepository, IRepository<TaskDTO> taskRepository, int taskID, TaskStateType newState) : 
			base(tagRepository, taskRepository, unit)
		{
			setStateHelperResult = this.SetStateHelper(taskID, newState);
		}
	}


	[TestClass]
	public class TaskServiceTests
	{
		static Mock<IRepository<TaskDTO>> _taskRepository;
		static Mock<IRepository<CounteragentDTO>> _counteragentRepository;
		static Mock<IRepository<TagDTO>> _tagRepository;
		static Mock<ILogger> _logger;
		static Mock<IUnitOfWork> _unit;
		static TaskService _taskService;
		static string _errorMessage = "TestError";
		private List<TaskDTO> taskList
		{
			get
			{
				return new List<TaskDTO>
				{
					new TaskDTO{Title="Task1", DateDue = DateTime.Now, DateCreated = DateTime.Now, State = TaskStateType.InProgress},
					new TaskDTO{Title="Task2", DateDue = DateTime.Now, DateCreated = DateTime.Now, State = TaskStateType.InProgress},
					new TaskDTO{Title="Task3", DateDue = DateTime.Now, DateCreated = DateTime.Now, State = TaskStateType.Cancelled},
					new TaskDTO{Title="Task4", DateDue = DateTime.Now, DateCreated = DateTime.Now, State = TaskStateType.Completed},
				};
			}
		}

		public TaskServiceTests()
		{
			_counteragentRepository = _counteragentRepository ?? new Mock<IRepository<CounteragentDTO>>(MockBehavior.Strict);
			_taskRepository = _taskRepository ?? new Mock<IRepository<TaskDTO>>(MockBehavior.Strict);
			_tagRepository = _tagRepository ?? new Mock<IRepository<TagDTO>>(MockBehavior.Strict);
			_logger = _logger ?? new Mock<ILogger>();
			_unit = _unit ?? new Mock<IUnitOfWork>(MockBehavior.Strict);
			Mapper.Reset();
			_taskService = _taskService ?? new TaskService(_tagRepository.Object, _taskRepository.Object, _unit.Object);
			_unit.Setup(a => a.Complete());
		}

		[TestMethod]
		public void CreateTest()
		{
			//Successful
			_taskRepository.Setup(a => a.Create(It.IsAny<TaskDTO>()));
			var actualScs = _taskService.Create(new TaskDTO());

			//Exception
			_taskRepository.Setup(a => a.Create(It.IsAny<TaskDTO>())).Throws(new Exception(_errorMessage));
			var actualEx = _taskService.Create(new TaskDTO());

			//Assert
			Assert.AreEqual(true, actualScs);
			Assert.AreEqual(false, actualEx);
			Assert.AreEqual(_taskService.ErrorMessage, _errorMessage);
		}

		[TestMethod]
		public void ReadActiveTest()
		{
			//Successful
			_taskRepository.Setup(a => a.Read(It.IsAny<Expression<Func<TaskDTO, bool>>>()))
				.Returns<Expression<Func<TaskDTO, bool>>>(b => taskList.AsQueryable().Where(b));
			var actualScs = _taskService.ReadActive().ToList();
			var expected = taskList.AsQueryable().Where(x => x.State == TaskStateType.InProgress).OrderBy(x => x.DateDue).ToList();

			//Exception
			_taskRepository.Setup(a => a.Read(It.IsAny<Expression<Func<TaskDTO, bool>>>()))
				.Throws(new Exception(_errorMessage));
			var actualEx = _taskService.ReadActive();

			//Assert
			Assert.AreEqual(actualScs.Count, 2);
			Assert.AreEqual(expected[0].Title, actualScs[0].Title);
			Assert.AreEqual(expected[1].Title, actualScs[1].Title);
			Assert.AreEqual(null, actualEx);
			Assert.AreEqual(_taskService.ErrorMessage, _errorMessage);
		}

		[TestMethod]
		public void ReadArchiveTest()
		{
			//Successful
			_taskRepository.Setup(a => a.Read(It.IsAny<Expression<Func<TaskDTO, bool>>>()))
				.Returns<Expression<Func<TaskDTO, bool>>>(b => taskList.AsQueryable().Where(b));
			var actualScs = _taskService.ReadArchive().ToList();
			var expected = taskList.AsQueryable()
				.Where(x => x.State == TaskStateType.Completed || x.State == TaskStateType.Cancelled).ToList();

			//Exception
			_taskRepository.Setup(a => a.Read(It.IsAny<Expression<Func<TaskDTO, bool>>>()))
				.Throws(new Exception(_errorMessage));
			var actualEx = _taskService.ReadArchive();


			//Assert
			Assert.AreEqual(expected.Count, actualScs.Count);
			Assert.AreEqual(expected[0].Title, actualScs[0].Title);
			Assert.AreEqual(expected[1].Title, actualScs[1].Title);
			Assert.AreEqual(null, actualEx);
			Assert.AreEqual(_taskService.ErrorMessage, _errorMessage);
		}

		[TestMethod]
		public void ReadCustomTest()
		{
			//Successful
			_taskRepository.Setup(a => a.Read(It.IsAny<Expression<Func<TaskDTO, bool>>>()))
				.Returns<Expression<Func<TaskDTO, bool>>>(b => taskList.AsQueryable().Where(b));
			var actualScs = _taskService.ReadCustom(x => x.Title == "Task1").ToList();
			var expected = taskList.AsQueryable().Where(x => x.Title == "Task1").ToList();

			//Exception
			_taskRepository.Setup(a => a.Read(It.IsAny<Expression<Func<TaskDTO, bool>>>()))
				.Throws(new Exception(_errorMessage));
			var actualEx = _taskService.ReadCustom(x => x.Title == "Task1");


			//Assert
			Assert.AreEqual(expected.Count, actualScs.Count);
			Assert.AreEqual(expected[0].Title, actualScs[0].Title);
			Assert.AreEqual(null, actualEx);
			Assert.AreEqual(_taskService.ErrorMessage, _errorMessage);
		}

		// TODO : тест отключен из-за обновления метода Update
		// Update проверяет статус задачи до обновления и использует метод Read
		// Необходимо перенастроить моки
		//[TestMethod]
		//public void UpdateTest()
		//{
		//	//Successful
		//	_taskRepository.Setup(a => a.Update(It.IsAny<TaskDTO>()));
		//	var actualScs = _taskService.Update(new TaskDTO() { State = TaskStateType.InProgress});

		//	//Exception
		//	_taskRepository.Setup(a => a.Update(It.IsAny<TaskDTO>()))
		//		.Throws(new Exception(_errorMessage));
		//	var actualEx = _taskService.Update(new TaskDTO() { State = TaskStateType.InProgress });

		//	//Assert
		//	//Assert.AreEqual(true, actualScs);
		//	Assert.AreEqual(false, actualEx);
		//	Assert.AreEqual(_taskService.ErrorMessage, _errorMessage);
		//}

		[TestMethod]
		public void SetStateHelperTest()
		{
			//Successful
			var testTaskDTO = new TaskDTO() { DateCreated = DateTime.Now, ID = 1, State = TaskStateType.InProgress };
			_taskRepository.Setup(a => a.Read(1)).Returns(testTaskDTO);
			_taskRepository.Setup(a => a.Update(It.IsAny<TaskDTO>()));
			var derivedTaskService = new DerivedTaskService(_unit.Object,_tagRepository.Object, _taskRepository.Object, 1, TaskStateType.Completed);
			var actual = derivedTaskService.setStateHelperResult;

			//Exception
			_taskRepository.Setup(a => a.Read(1))
				.Throws(new Exception(_errorMessage));
			var derivedTaskServiceEx = new DerivedTaskService(_unit.Object, _tagRepository.Object, _taskRepository.Object, 1, TaskStateType.Completed);
			var actualEx = derivedTaskServiceEx.setStateHelperResult;

			//Assert
			Assert.AreEqual(true, actual);
			Assert.AreEqual(TaskStateType.Completed, testTaskDTO.State);
			Assert.AreEqual(false, actualEx);
			Assert.AreEqual(_errorMessage, derivedTaskServiceEx.ErrorMessage);
		}
		
		[TestMethod]
		public void SetCompleteTest()
		{
			//Arrange
			var testTaskDTO = new TaskDTO() { DateCreated = DateTime.Now, ID = 1, State = TaskStateType.InProgress };
			_taskRepository.Setup(a => a.Read(1)).Returns(testTaskDTO);
			_taskRepository.Setup(a => a.Update(It.IsAny<TaskDTO>()));

			//Act
			//var derivedTaskService = new DerivedTaskService(_dal.Object, 1, TaskStateType.Completed);
			var actual = _taskService.SetComplete(1);

			//Assert
			Assert.AreEqual(true, actual);
			Assert.AreEqual(TaskStateType.Completed, testTaskDTO.State);
		}

		[TestMethod]
		public void SetCancelTest()
		{
			//Arrange
			var testTaskDTO = new TaskDTO() { DateCreated = DateTime.Now, ID = 1, State = TaskStateType.Cancelled };
			_taskRepository.Setup(a => a.Read(1)).Returns(testTaskDTO);
			_taskRepository.Setup(a => a.Update(It.IsAny<TaskDTO>()));

			//Act
			//var derivedTaskService = new DerivedTaskService(_dal.Object, 1, TaskStateType.Completed);
			var actual = _taskService.SetCancel(1);

			//Assert
			Assert.AreEqual(true, actual);
			Assert.AreEqual(TaskStateType.Cancelled, testTaskDTO.State);
		}

		[TestMethod]
		public void RepeatTest()
		{
			//Arrange
			var testCounteragentDTO = new CounteragentDTO();
			var testTaskDTO = new TaskDTO()
			{
				DateCreated = DateTime.Now,
				Title = "RepeatTest",
				Description = "RepeatTestDescription",
				DateDue = DateTime.Now,
				Counteragent = testCounteragentDTO,
				State = TaskStateType.InProgress
			};
			var repeatResultDTO = new TaskDTO();
			_taskRepository.Setup(a => a.Create(It.IsAny<TaskDTO>())).Callback<TaskDTO>((TaskDTO task) => repeatResultDTO = task);

			//Act
			var actual = _taskService.Repeat(testTaskDTO);

			//Assert
			Assert.AreEqual(true, actual);
			Assert.AreEqual("RepeatTest", repeatResultDTO.Title);
			Assert.AreEqual(testTaskDTO.Description, repeatResultDTO.Description);
			Assert.AreEqual(testTaskDTO.DateDue, repeatResultDTO.DateDue);
			Assert.AreEqual(testTaskDTO.Counteragent, repeatResultDTO.Counteragent);
			Assert.AreEqual(testTaskDTO.State, repeatResultDTO.State);
		}

		[TestMethod]
		public void ChangeTagsTest()
		{
			//Arrange
			var tagString = "  Tag2     , Tag3,   Tag4  , Tag 5 ";
			ICollection<TagDTO> tags = new List<TagDTO>()
			{
				new TagDTO(){Name = "Tag1"},
				new TagDTO(){Name = "Tag2"}
			};

			ICollection<TagDTO> expectedTags = new List<TagDTO>()
			{
				new TagDTO(){Name = "Tag2"},
				new TagDTO(){Name = "Tag3"},
				new TagDTO(){Name = "Tag4"},
			    new TagDTO(){Name = "Tag 5"}
			};
			
			var testTaskDTO = new TaskDTO()
			{
				DateCreated = DateTime.Now,
				Title = "RepeatTest",
				Description = "RepeatTestDescription",
				DateDue = DateTime.Now,
				State = TaskStateType.InProgress,
				Tags = new List<TagDTO>()
			};
			var newTaskDTO = testTaskDTO;
			_tagRepository.Setup(x => x.Create(It.IsAny<TagDTO>()));
			_tagRepository.Setup(x => x.ReadAll())
				.Returns(expectedTags);

			//Act
			var actual = _taskService.ChangeTags(ref newTaskDTO, tagString);

			//Assert
			Assert.AreEqual(true, actual);
			Assert.AreEqual(expectedTags.Count, testTaskDTO.Tags.Count);
			Assert.AreEqual(expectedTags.ToArray()[0].Name, testTaskDTO.Tags.ToArray()[0].Name);
			Assert.AreEqual(expectedTags.ToArray()[1].Name, testTaskDTO.Tags.ToArray()[1].Name);
			Assert.AreEqual(expectedTags.ToArray()[2].Name, testTaskDTO.Tags.ToArray()[2].Name);
			Assert.AreEqual(expectedTags.ToArray()[3].Name, testTaskDTO.Tags.ToArray()[3].Name);
		}
	}
}
