﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TODO.BAL.Services;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;

namespace TODO.BAL.Tests
{
	[TestClass]
	public class TestProjectService : ProjectService
	{
		public TestProjectService(IRepository<ProjectDTO> projects, IRepository<TagDTO> tags, IUnitOfWork unit, ILogger logger) : base(projects, tags, unit, logger)
		{
		}

		public bool TestSetState(int projectID, ProjectStateType state)
		{
			return this.SetState(projectID, state);
		}
	}

	/// <summary>
	/// Summary description for ProjectServiceTests
	/// </summary>
	[TestClass]
	public class ProjectServiceTests
	{
		static Mock<IRepository<ProjectDTO>> _projectRepository;
		static Mock<ILogger> _logger;
		static Mock<IRepository<TagDTO>> _tagRepository;
		static Mock<IUnitOfWork> _unit;
		static ProjectService _projectService;
		static TestProjectService _testProjectService;
		static List<ProjectDTO> _sampleData;

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		/// <summary>
		/// Выполняется перед каждым тестом
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_projectRepository = new Mock<IRepository<ProjectDTO>>(MockBehavior.Strict);
			_tagRepository = new Mock<IRepository<TagDTO>>(MockBehavior.Strict);
			_logger = new Mock<ILogger>();
			_unit = new Mock<IUnitOfWork>(MockBehavior.Strict);
			_projectService = new ProjectService(_projectRepository.Object, _tagRepository.Object, _unit.Object, _logger.Object);
			_testProjectService = new TestProjectService(_projectRepository.Object, _tagRepository.Object, _unit.Object, _logger.Object);

			_unit.Setup(x => x.Complete());

			_sampleData = new List<ProjectDTO>
			{
				new ProjectDTO { ID = 1, Title = "Project1", DateCreated = DateTime.Now, DateDuePlanned = DateTime.Now.AddSeconds(-1), State = ProjectStateType.InProgress },
				new ProjectDTO { ID = 2, Title = "Project2", DateCreated = DateTime.Now, DateDuePlanned = DateTime.Now.AddSeconds(-1), State = ProjectStateType.Cancelled },
				new ProjectDTO { ID = 3, Title = "Project3", DateCreated = DateTime.Now, DateDuePlanned = DateTime.Now.AddSeconds(-1), State = ProjectStateType.Completed },
				new ProjectDTO { ID = 4, Title = "Project4", DateCreated = DateTime.Now, DateDuePlanned = DateTime.Now.AddSeconds(-1), State = ProjectStateType.Deleted }
			};
		}

		[TestMethod]
		public void Create_Test()
		{
			_projectRepository.Setup(x => x.Create(It.IsAny<ProjectDTO>()));
			var created = _projectService.Create(new ProjectDTO());
			Assert.AreEqual<bool>(true, created);

			_projectRepository.Setup(a => a.Create(It.IsAny<ProjectDTO>())).Throws<Exception>();
			created = _projectService.Create(new ProjectDTO());
			Assert.AreEqual(false, created);
		}

		[TestMethod]
		public void ReadActive_Test()
		{
			_projectRepository.Setup(x => x.Read(It.IsAny<Expression<Func<ProjectDTO, bool>>>()))
				.Returns<Expression<Func<ProjectDTO, bool>>>(x => _sampleData.Where(y => y.State == ProjectStateType.InProgress).AsQueryable());
			var active = _projectService.ReadActive();

			_projectRepository.Setup(x => x.Read(It.IsAny<Expression<Func<ProjectDTO, bool>>>()))
				.Throws(new Exception());
			var ex = _projectService.ReadActive();

			Assert.AreEqual(1, active.Count());
			Assert.IsTrue(active.All(x => x.State == ProjectStateType.InProgress));
			Assert.IsNull(ex);
		}

		[TestMethod]
		public void ReadArchive_Test()
		{
			_projectRepository.Setup(x => x.Read(It.IsAny<Expression<Func<ProjectDTO, bool>>>()))
				.Returns<Expression<Func<ProjectDTO, bool>>>(x => _sampleData.Where(
					y => y.State == Common.Types.ProjectStateType.Cancelled || y.State == ProjectStateType.Completed).AsQueryable());
			var active = _projectService.ReadActive();

			_projectRepository.Setup(x => x.Read(It.IsAny<Expression<Func<ProjectDTO, bool>>>()))
				.Throws(new Exception());
			var ex = _projectService.ReadActive();

			Assert.AreEqual(2, active.Count());
			Assert.IsTrue(active.All(x => x.State == ProjectStateType.Cancelled || x.State == ProjectStateType.Completed));
			Assert.IsNull(ex);
		}

		[TestMethod]
		public void SetState_Test()
		{
			var project = _sampleData.Find(x => x.ID == 1);

			_projectRepository.Setup(x => x.Read(1))
				.Returns(project);
			_projectRepository.Setup(x => x.Update(It.IsAny<ProjectDTO>()));

			var result = _testProjectService.TestSetState(1, ProjectStateType.Cancelled);

			Assert.IsTrue(result);
			Assert.AreEqual(ProjectStateType.Cancelled, project.State);

			result = _testProjectService.TestSetState(1, ProjectStateType.Completed);

			Assert.IsTrue(result);
			Assert.AreEqual(ProjectStateType.Completed, project.State);

			result = _testProjectService.TestSetState(1, ProjectStateType.Deleted);

			Assert.IsTrue(result);
			Assert.AreEqual(ProjectStateType.Deleted, project.State);

			result = _testProjectService.TestSetState(1, ProjectStateType.InProgress);

			Assert.IsTrue(result);
			Assert.AreEqual(ProjectStateType.InProgress, project.State);

			_projectRepository.Setup(x => x.Update(It.IsAny<ProjectDTO>()))
				.Throws(new Exception());

			result = _testProjectService.TestSetState(1, ProjectStateType.Cancelled);

			Assert.IsFalse(result);
		}
	}
}
