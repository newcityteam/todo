﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.BAL.Services;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.DAL;
using TODO.DAL.Repositories;

namespace TODO.BAL.Tests
{
	[TestClass]
	public class FinanceOperationServiceTests
	{
		static Mock<IRepository<FinanceOperationDTO>> _finOpRepository;
		static Mock<IRepository<ProjectDTO>> _projectRepository;
		static Mock<IRepository<TaskDTO>> _taskRepository;
		static Mock<ILogger> _logger;
		static Mock<IUnitOfWork> _unit;
		static FinanceOperationService _finOpService;
		static string _errorMessage = "TestError";
		private List<FinanceOperationDTO> finOpList
		{
			get
			{
				return new List<FinanceOperationDTO>
				{
					new FinanceOperationDTO{Date = DateTime.Now, AmountIncome = 0, AmountOutcome = 10000, Description="default1"},
					new FinanceOperationDTO{Date = DateTime.Now, AmountIncome = 10000, AmountOutcome = 0, Description="default2"},
					new FinanceOperationDTO{Date = DateTime.Now, AmountIncome = 500, AmountOutcome = 10000, Description="default3"},
					new FinanceOperationDTO{Date = DateTime.Now, AmountIncome = 1000, AmountOutcome = 0, Description="default4" }
				};
			}
		}

		public FinanceOperationServiceTests()
		{
			_finOpRepository = new Mock<IRepository<FinanceOperationDTO>>(MockBehavior.Strict);
			_projectRepository = new Mock<IRepository<ProjectDTO>>(MockBehavior.Strict);
			_taskRepository = new Mock<IRepository<TaskDTO>>(MockBehavior.Strict);
			_logger = new Mock<ILogger>(MockBehavior.Strict);
			_unit = new Mock<IUnitOfWork>();
			_unit.Setup(a => a.Complete());
			_finOpService = new FinanceOperationService(_finOpRepository.Object, _unit.Object, _taskRepository.Object, _projectRepository.Object);
		}

		[TestMethod]
		public void CheckerTest()
		{
			var _finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now.AddDays(10), AmountIncome = 1, AmountOutcome = 0, ProjectID = 1, TaskID = 1 };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 0, AmountOutcome = -1, ProjectID = 1, TaskID = 1 };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = -1, AmountOutcome = 0, ProjectID = 1, TaskID = 1 };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 0, AmountOutcome = 0, ProjectID = 1, TaskID = 1 };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 1, AmountOutcome = 1, ProjectID = 1, TaskID = 1 };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 1, AmountOutcome = 0, ProjectID = null, TaskID = 1 };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 0, AmountOutcome = 1, ProjectID = 1, TaskID = null };
			Assert.AreEqual(false, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 1, AmountOutcome = 0, ProjectID = 1, TaskID = null };
			Assert.AreEqual(true, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 0, AmountOutcome = 1, ProjectID = null, TaskID = 1 };
			Assert.AreEqual(true, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));

			_finOpDTOItem = new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 0, AmountOutcome = 1, ProjectID = 1, TaskID = 1 };
			Assert.AreEqual(true, _finOpService.IsFinanceOperationCorrect(_finOpDTOItem));
		}

		[TestMethod]
		public void CreateTest()
		{
			// Success
			_finOpRepository.Setup(a => a.Create(It.IsAny<FinanceOperationDTO>()));
			var actualSc = _finOpService.Create(new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 1, AmountOutcome = 0, ProjectID = 1, TaskID = null });
			Assert.AreEqual(true, actualSc);

			// Exception by Checker
			var actualExbyChecker = _finOpService.Create(new FinanceOperationDTO());
			Assert.AreEqual(false, actualExbyChecker);
			Assert.AreEqual("Incorrect financial operation", _finOpService.ErrorMessage);

			// Exception by Mock
			_finOpRepository.Setup(a => a.Create(It.IsAny<FinanceOperationDTO>())).Throws(new Exception(_errorMessage));
			var actualEx = _finOpService.Create(new FinanceOperationDTO() { Date = DateTime.Now, AmountIncome = 1, AmountOutcome = 0, ProjectID = 1, TaskID = null });
			Assert.AreEqual(false, actualEx);
			Assert.AreEqual(_errorMessage, _finOpService.ErrorMessage);
		}



	}
}
