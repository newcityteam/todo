﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL
{
	/// <summary>
	/// Класс, реализующий паттерн Unit Of Work для БД
	/// На вход и выход всех репозиториев передаются DTO-модели
	/// Работа с Entity-моделями происходит только в репозиториях
	/// </summary>
	public class UnitOfWork: IUnitOfWork
	{
		/// <summary>
		/// Контекст БД
		/// </summary>
		private TODOContext _context { get; set; }

		/// <summary>
		/// Конструктор UnitOfWork. 
		/// </summary>
		public UnitOfWork(TODOContext context)
		{
			_context = context;
			MapperInitialize();
		}

		/// <summary>
		/// Сохранение всех изменений в БД
		/// </summary>
		public void Complete()
		{
			_context.SaveChanges();
		}

		/// <summary>
		/// Инициализация маппера.
		/// У слоя DAL свой маппер, т.к. Entity-модели должны быть видны только в этом слое.
		/// Наружу DAL отдает DTO-модели
		/// </summary>
		private void MapperInitialize()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.CreateMap<TaskEntity, TaskDTO>()
				.ReverseMap()
				.ForMember(m => m.CounteragentID, opt => opt.MapFrom(src => src.Counteragent.ID))
				.ForMember(m => m.Counteragent, opt => opt.Ignore())
				.ForMember(m => m.ProjectID, opt => opt.MapFrom(src => src.Project.ID))
				.ForMember(m => m.Project, opt => opt.Ignore());

				cfg.CreateMap<CounteragentEntity, CounteragentDTO>()
				.ReverseMap()
				.ForMember(m => m.Tasks, opt => opt.Ignore())
				.ForMember(m => m.Projects, opt => opt.Ignore());

				cfg.CreateMap<TagEntity, TagDTO>()
				.ReverseMap();

				cfg.CreateMap<ProjectEntity, ProjectDTO>()
				.ReverseMap()
				.ForMember(m => m.CounteragentID, opt => opt.MapFrom(src => src.Counteragent.ID))
				.ForMember(m => m.Counteragent, opt => opt.Ignore())
				.ForMember(m => m.Tasks, opt => opt.Ignore())
				.ForMember(m => m.IncomeFinance, opt => opt.Ignore());

				cfg.CreateMap<FinanceOperationEntity, FinanceOperationDTO>()
				.ReverseMap()
				.ForMember(m => m.Task, opt => opt.Ignore())
				.ForMember(m => m.Project, opt => opt.Ignore())
				.ForMember(m => m.TaskID, opt => opt.MapFrom(src => src.Task.ID))
				.ForMember(m => m.ProjectID, opt => opt.MapFrom(src => src.Project.ID));
			});
		}

	}
}
