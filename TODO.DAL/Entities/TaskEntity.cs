﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Types;

namespace TODO.DAL.Entities
{
	/// <summary>
	/// Сущность Задача
	/// </summary>
	public class TaskEntity
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Название задачи
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Описание задачи
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Дата создания задачи
		/// </summary>
		public DateTime DateCreated { get; set; }

		/// <summary>
		/// Планируемая дата выполнения задачи
		/// </summary>
		public DateTime? DateDue { get; set; }

		/// <summary>
		/// Статус задачи
		/// </summary>
		public TaskStateType State { get; set; }

		/// <summary>
		/// Тип задачи: Исходящая / Входящая
		/// </summary>
		public TaskDirectionType Direction { get; set; }

		/// <summary>
		/// Сумма планируемых затрат
		/// </summary>
		public decimal PlannedAmount { get; set; }

	    /// <summary>
	    /// Идентификатор соответствующего задаче контрагента
	    /// </summary>
	    public int? CounteragentID { get; set; }
	    /// <summary>
	    /// Соответствующий задаче контрагент
	    /// </summary>
	    public CounteragentEntity Counteragent { get; set; }

		/// <summary>
		/// Теги для задачи
		/// </summary>
		public ICollection<TagEntity> Tags { get; set; }

		/// <summary>
		/// Финансовые операции по задаче
		/// </summary>
		public ICollection<FinanceOperationEntity> FinanceOperations { get; set; }

		/// <summary>
		/// Идентификатор соответствующего задаче проекта
		/// </summary>
		public int? ProjectID { get; set; }

		/// <summary>
		/// Соответствующий задаче проект
		/// </summary>
		public ProjectEntity Project { get; set; }
	}
}
