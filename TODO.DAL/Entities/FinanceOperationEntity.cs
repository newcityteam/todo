﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.DAL.Entities
{
	/// <summary>
	/// Сущность Финансовая операция. 
	/// </summary>
	[Table("FinanceOperations")]
	public class FinanceOperationEntity
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Идентификатор задачи, в рамках которой произошел платеж (только для операций по кредиту)
		/// </summary>
		public int? TaskID { get; set; }

		/// <summary>
		/// Идентификатор проекта, в рамках которого произошел платеж
		/// </summary>
		public int? ProjectID { get; set; }

		/// <summary>
		/// Дата поступления платежа
		/// </summary>
		public DateTime Date { get; set; }

		/// <summary>
		/// Сумма операции по дебету (нам заплатили)
		/// </summary>
		public decimal AmountIncome { get; set; }

		/// <summary>
		/// Сумма операции по кредиту (мы заплатили)
		/// </summary>
		public decimal AmountOutcome { get; set; }

		/// <summary>
		/// Описание и примечания
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Навигационное свойство для проектов
		/// </summary>
		public ProjectEntity Project { get; set; }

		/// <summary>
		/// Навигационное свойство для задач
		/// </summary>
		public TaskEntity Task { get; set; }
	}
}
