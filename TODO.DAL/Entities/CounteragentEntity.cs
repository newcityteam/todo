﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Types;

namespace TODO.DAL.Entities
{
	/// <summary>
	/// Сущность Контрагент
	/// </summary>
	public class CounteragentEntity
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }
		
		/// <summary>
		/// ФИО / наименование
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Адрес
		/// </summary>
		public string Address { get; set; }
		
		/// <summary>
		/// Телефон
		/// </summary>
		public string PhoneNumber { get; set; }
		
		/// <summary>
		/// Примечания
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// ИНН - идентификационный номер налогоплательщика
		/// </summary>
		public string INN { get; set; }

		/// <summary>
		/// КПП - код причины постановки на учет
		/// </summary>
		public string KPP { get; set; }

		/// <summary>
		/// Расчетный счёт
		/// </summary>
		public string PaymentAccount { get; set; }

		/// <summary>
		/// Тип контрагента
		/// </summary>
		public CounteragentType Type { get; set; }

        /// <summary>
        /// Задачи, связанные с контрагентом
        /// </summary>
        public ICollection<TaskEntity> Tasks { get; set; }

		/// <summary>
		/// Проекты, связанные с контрагентом
		/// </summary>
		public ICollection<ProjectEntity> Projects { get; set; }
	}
}
