﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.DAL.Entities
{
	/// <summary>
	/// Сущность тег
	/// </summary>
	public class TagEntity
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Название тега
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Навигационное свойство для Проектов
		/// </summary>
		public ICollection<ProjectEntity> Projects { get; set; }

		/// <summary>
		/// Навигационное свойство для Задач
		/// </summary>
		public ICollection<TaskEntity> Tasks { get; set; }
	}
}
