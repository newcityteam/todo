﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TODO.DAL.Entities;
using TODO.Common.Types;

namespace TODO.DAL.Context
{
	class TODOInitializerDEV : DropCreateDatabaseAlways<TODOContext>
	{
		protected override void Seed(TODOContext context)
		{
			context.CounteragentSet.Add(new CounteragentEntity
			{
				Name = "User",
				PhoneNumber = "8 555 33 114 88",
				Address = "Some city",
				Description = "Make Alice great again",
				Type = CounteragentType.IndividualPerson
			});

			context.CounteragentSet.Add(new CounteragentEntity
			{
				Name = "Counteragent 1",
				PhoneNumber = "8 927 11 234 32",
				Address = "TLT address, street ",
				INN = "111222333444",
				KPP = "100200300",
				PaymentAccount = "12 12 23 23 34 34",
				Description = "Some description for Counteragent 1",
				Type = CounteragentType.Legal
			});

			context.CounteragentSet.Add(new CounteragentEntity
			{
				Name = "Counteragent 2",
				PhoneNumber = "8 927 11 234 32",
				Address = "Some address, street",
				INN = "777888000555",
				KPP = "450345323",
				PaymentAccount = "22 13 83 28 37 74",
				Description = "Some description for Counteragent 2",
				Type = CounteragentType.Legal
			});

			context.CounteragentSet.Add(new CounteragentEntity
			{
				Name = "Alice",
				PhoneNumber = "8 555 33 114 88",
				Address = "Some city",
				Description = "Make Alice great again",
				Type = CounteragentType.IndividualPerson
			});
			
			context.ProjectSet.Add(new ProjectEntity
			{
				Title = "Project1",
				Description = "Has three tasks, no tag, but one task was cancelled. Counteragetn User. InProgress",
				DateCreated = DateTime.Now.AddDays(-1),
				DateDuePlanned = DateTime.Now.AddDays(5),
				State = ProjectStateType.InProgress,
				CounteragentID = 1
			});

			context.ProjectSet.Add(new ProjectEntity
			{
				Title = "Project2",
				Description = "Has two tasks, no tag, counteragent 1. In Progress",
				DateCreated = DateTime.Now.AddDays(-1),
				DateDuePlanned = DateTime.Now.AddDays(5),
				State = ProjectStateType.InProgress,
				CounteragentID = 2
			});

			context.ProjectSet.Add(new ProjectEntity
			{
				Title = "Project3",
				Description = "Has no tasks, no tag, counteragent 2. InProgress",
				DateCreated = DateTime.Now.AddDays(-1),
				DateDuePlanned = DateTime.Now.AddDays(5),
				State = ProjectStateType.InProgress,
				CounteragentID = 3
			});

			context.TaskSet.Add(new TaskEntity
            {
                Title = "Task number one",
                Description = "Has no project, no counteragent, no tags. Cancelled. Planned amount 100",
                DateCreated = DateTime.Now.AddDays(-1),
                DateDue = DateTime.Now.AddDays(5),
                State = TaskStateType.Cancelled,
                Direction = TaskDirectionType.Outcome,
                CounteragentID = null,
				ProjectID=null,
				PlannedAmount = 100
            });

            context.TaskSet.Add(new TaskEntity
            {
                Title = "Task number two",
                Description = "Has no project, no tag, no planned amount. Counteragent - User. InProgress",
                DateCreated = DateTime.Now,
                DateDue = DateTime.Now.AddDays(1),
                State = TaskStateType.InProgress,
                Direction = TaskDirectionType.Income,
                CounteragentID = 1,
				ProjectID=null
            });

            context.TaskSet.Add(new TaskEntity
            {
                Title = "Task number three",
                Description = "Has no project, no tag. Counteragent 2. InProgress. PlannedAmount = 200",
                DateCreated = DateTime.Now.AddMinutes(-6),
                DateDue = DateTime.Now.AddDays(-1),
                State = TaskStateType.InProgress,
                Direction = TaskDirectionType.Income,
                CounteragentID = 3,
	            PlannedAmount = 200
			});

			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number four.",
				Description = "Project 1, counteragetn User. Cancelled",
				DateCreated = DateTime.Now.AddDays(-1),
				DateDue = DateTime.Now.AddDays(5),
				State = TaskStateType.Cancelled,
				Direction = TaskDirectionType.Outcome,
				CounteragentID = 1,
				ProjectID = 1
			});

			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number five.",
				Description = "Project 1, counteragetn User. InProgress. PlannedAmount = 300",
				DateCreated = DateTime.Now,
				DateDue = DateTime.Now.AddDays(1),
				State = TaskStateType.InProgress,
				Direction = TaskDirectionType.Income,
				CounteragentID = 1,
				ProjectID = 1,
				PlannedAmount = 300
			});

			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number six",
				Description = "Project 1, counteragetn User. Completed. PlannedAmount = 600",
				DateCreated = DateTime.Now.AddDays(-1),
				DateDue = DateTime.Now.AddDays(5),
				State = TaskStateType.Completed,
				Direction = TaskDirectionType.Outcome,
				CounteragentID = 1,
				ProjectID = 1,
				PlannedAmount = 600
			});

			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number seven",
				Description = "Project 2, counteragetn User. Completed. PlannedAmount = 700",
				DateCreated = DateTime.Now,
				DateDue = DateTime.Now.AddDays(1),
				State = TaskStateType.Completed,
				Direction = TaskDirectionType.Income,
				CounteragentID = 1,
				ProjectID = 2,
				PlannedAmount = 700
			});

			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number eight",
				Description = "Project 2, counteragetn 1. InProgress. PlannedAmount = 800",
				DateCreated = DateTime.Now.AddMinutes(-6),
				DateDue = DateTime.Now.AddDays(-1),
				State = TaskStateType.InProgress,
				Direction = TaskDirectionType.Income,
				CounteragentID = 2,
				ProjectID = 2,
				PlannedAmount = 800
			});

			context.TagSet.Add(new TagEntity { Name = "ремонт" });
			context.TagSet.Add(new TagEntity { Name = "реклама" });
			context.TagSet.Add(new TagEntity { Name = "еда" });

			base.Seed(context);
        }
	}
}
