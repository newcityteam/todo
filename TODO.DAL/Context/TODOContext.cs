﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.DAL.Entities;

namespace TODO.DAL.Context
{
	/// <summary>
	/// Контекст БД
	/// </summary>
	public class TODOContext : DbContext
	{
		/// <summary>
		/// DbSet Задачи
		/// </summary>
		public DbSet<TaskEntity> TaskSet { get; set; }

		/// <summary>
		/// Dbset Контрагенты
		/// </summary>
		public DbSet<CounteragentEntity> CounteragentSet { get; set; }

		/// <summary>
		/// Dbset Контрагенты
		/// </summary>
		public DbSet<ProjectEntity> ProjectSet { get; set; }

		/// <summary>
		/// Dbset Проекты
		/// </summary>
		public DbSet<TagEntity> TagSet { get; set; }

		/// <summary>
		/// DbSet финансовых операций
		/// </summary>
		public DbSet<FinanceOperationEntity> FinanceOperationSet { get; set; }
		
		/// <summary>
		/// Инициализатор контекста БД
		/// </summary>
		/// <param name="connectionString">Строка подключения к БД.</param>
		public TODOContext(string connectionString) : base(connectionString)
		{

		}

		/// <summary>
		/// Конструктор БД и сущностей
		/// </summary>
		/// <param name="modelBuilder"></param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			// Tasks

			modelBuilder.Entity<TaskEntity>()
				.ToTable("Tasks")
				.HasKey(task => task.ID);

			modelBuilder.Entity<TaskEntity>().Property(task => task.Title).IsRequired().HasMaxLength(100);
			modelBuilder.Entity<TaskEntity>().Property(task => task.Description).IsOptional().HasMaxLength(1000);
			modelBuilder.Entity<TaskEntity>().Property(task => task.DateCreated).IsRequired();
			modelBuilder.Entity<TaskEntity>().Property(task => task.DateDue).IsOptional();
			modelBuilder.Entity<TaskEntity>().Property(task => task.State).IsRequired();
			modelBuilder.Entity<TaskEntity>().Property(task => task.Direction).IsRequired();

			modelBuilder.Entity<TaskEntity>()
				.HasOptional(task => task.Counteragent)
                .WithMany(cAgent => cAgent.Tasks)
                .HasForeignKey(task => task.CounteragentID)
                .WillCascadeOnDelete(false);

			modelBuilder.Entity<TaskEntity>()
				.HasOptional(task => task.Project)
				.WithMany(project => project.Tasks)
				.HasForeignKey(task => task.ProjectID)
				.WillCascadeOnDelete(false);

			// Counteragents

			modelBuilder.Entity<CounteragentEntity>()
				.ToTable("Counteragents")
				.HasKey(counteragent => counteragent.ID);

			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.Name).IsRequired().HasMaxLength(50);
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.Address).IsOptional().HasMaxLength(100);
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.PhoneNumber).IsOptional().HasMaxLength(30);
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.Description).IsOptional().HasMaxLength(1000);
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.INN).IsOptional().HasMaxLength(12); // ИНН - 12 цифр.
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.KPP).IsOptional().HasMaxLength(9); // КПП - 9 цифр.
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.PaymentAccount).IsOptional().HasMaxLength(30);
			modelBuilder.Entity<CounteragentEntity>().Property(counteragent => counteragent.Type).IsRequired();

			// Tags

			modelBuilder.Entity<TagEntity>()
				.ToTable("Tags")
				.HasKey(tag => tag.ID);

			modelBuilder.Entity<TagEntity>().Property(tag => tag.Name).IsRequired().HasMaxLength(20);

			modelBuilder.Entity<TagEntity>()
				.HasMany(tag => tag.Tasks)
				.WithMany(task => task.Tags)
				.Map(m =>
				{
					m.ToTable("TagsTasks");
					m.MapLeftKey("TagId");
					m.MapRightKey("TaskId");
				});

			modelBuilder.Entity<TagEntity>()
				.HasMany(tag => tag.Projects)
				.WithMany(project => project.Tags)
				.Map(m =>
				{
					m.ToTable("TagsProjects");
					m.MapLeftKey("TagId");
					m.MapRightKey("ProjectId");
				});

			// Projects

			modelBuilder.Entity<ProjectEntity>()
				.ToTable("Projects")
				.HasKey(project => project.ID);

			modelBuilder.Entity<ProjectEntity>().Property(project => project.Title).IsRequired().HasMaxLength(100);
			modelBuilder.Entity<ProjectEntity>().Property(project => project.Description).IsOptional().HasMaxLength(1000);
			modelBuilder.Entity<ProjectEntity>().Property(project => project.DateCreated).IsRequired();
			modelBuilder.Entity<ProjectEntity>().Property(project => project.DateDuePlanned).IsOptional();
			modelBuilder.Entity<ProjectEntity>().Property(project => project.DateDueFact).IsOptional();
			modelBuilder.Entity<ProjectEntity>().Property(project => project.State).IsRequired();
			modelBuilder.Entity<ProjectEntity>().Property(project => project.PlannedAmount).IsOptional();

			modelBuilder.Entity<ProjectEntity>()
				.HasRequired(project => project.Counteragent)
				.WithMany(cAgent => cAgent.Projects)
				.HasForeignKey(project => project.CounteragentID)
				.WillCascadeOnDelete(false);

			// FinanceOperations

			modelBuilder.Entity<FinanceOperationEntity>()
				.ToTable("FinanceOperations")
				.HasKey(fin => fin.ID);

			modelBuilder.Entity<FinanceOperationEntity>().Property(fin => fin.Date).IsRequired();
			modelBuilder.Entity<FinanceOperationEntity>().Property(fin => fin.AmountIncome).IsRequired().HasPrecision(15,2);
			modelBuilder.Entity<FinanceOperationEntity>().Property(fin => fin.AmountOutcome).IsRequired().HasPrecision(15, 2);
			modelBuilder.Entity<FinanceOperationEntity>().Property(fin => fin.Description).IsOptional().HasMaxLength(1000);

			modelBuilder.Entity<FinanceOperationEntity>()
				.HasOptional(fin => fin.Project)
				.WithMany(project => project.IncomeFinance)
				.HasForeignKey(fin => fin.ProjectID)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FinanceOperationEntity>()
				.HasOptional(fin => fin.Task)
				.WithMany(task => task.FinanceOperations)
				.HasForeignKey(fin => fin.TaskID)
				.WillCascadeOnDelete(false);

			Configuration.LazyLoadingEnabled = false;

#if DEBUG
			Database.SetInitializer(new TODOInitializerDEV());
#else
			Database.SetInitializer(new TODOInitializerPROD());
#endif
			base.OnModelCreating(modelBuilder);
		}
	}
}
