﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// Репозиторий для работы с сущностью "Финансовая операция"
	/// </summary>
	public class FinanceOperationRepository : IRepository<FinanceOperationDTO>
	{
		private TODOContext _context;

		public FinanceOperationRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект FinanceOperationDTO</param>
		public void Create(FinanceOperationDTO item)
		{
			var entity = Mapper.Map<FinanceOperationEntity>(item);
			_context.FinanceOperationSet.Add(entity);

			// сохраняем в БД, чтобы получить ID нового объекта
			_context.SaveChanges();
			item.ID = entity.ID;
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект FinanceOperationDTO</returns>
		public FinanceOperationDTO Read(int id)
		{
			var entity = _context.FinanceOperationSet.Find(id);
			if (entity == null)
			{
				throw new NullReferenceException();
			}
			_context.Entry(entity).Reference(r => r.Task).Load();
			_context.Entry(entity).Reference(r => r.Project).Load();
			var dto = Mapper.Map<FinanceOperationDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов FinanceOperationDTO</returns>
		public IEnumerable<FinanceOperationDTO> Read(Expression<Func<FinanceOperationDTO, bool>> query)
		{
			var entities = _context.FinanceOperationSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Reference(r => r.Task).Load();
				_context.Entry(entity).Reference(r => r.Project).Load();
			}
			return Mapper.Map<IEnumerable<FinanceOperationDTO>>(entities).AsQueryable().Where(query).ToList();
		}

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов FinanceOperationDTO</returns>
		public IEnumerable<FinanceOperationDTO> ReadAll()
		{
			var entities = _context.FinanceOperationSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Reference(r => r.Task).Load();
				_context.Entry(entity).Reference(r => r.Project).Load();
			}
			return Mapper.Map<IEnumerable<FinanceOperationDTO>>(entities);
		}

		/// <summary>
		/// Обновление объекта не поддерживается для Финансовых операций
		/// </summary>
		/// <param name="item">Объект CounteragentDTO</param>
		public void Update(FinanceOperationDTO item)
		{
			throw new Exception("Method 'Update' is not allowed for a financial operation");
		}

		/// <summary>
		/// Удаление объекта не поддерживается для финансовых операций
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		public void Delete(int id)
		{
			throw new Exception("Method 'Delete' is not allowed for a financial operation");
		}
	}
}
