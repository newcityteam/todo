﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// Репозиторий для работы с сущностью Задача.
	/// Обработку исключений выполнять в вышестоящих классах
	/// </summary>
	public class TaskRepository : IRepository<TaskDTO>
	{
		private TODOContext _context;

		public TaskRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект TaskDTO</param>
		public void Create(TaskDTO item)
		{
			var entity = Mapper.Map<TaskEntity>(item);
			var tags = new List<TagEntity>();
			var dbSet = _context.TagSet;
			foreach (var t in item.Tags)
			{
				var tag = dbSet.Find(t.ID) ?? Mapper.Map<TagEntity>(t);
				tags.Add(tag);
			}
			entity.Tags = tags;
			_context.TaskSet.Add(entity);
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект TaskDTO</returns>
		public TaskDTO Read(int id)
		{
			var entity = _context.TaskSet.Find(id);
			if (entity == null)
			{
				throw new NullReferenceException();
			}
			_context.Entry(entity).Reference(r => r.Counteragent).Load();
			_context.Entry(entity).Reference(r => r.Project).Load();
			_context.Entry(entity).Collection(r => r.Tags).Load();
			var dto = Mapper.Map<TaskDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов TaskDTO</returns>
		public IEnumerable<TaskDTO> Read(Expression<Func<TaskDTO, bool>> query)
		{
			var entities = _context.TaskSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Reference(r => r.Counteragent).Load();
				_context.Entry(entity).Reference(r => r.Project).Load();
				_context.Entry(entity).Collection(r => r.Tags).Load();
			}
			return Mapper.Map<IEnumerable<TaskDTO>>(entities).AsQueryable().Where(query).ToList();
        }

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов TaskDTO</returns>
		public IEnumerable<TaskDTO> ReadAll()
		{
			var entities = _context.TaskSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Reference(r => r.Counteragent).Load();
				_context.Entry(entity).Reference(r => r.Project).Load();
				_context.Entry(entity).Collection(r => r.Tags).Load();
			}
			return Mapper.Map<IEnumerable<TaskDTO>>(entities);
		}

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект TaskDTO</param>
		public void Update(TaskDTO item)
		{
			var newEntity = Mapper.Map<TaskEntity>(item);
			var oldEntity= _context.TaskSet.Find(item.ID);
			var tags = new List<TagEntity>();
			var dbSet = _context.TagSet;
			foreach (var t in item.Tags)
			{
				var tag = dbSet.Find(t.ID) ?? Mapper.Map<TagEntity>(t);
				tags.Add(tag);
			}
			
			_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
			oldEntity.Tags = tags;
			_context.Entry(oldEntity).State = EntityState.Modified;
		}

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		public void Delete(int id)
		{
			var entity = _context.CounteragentSet.Find(id);
			_context.Entry(entity).State = EntityState.Deleted;
		}
	}
}
