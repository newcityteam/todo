﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// Репозиторий для работы с сущностью Задача.
	/// Обработку исключений выполнять в вышестоящих классах
	/// </summary>
	public class CounteragentRepository : IRepository<CounteragentDTO>
	{
		private TODOContext _context;

		public CounteragentRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект CounteragentDTO</param>
		public void Create(CounteragentDTO item)
		{
			var entity = Mapper.Map<CounteragentEntity>(item);
			_context.CounteragentSet.Add(entity);
			
			// сохраняем в БД, чтобы получить ID нового объекта
			_context.SaveChanges();
			item.ID = entity.ID;
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект CounteragentDTO</returns>
		public CounteragentDTO Read(int id)
		{
			var entity = _context.CounteragentSet.Find(id);
			if (entity == null)
			{
				throw new	NullReferenceException();
			}
			_context.Entry(entity).Collection(r => r.Tasks).Load();
			_context.Entry(entity).Collection(r => r.Projects).Load();
			var dto = Mapper.Map<CounteragentDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов CounteragentDTO</returns>
		public IEnumerable<CounteragentDTO> Read(Expression<Func<CounteragentDTO, bool>> query)
		{
			var entities = _context.CounteragentSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Collection(r => r.Tasks).Load();
				_context.Entry(entity).Collection(r => r.Projects).Load();
			}
			return Mapper.Map<IEnumerable<CounteragentDTO>>(entities).AsQueryable().Where(query).ToList();
		}

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов CounteragentDTO</returns>
		public IEnumerable<CounteragentDTO> ReadAll()
		{
			var entities = _context.CounteragentSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Collection(r => r.Tasks).Load();
				_context.Entry(entity).Collection(r => r.Projects).Load();
			}
			return Mapper.Map<IEnumerable<CounteragentDTO>>(entities);
		}

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект CounteragentDTO</param>
		public void Update(CounteragentDTO item)
		{
			var newEntity = Mapper.Map<CounteragentEntity>(item);
			var oldEntity= _context.CounteragentSet.Find(item.ID);

			_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
			_context.Entry(oldEntity).State = EntityState.Modified;
		}

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		public void Delete(int id)
		{
			var entity = _context.CounteragentSet.Find(id);
			_context.Entry(entity).State = EntityState.Deleted;
		}
	}
}
