﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// Репозиторий для работы с сущностью Тег.
	/// Обработку исключений выполнять в вышестоящих классах
	/// </summary>
	public class TagRepository : IRepository<TagDTO>
	{
		private TODOContext _context;

		public TagRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект TagDTO</param>
		public void Create(TagDTO item)
		{
			var entity = Mapper.Map<TagEntity>(item);
			_context.TagSet.Add(entity);
		}


		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект TagDTO</returns>
		public TagDTO Read(int id)
		{
			var entity = _context.TagSet.Find(id);
			if (entity == null)
			{
				throw new NullReferenceException();
			}
			_context.Entry(entity).Collection(r => r.Projects).Load();
			_context.Entry(entity).Collection(r => r.Tasks).Load();
			var dto = Mapper.Map<TagDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов TagDTO</returns>
		public IEnumerable<TagDTO> Read(Expression<Func<TagDTO, bool>> query)
		{
			var entities = _context.TagSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Collection(r => r.Projects).Load();
				_context.Entry(entity).Collection(r => r.Tasks).Load();
			}
			return Mapper.Map<IEnumerable<TagDTO>>(entities).AsQueryable().Where(query).ToList();
		}

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов TagDTO</returns>
		public IEnumerable<TagDTO> ReadAll()
		{
			var entities = _context.TagSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Collection(r => r.Projects).Load();
				_context.Entry(entity).Collection(r => r.Tasks).Load();
			}
			return Mapper.Map<IEnumerable<TagDTO>>(entities);
		}

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект TagDTO</param>
		public void Update(TagDTO item)
		{
			var newEntity = Mapper.Map<TagEntity>(item);
			var oldEntity = _context.TagSet.Find(item.ID);

			_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
			_context.Entry(oldEntity).State = EntityState.Modified;
		}

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		public void Delete(int id)
		{
			var entity = _context.TagSet.Find(id);
			_context.Entry(entity).State = EntityState.Deleted;
		}
	}
}
