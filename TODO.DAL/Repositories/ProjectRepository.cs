﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Types;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// ProjectRepository
	/// </summary>
	public class ProjectRepository : IRepository<ProjectDTO>
	{
		private readonly TODOContext _context;

		public ProjectRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект ProjectDTO</param>
		public void Create(ProjectDTO item)
		{
			var entity = Mapper.Map<ProjectEntity>(item);
			_context.ProjectSet.Add(entity);

			// сохраняем в БД, чтобы получить ID нового объекта
			_context.SaveChanges();
			item.ID = entity.ID;
		}

		public void Delete(int id)
		{
			var entity = _context.ProjectSet.Find(id);
			_context.Entry(entity).State = EntityState.Deleted;
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект ProjectDTO</returns>
		public ProjectDTO Read(int id)
		{
			var entity = _context.ProjectSet.Find(id);
			if (entity == null)
			{
				throw new NullReferenceException();
			}
			_context.Entry(entity).Reference(r => r.Counteragent).Load();
			_context.Entry(entity).Collection(r => r.Tasks).Load();
			_context.Entry(entity).Collection(r => r.Tags).Load();
			_context.Entry(entity).Collection(r => r.IncomeFinance).Load();
			var dto = Mapper.Map<ProjectDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов ProjectDTO</returns>
		public IEnumerable<ProjectDTO> Read(Expression<Func<ProjectDTO, bool>> query)
		{
			var entities = _context.ProjectSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Reference(r => r.Counteragent).Load();
				_context.Entry(entity).Collection(r => r.Tasks).Load();
				_context.Entry(entity).Collection(r => r.Tags).Load();
				_context.Entry(entity).Collection(r => r.IncomeFinance).Load();
			}
			return Mapper.Map<IEnumerable<ProjectDTO>>(entities).AsQueryable().Where(query).ToList();
		}

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов ProjectDTO</returns>
		public IEnumerable<ProjectDTO> ReadAll()
		{
			var entities = _context.ProjectSet.ToList();
			foreach (var entity in entities)
			{
				_context.Entry(entity).Reference(r => r.Counteragent).Load();
				_context.Entry(entity).Collection(r => r.Tasks).Load();
				_context.Entry(entity).Collection(r => r.Tags).Load();
				_context.Entry(entity).Collection(r => r.IncomeFinance).Load();
			}
			return Mapper.Map<IEnumerable<ProjectDTO>>(entities);
		}

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект ProjectDTO</param>
		public void Update(ProjectDTO item)
		{
			var newEntity = Mapper.Map<ProjectEntity>(item);
			var oldEntity = _context.ProjectSet.Find(item.ID);

			var dbSet = _context.TagSet;
			var tags = item.Tags.Select(t => dbSet.Find(t.ID) ?? Mapper.Map<TagEntity>(t)).ToList();

			_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
			oldEntity.Tags = tags;
			_context.Entry(oldEntity).State = EntityState.Modified;
		}
	}
}
