﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.Common.DTO;
using TODO.Common.Interfaces.Services;
using TODO.DAL.Context;
using TODO.DAL.Repositories;

namespace TODO.DAL.Tests
{
	[TestClass]
	public class FinanceOperationRepositoryTests
	{
		private static TODOContext _context = new TODOContext("TestConnection");
		private static IUnitOfWork _unit = new UnitOfWork(_context);
		private static FinanceOperationRepository _FinOpRepo = new FinanceOperationRepository(_context);

		// Создание финансовой операции и чтение
		[TestMethod]
		public void CreateCorrect()
		{
			var currentMaxId = new ObservableCollection<FinanceOperationDTO>(_FinOpRepo.ReadAll()).Count;
			var testItem = new FinanceOperationDTO()
			{
				Date = DateTime.Now,
				AmountIncome = 12340,
				AmountOutcome = 0,
				Description = "default"
			};
			_FinOpRepo.Create(testItem);
			var result = _FinOpRepo.Read(currentMaxId + 1);

			Assert.AreEqual(result.Date, testItem.Date);
			Assert.AreEqual(result.Description, testItem.Description);
			Assert.AreEqual(result.AmountIncome, testItem.AmountIncome);
			Assert.AreEqual(result.AmountOutcome, testItem.AmountOutcome);
		}

		// Создание пустой финансовой операции
		public void CreateEmpty()
		{
			var testItem = new FinanceOperationDTO();
			_FinOpRepo.Create(testItem);
		}
	}
}
