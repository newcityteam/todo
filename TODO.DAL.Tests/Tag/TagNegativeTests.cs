﻿using System;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL.Context;
using TODO.DAL.Repositories;

namespace TODO.DAL.Tests.Tag
{
	[TestClass]
	public class TagNegativeTests
	{
		private IRepository<TagDTO> _tagRepo;

		[TestInitialize]
		public void Initialize()
		{
			var context = new TODOContext("TestConnection");
			var tagRepo = new TagRepository(context);
			
			Mapper.Reset();
		}

		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void ReadNonExsistTag()
		{
			_tagRepo.Read(Int32.MaxValue);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void DeleteNonExsistTag()
		{
			_tagRepo.Delete(Int32.MaxValue);
		}
	}
}
