﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL.Context;
using TODO.DAL.Repositories;

namespace TODO.DAL.Tests.Tag
{
	[TestClass]
	public class TagPositiveTests
	{
		private IRepository<TagDTO> _tagRepo;

		[TestInitialize]
		public void Initialize()
		{
			var context = new TODOContext("TestConnection");
			_tagRepo = new TagRepository(context);

			Mapper.Reset();
		}

		[TestMethod]
		public void Create()
		{
			try
			{
				var dto = new TagDTO { Name = "TEST TAG " + DateTime.Now.ToLongTimeString() };
				_tagRepo.Create(dto);
				Assert.IsTrue(true);
			}
			catch (Exception ex)
			{
				Assert.Fail(ex.Message);
			}
		}

		[TestMethod]
		public void Read()
		{
			try
			{
				var newDto = new TagDTO { Name = "TEST TAG " + DateTime.Now.ToLongTimeString() };
				_tagRepo.Create(newDto);

				var testDto = _tagRepo.Read(newDto.ID);

				Assert.AreEqual(newDto.Name, testDto.Name);
			}
			catch (Exception ex)
			{
				Assert.Fail(ex.Message);
			}
		}

		[TestMethod]
		public void ReadAll()
		{
			try
			{
				var newDto = new TagDTO { Name = "TEST TAG " + DateTime.Now.ToLongTimeString() };
				_tagRepo.Create(newDto);
				_tagRepo.Create(newDto);

				var testDto = _tagRepo.ReadAll().ToList();

				Assert.AreEqual(testDto.Count > 2, true);
			}
			catch (Exception ex)
			{
				Assert.Fail(ex.Message);
			}
		}

		[TestMethod]
		public void Update()
		{
			try
			{
				var dto = new TagDTO { Name = "TEST TAG " + DateTime.Now.ToLongTimeString() };
				_tagRepo.Create(dto);

				dto.Name += " UPDATED";

				_tagRepo.Update(dto);

				var updatedDto = _tagRepo.Read(dto.ID);

				Assert.AreEqual(dto.Name, updatedDto.Name);
			}
			catch (Exception ex)
			{
				Assert.Fail(ex.Message);
			}
		}

		[TestMethod]
		public void Delete()
		{
			try
			{
				var dto = new TagDTO { Name = "TEST TAG " + DateTime.Now.ToLongTimeString() };
				_tagRepo.Create(dto);

				_tagRepo.Delete(dto.ID);

				try
				{
					_tagRepo.Read(dto.ID);
					Assert.Fail("Запись не удалилась");
				}
				catch 
				{
					// запись удалилась
					Assert.IsTrue(true);
				}
			}
			catch (Exception ex)
			{
				Assert.Fail(ex.Message);
			}
		}
	}
}
