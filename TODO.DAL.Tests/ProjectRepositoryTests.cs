﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TODO.DAL;
using TODO.DAL.Context;
using TODO.DAL.Entities;
using TODO.DAL.Repositories;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using AutoMapper;
using System.Data.Entity;
using System.Linq;

namespace TODO.DAL.Tests
{
	[TestClass]
	public class ProjectRepositoryTests
	{
		private class TestContext : TODOContext
		{
			public TestContext(string connectionString) : base(connectionString)
			{
			}

			protected override void OnModelCreating(DbModelBuilder modelBuilder)
			{
				base.OnModelCreating(modelBuilder);
				Database.SetInitializer(new TestInitializer());
			}

		}

		private class TestInitializer : DropCreateDatabaseAlways<TestContext>
		{
			protected override void Seed(TestContext context)
			{
				_context.CounteragentSet.Add(new CounteragentEntity
				{
					Name = "TestCounteragent",
					Type = CounteragentType.IndividualPerson
				});
				_context.ProjectSet.Add(new ProjectEntity
				{
					Title = "TestProject1",
					Description = "Test project 1 description",
					DateCreated = DateTime.Now.AddDays(-2),
					DateDuePlanned = DateTime.Now.AddDays(2),
					DateDueFact = null,
					State = ProjectStateType.InProgress,
					CounteragentID = 1
				});
				_context.ProjectSet.Add(new ProjectEntity
				{
					Title = "TestProject2",
					Description = "Test project 2 description",
					DateCreated = DateTime.Now,
					DateDuePlanned = DateTime.Now.AddDays(26),
					DateDueFact = null,
					State = ProjectStateType.InProgress,
					CounteragentID = 1
				});
				_context.ProjectSet.Add(new ProjectEntity
				{
					Title = "TestProject3",
					Description = "Test project 3 description",
					DateCreated = DateTime.Now.AddDays(-55),
					DateDuePlanned = DateTime.Now.AddDays(246),
					DateDueFact = DateTime.Now.AddDays(-1),
					State = ProjectStateType.Completed,
					CounteragentID = 1
				});
				_context.ProjectSet.Add(new ProjectEntity
				{
					Title = "ToBeDeletedProject",
					Description = "ToBeDeletedProject description",
					DateCreated = DateTime.Now.AddDays(-15),
					DateDuePlanned = DateTime.Now.AddDays(5),
					DateDueFact = null,
					State = ProjectStateType.InProgress,
					CounteragentID = 1
				});
				_context.ProjectSet.Add(new ProjectEntity
				{
					Title = "ToBeUpdatedProject",
					Description = "ToBeUpdatedProject description",
					DateCreated = DateTime.Now.AddDays(-11),
					DateDuePlanned = DateTime.Now.AddDays(4),
					DateDueFact = null,
					State = ProjectStateType.Cancelled,
					CounteragentID = 1
				});

				base.Seed(context);
			}
		}

		private static TestContext _context = new TestContext("TestConnection");
		private static IUnitOfWork _unitOfWork = new UnitOfWork(_context);
		private static IRepository<ProjectDTO> _projectRepo = new ProjectRepository(_context);

		[TestMethod]
		public void CreateTest()
		{
			// Arrange
			var item = new ProjectEntity
			{
				Title = "NewProject",
				Description = "New project description",
				DateCreated = DateTime.Now.AddDays(-15),
				DateDuePlanned = DateTime.Now.AddDays(5),
				DateDueFact = null,
				State = ProjectStateType.InProgress,
				CounteragentID = 1,
				Counteragent = _context.CounteragentSet.Find(1)
			};

			// Act
			_projectRepo.Create(Mapper.Map<ProjectDTO>(item));

			// Assert
			var result = _context.ProjectSet.Where(project => project.Title == item.Title).FirstOrDefault();
			Assert.IsTrue(
				result.Title == item.Title &&
				result.Description == item.Description &&
				result.DateCreated == item.DateCreated &&
				result.DateDuePlanned == item.DateDuePlanned &&
				result.DateDueFact == item.DateDueFact &&
				result.State == item.State &&
				result.CounteragentID == item.CounteragentID);
		}

		[TestMethod]
		public void DeleteTest()
		{
			// Arrange
			// Уже добавлена сущность в инициализаторе.
			var id = _context.ProjectSet.Where(project => project.Title == "ToBeDeletedProject").FirstOrDefault().ID;

			// Act
			_projectRepo.Delete(id);
			_unitOfWork.Complete();

			// Assert
			var result = _context.ProjectSet.Where(project => project.Title == "ToBeDeletedProject").FirstOrDefault();
			Assert.IsNull(result);
		}

		[TestMethod]
		public void UpdateTest()
		{
			// Arrange
			// Уже добавлена сущность в инициализаторе.
			var item = _context.ProjectSet.Where(project => project.Title == "ToBeUpdatedProject").FirstOrDefault();

			// Act
			item.Description = "UpdatedDescription";
			_projectRepo.Update(Mapper.Map<ProjectDTO>(item));
			_unitOfWork.Complete();

			// Assert
			var result = _context.ProjectSet.Where(project => project.Title == "ToBeUpdatedProject").FirstOrDefault();
			Assert.AreEqual("UpdatedDescription", result.Description);
		}

		[TestMethod]
		public void ReadByIdTest()
		{
			// Succesful behavior
			// Arrange
			// Уже добавлена сущность в инициализаторе.
			var item = _context.ProjectSet.Find(1);

			// Act
			var result = _projectRepo.Read(1);

			// Assert
			Assert.IsTrue(
				result.Title == item.Title &&
				result.Description == item.Description &&
				result.DateCreated == item.DateCreated &&
				result.DateDuePlanned == item.DateDuePlanned &&
				result.DateDueFact == item.DateDueFact &&
				result.State == item.State &&
				result.Counteragent.ID == item.CounteragentID);

			// Exception behavior
			// Act/Assert
			Assert.ThrowsException<NullReferenceException>(() => _projectRepo.Read(-1));
		}

		[TestMethod]
		public void ReadByQueryTest()
		{
			// Arrange
			// Уже добавлены сущности в инициализаторе.
			var items = _context.ProjectSet.Where(project => project.State == ProjectStateType.InProgress).ToList();

			// Act
			var result = _projectRepo.Read(project => project.State == ProjectStateType.InProgress).ToList();

			// Assert
			Assert.IsFalse(result.Exists(project => project.State != ProjectStateType.InProgress));
		}

		[TestMethod]
		public void ReadAllTest()
		{
			// Arrange
			// Уже добавлены сущности в инициализаторе.
			var items = _context.ProjectSet.ToList();

			// Act
			var result = _projectRepo.ReadAll().ToList();

			// Assert
			Assert.AreEqual(result.Count, items.Count);
			foreach (var r in result)
			{
				Assert.IsTrue(items.Exists(item =>
					r.Title == item.Title &&
					r.Description == item.Description &&
					r.DateCreated == item.DateCreated &&
					r.DateDuePlanned == item.DateDuePlanned &&
					r.DateDueFact == item.DateDueFact &&
					r.State == item.State &&
					r.Counteragent.ID == item.CounteragentID));
			}
		}
	}
}
