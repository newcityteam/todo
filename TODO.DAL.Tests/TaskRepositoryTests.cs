﻿
using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.DAL.Context;
using TODO.DAL.Repositories;



namespace TODO.DAL.Tests
{
	/// <summary>
	/// Интеграционный текст репозитория Tasks и БД.
	/// <note type="note">Используется БД с тремя записями, предсоздаваемая в ToDoContextDEV.</note>
	/// <note type="note">Не запускать автоматически, порядок запуска не важен.</note>
	/// <note type="note">На 11.08 метод Read(Expression) не покрыт тестами из-за мега-бага в архитектуре.</note> 
	/// <note type="note">(всё равно этот метод надо полностью переписывать и только после этого тестировать).</note>
	/// </summary>
	[TestClass]
	public class TaskRepositoryTests
	{
		private static Mock<ILogger> mock_logger = new Mock<ILogger>();
		private static TODOContext _context = new TODOContext("TestConnection");
		private static TaskRepository _taskRepo = new TaskRepository(_context);
		private static CounteragentRepository _caRepo = new CounteragentRepository(_context);
		private static TagRepository _tagRepo = new TagRepository(_context);
		private static IUnitOfWork _unit = new UnitOfWork(_context);

		/// <summary>
		/// Создать корректную запись Task
		/// </summary>
		[TestMethod]
		public void Test_CreateCorrect()
		{
			var dto = new TaskDTO()
			{
				DateCreated = DateTime.Now.Date,
				Title = "default",
				DateDue = new DateTime(2018, 10, 10),
				Description = "default"
			};
			_taskRepo.Create(dto);
			// БД создана с тремя записями, я добавил еще одну
			// Итого id = 4
			var dto2 = _taskRepo.Read(4);
			Assert.AreEqual(dto.Title, dto2.Title);
			Assert.AreEqual(dto.Description, dto2.Description);
			Assert.AreEqual(dto.DateDue, dto2.DateDue);
			Assert.AreEqual(dto.DateCreated, dto2.DateCreated);
		}

		/// <summary>
		/// Создать пустую Task (не даст из-за обязательного поля DateCreated)
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
		public void Test_CreateEmptyDTO()
		{
			var dto = new TaskDTO();
			_taskRepo.Create(dto);
		}

		/// <summary>
		/// Создать таску с заполненным полем DateCreated
		/// </summary>
		[TestMethod]
		public void Test_CreateDTOwithDateCreated()
		{
			var dto = new TaskDTO()
			{
				DateCreated = DateTime.Now.Date
			};
			_taskRepo.Create(dto);
			// База рестартовала, 3 записи по умолчанию + 1
			var dto2 = _taskRepo.Read(4);
			Assert.AreEqual(DateTime.Now.Date, dto2.DateCreated);
		}

		/// <summary>
		/// Чтение корректной записи по ID
		/// </summary>
		[TestMethod]
		public void Test_ReadCorrect()
		{
			var dto = _taskRepo.Read(1);
			// Сравниваем значение с предзаписанной записью из ToDoContexDEV
			Assert.AreEqual("Task number one", dto.Title);
			Assert.AreEqual("Some description for task one", dto.Description);
			Assert.AreEqual(DateTime.Now.AddDays(-1).Date, dto.DateCreated.Date);
			var dt = (DateTime)dto.DateDue;
			Assert.AreEqual(DateTime.Now.AddDays(5).Date, dt.Date);
			Assert.AreEqual(TaskStateType.Cancelled, dto.State);
			Assert.AreEqual(TaskDirectionType.Outcome, dto.Direction);
			Assert.IsNull(dto.Counteragent);
		}

		/// <summary>
		/// Чтение записи по некорректному ID. Метод должен возвращать NullReferenceException.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void Test_ReadIncorrectID()
		{
			_taskRepo.Read(-1);
		}

		/// <summary>
		/// Удаление записи по корректному ID 
		/// Не работает совсем, но метод ни разу не вызывается в проекте
		/// </summary>
		[TestMethod]
		public void Test_DeleteCorrect()
		{
			_taskRepo.Delete(1);
			var _dto = _taskRepo.Read(1);
			Assert.AreEqual(TaskStateType.Deleted, _dto.State);
		}

		/// <summary>
		/// Удаление записи с некорректным ID
		/// </summary>
		[TestMethod]
		public void Test_DeleteIncorrectID()
		{
			_taskRepo.Delete(10);
		}

		/// <summary>
		/// Корректный (непустой) ReadAll
		/// </summary>
		[TestMethod]
		public void Test_ReadAllCorrect()
		{
			var _vr = _taskRepo.ReadAll();
			int i = 0;
			foreach (var item in _vr)
			{
				i++;
			}
			Assert.AreEqual(3, i);
		}

		/// <summary>
		/// Пустой ReadAll
		/// Будет работать только если закомментировать инициализацию базы в ToDoContextDEV 
		/// </summary>
		[TestMethod]
		public void Test_ReadAllEmpty()
		{
			var _vr = _taskRepo.ReadAll();
			Assert.IsTrue(!_vr.Any());
		}

		/// <summary>
		/// Update на записи с некорректным id 
		/// Тест не проходит, необработанное исключение
		/// </summary>
		[TestMethod]
		public void Test_UpdateIncorrectID()
		{
			var _dto = new TaskDTO()
			{
				ID = 6,
				Title = "test Title"
			};
			_taskRepo.Update(_dto);
		}

		/// <summary>
		/// Update на запись с пустой DateDue 
		/// </summary>
		[TestMethod]
		public void Test_UpdateDeleteDateDue()
		{
			var _dto = _taskRepo.Read(1);
			_dto.DateDue = null;
			_taskRepo.Update(_dto);
			var dto2 = _taskRepo.Read(1);
			Assert.IsNull(dto2.DateDue);
		}

		/// <summary>
		/// Update записи поверх себя без изменений 
		/// </summary>
		[TestMethod]
		public void Test_UpdateYourselfwithChanges()
		{
			var _dto = new TaskDTO()
			{
				Title = "default",
				DateCreated = DateTime.Now.Date
			};
			_taskRepo.Create(_dto);
			_taskRepo.Update(_dto);
			var dto2 = _taskRepo.Read(4);
			Assert.AreEqual("default", dto2.Title);
		}

		/// <summary>
		/// Update на удаление CounterAgent
		/// </summary>
		[TestMethod]
		public void Test_UpdateDeleteCounteragent()
		{
			var _dto = new TaskDTO()
			{
				ID = 1,
				DateCreated = DateTime.Now.Date,
				Counteragent = null
			};
			_taskRepo.Update(_dto);
			var dto2 = _taskRepo.Read(1);
			Assert.IsNull(dto2.Counteragent);
		}

		/// <summary>
		/// Update с пустым CounterAgent, которого нет в справочнике 
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(DbUpdateException))]
		public void Test_UpdateNewEmptyCountragent()
		{
			var _dto = new TaskDTO()
			{
				ID = 1,
				DateCreated = DateTime.Now.Date,
				Counteragent = new CounteragentDTO()
			};

			_taskRepo.Update(_dto);
		}

		/// <summary>
		/// Update с null контрагентом
		/// </summary>
		[TestMethod]
		public void Test_UpdateNewNullCounteragent()
		{
			var _dto = new TaskDTO()
			{
				ID = 1,
				DateCreated = DateTime.Now.Date,
				Counteragent = null
			};

			_taskRepo.Update(_dto);
			var _read = _taskRepo.Read(1);
			Assert.IsNull(_read.Counteragent);
		}

		/// <summary>
		/// Update с заполненным CounterAgent, которого нет в справочнике 
		/// </summary>
		[TestMethod]
		public void Test_UpdateNewFillCounteragent()
		{
			var _dto = new TaskDTO()
			{
				ID = 1,
				DateCreated = DateTime.Now.Date,
				Counteragent = new CounteragentDTO()
				{
					ID = 10,
					Address = "test",
					Description = "vasya",
					INN = "11111",
					KPP = "1111",
					Name = "kolya",
					PaymentAccount = "111",
					PhoneNumber = "222",
					Type = CounteragentType.Legal,
				}
			};
			_taskRepo.Update(_dto);
		}

		/// <summary>
		/// Update с новым контрагентом, но id которого используется в справочнике
		/// По текущему поведению - он перезаписывает Контрагента! 
		/// </summary>
		[TestMethod]
		public void Test_UpdateFakeCounteragent()
		{
			var _dto = new TaskDTO()
			{
				ID = 1,
				DateCreated = DateTime.Now.Date,
				Counteragent = new CounteragentDTO()
				{
					ID = 1,
					Address = "test",
					Description = "vasya",
					INN = "11111",
					KPP = "1111",
					Name = "kolya",
					PaymentAccount = "111",
					PhoneNumber = "222",
					Type = CounteragentType.Legal,
				}
			};
			_taskRepo.Update(_dto);
			var _dto2 = _taskRepo.Read(1);
			Assert.IsNotNull(_dto2.Counteragent);
			// в настоящее время тут сваливается в null

		}
	}
}
