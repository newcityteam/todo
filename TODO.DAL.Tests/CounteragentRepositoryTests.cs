﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TODO.Common.DTO;
using TODO.Common.Interfaces.Services;
using TODO.DAL.Context;
using TODO.DAL.Repositories;

namespace TODO.DAL.Tests
{
	[TestClass]
	public class CounteragentRepositoryTests
	{
		private static TODOContext _context = new TODOContext("TestConnection");
		private static IUnitOfWork _unit = new UnitOfWork(_context);
		private static CounteragentRepository _caRepo = new CounteragentRepository(_context);
		

		/// <summary>
		/// Создание стандартного контрагента и чтение его из базы
		/// </summary>
		[TestMethod]
		public void CreateCorrect()
		{
			var currentMaxId = new ObservableCollection<CounteragentDTO>(_caRepo.ReadAll()).Count;
			var testItem = new CounteragentDTO
			{
				Name = "Default",
				Description = "Standart counteragent",
				PhoneNumber = "35783794790",
				Address = "Default",
				Type = Common.Types.CounteragentType.IndividualPerson
			};
			_caRepo.Create(testItem);
			var result = _caRepo.Read(currentMaxId + 1);

			Assert.AreEqual(result.Name, testItem.Name);
			Assert.AreEqual(result.Description, testItem.Description);
			Assert.AreEqual(result.Address, testItem.Address);
			Assert.AreEqual(result.Type, testItem.Type);
		}

		/// <summary>
		/// Проверка на создание с пустой DTO
		/// Ожидается получение ошибки
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void CreateEmpty_ExceptionExpected()
		{
			var testItem = new CounteragentDTO();

			_caRepo.Create(testItem);
		}

		/// <summary>
		/// Read при неправильном id должен вызывать Exception
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void ReadWrongId_ExceptionExpected()
		{
			_caRepo.Read(-1);
		}
		/// <summary>
		/// Проверка метода ReadAll: возвращаемых элементов должно быть больше 2-х
		/// </summary>
		[TestMethod]
		public void ReadAllCorrect()
		{
			for (var i = 0; i < 3; i++)
			{
				var testItem = new CounteragentDTO
				{
					Name = $"Item for test ReadAll method #{i}",
					Description = $"Description for item #{i} for test ReadAll method",
					PhoneNumber = "56546234325",
					Address = "Address for test ReadAll method",
					Type = Common.Types.CounteragentType.IndividualPerson
				};
				_caRepo.Create(testItem);
			}

			var result = new ObservableCollection<CounteragentDTO>(_caRepo.ReadAll()).Count;

			Assert.IsTrue(result > 2);
		}

		/// <summary>
		/// Проверка перегрузки метода Read(Func query)
		/// </summary>
		[TestMethod]
		public void ReadExpressionCorrect()
		{
			var testItem = new CounteragentDTO
			{
				Name = "Test item",
				Description = "Description for test Read method with Expression attribute",
				PhoneNumber = "47296463473",
				Address = "Test addr",
				Type = Common.Types.CounteragentType.IndividualPerson
			};
			_caRepo.Create(testItem);

			var result = _caRepo.Read(x => x.Description == testItem.Description);

			Assert.IsNotNull(result);
		}

		/// <summary>
		/// А что, если в поле для номера телефона ввести строку?
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void CreateWithWordsInPhoneNumber_ExceprionExpected()
		{
			var testItem = new CounteragentDTO
			{
				Name = "Test item",
				Description = "Just trying to put string in phonenumber",
				PhoneNumber = "Default",
				Address = "Test addr",
				Type = Common.Types.CounteragentType.IndividualPerson
			};
			_caRepo.Create(testItem);
		}

		/// <summary>
		/// Тест на ненулевые данные для юр. лица (ИНН, КПП, РС) у физ. лица
		/// </summary>
		[TestMethod]
		public void CreatePhysicalWithInn_ExceptionExpected()
		{
			var testItem = new CounteragentDTO
			{
				Name = "Physical person",
				Description = "Физ. лицо не должен содержать ИНН, КПП и расчётный счёт",
				Address = "Default",
				PhoneNumber = "45727838366",
				Type = Common.Types.CounteragentType.IndividualPerson,
				INN = "Default",
				KPP = "346543634",
				PaymentAccount = "Default"
			};

			_caRepo.Create(testItem);
			var result = _caRepo.Read(x => x.Description == testItem.Description) as CounteragentDTO;
			var testString = result.INN ?? result.KPP ?? result.PaymentAccount ?? string.Empty;

			if (result.Type == 0 && testString != string.Empty)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Проверка метода Update 
		/// </summary>
		[TestMethod]
		public void UpdateCorrect()
		{
			var testItem = new CounteragentDTO
			{
				Name = "Name of item before update",
				Description = "Testing update method",
				PhoneNumber = "37637643372",
				Address = "Default",
				Type = Common.Types.CounteragentType.IndividualPerson
			};
			_caRepo.Create(testItem);
			var expect = _caRepo.Read(x => x.Name == testItem.Name);
			var testItem2 = testItem;
			foreach (CounteragentDTO s in expect)
			{
				testItem2 = s;
			}
			testItem2.Name = "Name of item after update";

			_caRepo.Update(testItem2);
			var result = _caRepo.Read(x => x.Name == testItem2.Name);

			Assert.AreNotEqual(expect, result);
		}

		/// <summary>
		/// Проверка метода Delete
		/// </summary>
		[TestMethod]
		public void DeleteCorrect()
		{
			var minValue = 1;
			var max = new ObservableCollection<CounteragentDTO>(_caRepo.ReadAll()).Count;

			for (; max > minValue; max--)
			{
				_caRepo.Delete(max);
			}

			var result = new ObservableCollection<CounteragentDTO>(_caRepo.ReadAll()).Count;

			Assert.AreEqual(result, minValue);
		}
	}
}
